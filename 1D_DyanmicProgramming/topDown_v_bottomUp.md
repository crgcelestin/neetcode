# Top-Down (Recursive, Memoization) v Bottom Up (Iterative, Tabulation)

## Two Methods in solving DP
- Top Down, Bottom Up
    * both methods: use extra memory to store solution to sub problems, avoid recomputation, improve performance by great margin

### Top Down Approach:
- Implement solution naturally using recursion -> modify to save solution to each subproblem in array/hash table
    * First check if there is a previously solved sub -> if yes, return stored value else top down calcs solutions in typical manner i.e a memoized version of recursive solution

### Bottom Up Approach:
- depends on natural idea: solution of subproblem depends on solution to smaller subs -> bottom up approach sorts subs on input size, solve iteratively in order of smallest to largest
    - solve smaller subs first to solve large problem and store in extra memory

## Critical Differences
-  top down: assumed subs are to be solved using smaller sub once in recursion whereas bottom up: compose subs solution iteratively using smaller subs
- top down has easy implementation as a result of adding array, lookup table to store results v bottom up where we define iterative order to fill table, take care of boundary conditions
- top down is slower due to recursive call overhead v bottom up which has better constant factors
- if recursion is deep, may run out of stack with top down approach
- both guarantee same tc except in strange events where top down doesn't recurse to examine all possible subproblems
- top down: starts from large input size, reach smallest version of problem (base case), store subproblem solutions from base case to larger problem v bottom up: store solution for base case -> large problem
- w/ bottom up: there is scope to optimize tc and sc further, reduce 1 loop or try to save space from linear to constant, challenge in top down approach

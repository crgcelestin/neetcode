# [Guide to DP](https://medium.com/@meetdianacheung/dynamic-programming-made-easy-the-ideal-method-for-technical-interviews-c975cb7e6859)
- DP is an optimization technique used to solve complex problem via subproblems, store results to avoid recomputations

## IDEAL
- id problem, def state w/ fewest params, express recurrence relation + base cases, adopt implementation approach (top down v bottom up), observe sc and tc
### Example
1. id dp problem -> __suitable for situations asking for 'max/longest' or 'min/shortest' solution on sequence (array, matrix, string) as well as problems asking for several ways to do something meeting specific conditions__ = great candidates
    * applying dp method involves breaking down original problem into sub-problems satisfying 2 methods
        - overlapping sub-problems
            * after splitting original problem, some sub-problems are encountered several times
                - calc fib(4) require calc fib(2), fib(1), fib(0) are encountered several times
        - optimal substructure
            * optimal solution can be obtained via optimal solutions from sub-problems
                - optimal solution to computing fib(4) as combo of optimal solutions to fib(3), fib(2)
2. Define state w/ fewest parameters
    - state = set of params that uniquely id various sub-problems, use as few params as possible to mitigate space req to store state data
        * ex: fibonacci seq, state = serial # or position in sequence `n`
3. Express recurrence relation, base cases
    * Recurrence relation:
        - formulate relation between prev states to reach current
            * for fib, recurrence relation is -> fib(n) = fib(n-1) + fib(n-2)
    * Base Cases:
        - subproblem that doesn't depend on other subproblem
            * bases cases for fib are fib(0) = 0, fib(1) = 1
4. adopt implementation (top down v bottom up)
    - top down (recursive + memoization): centers around decomposition, start with original problem and break into smaller subproblems -> recursion is used to repeatedly split problem into several smaller subproblems and solve them
        * memoization involves storing results of these subproblems in dict/hashmap for lookup + avoid recomputation and improve tc
            ```js
            var fib = function(n, memo = {}){

                // if we find that we contain # in memo meaning we have already precomputed fib(#), then return that result
                if(n in memo){
                    return memo[n]
                }

                // base cases
                if(n === 0 || n === 1){
                    return n;
                }

                // performing calculations, we know that fibonacci is about our desired result for fib(#) being the aggregate sum of prior populations
                let result = fib(n-1, memo) + fib(n-2, memo);

                // storing precalculated result for use for end result
                memo[n] = result;
                return result;
            }
            ```
    - bottom up (iterative + tabulation): centers around composition, start with smallest subproblem and then build up with iteration used to go through all possibilities and store results in table/array
        ```js
        var fib = function(n){
            // storing base cases of 0,1
            let table = [0,1]

            // iterate by units of 2 and use the recurrence relation that is going to provide us fib product that we are adding to table
            for(let i = 2; i <= n; i++){
                table.push(
                    table[i-1] + table[i-2]
                )
            }
            return table[n];

        }
        ```
    - More intuitive to reason with top down approach as well as the order of computing subproblems doesn't matter
    - tc analysis is simpler and no stack overflow risk for bottom up approach
5. analyze tc, sc
    - tc: count # of states, analyze work performed for each state
        * for fib problem, top down and bottom up yield O(n) tc as each fib # is calculated once, stored in ds that can be accessed in constant time
    - sc: aux space introduced by data store of subs
        * O(n) as ds stores results of numbers till n
## DP Challenges
- unable to id/define incorrect state, recurrence relation, base cases
    * critical to dp method -> improve by practicing more dp for common patterns
- stuck in def, accessing suitable ds to store sub results
    - complex probs may require multi dimensional list to store results for subs and may be hard to visualize, work w/ -> practice constructing, initializing, accessing multi dimensional lists
- greedy algo v dynamic programming: pick wrong approach
    * greedy  = pick local optimal solution at each state (fast, simpler to implement) -> if any problem asks for correct solution
    * dp -> use recurrence relation + storage of results of subproblems at each local state to find global optimal solution
        * solution to sub aids in getting to global optimal solution, then dp approach
## Summary

## Practice Problems
- [Target Sum](https://leetcode.com/problems/target-sum/)
- [Max Subarray](https://leetcode.com/problems/maximum-subarray/)
- [Coin Change](https://leetcode.com/problems/coin-change/)
- [Climbing Stairs](https://leetcode.com/problems/climbing-stairs/)
- [Palindromic Substrings](https://leetcode.com/problems/palindromic-substrings/)
- [Longest Increase Subsequence](https://leetcode.com/problems/longest-increasing-subsequence/)
* & House Robber

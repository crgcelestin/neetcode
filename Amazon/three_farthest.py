'''
Given a problem where in a grid we need to return the three furthest points from the origin (0,0)

we could be given 4 points
test sample
[0,0] -> 0
[1,6] -> sqrt(37)
[5,3] -> sqrt(34)
[-3,-4] -> sqrt(25)
[-1,-11] -> sqrt(122)

we need to return the three furthest in order of descending distance
output
[
[-1,-11],
[1,6],
[5,3]
]

distance equation = sqrt((x1-x2)**2 + (y1-y2)**2)
origin distances = sqrt((x**2)+(y**2))

we are assuming there are no duplicates

approach1

we want to create a empty map (tracker), empty array (res)
iterate through arrays in the array
then we want to store distance of each point from origin as a value, point as a key
then append each max in map.items() until the length of returning array is 3
return res
'''
class Solution:
    def isFarthest(self, points:list[list[int]]) -> list[list[int]]:
        tracker={}
        res=[]
        for point in points:
            # distance formula
            distance= ((point[0]**2)+(point[1]**2))**(1/2)
            # must append point as a tuple to tracker as they are imutable
            tracker[tuple(point)]=distance
        # we only want to return at max 3
        while len(res)!= 3:
            # use max in order to find largest of tracker values
            '''
            max_dist=0
            for dist in tracker.values():
                if dist>max_dist:
                    max_dist=dist
            '''
            max_dist=max(tracker.values())
            # list comprehension returns points in order of greatest to least in distance
            farthest=[point for point, dist in tracker.items() if dist==max_dist]
            for point in farthest:
                # append list conversion of point to res, as we know its the largest as it pertains to distance at the current point
                res.append(list(point))
                # delete key that corresponds to max distance
                del tracker[point]
        return res

inputs = [
    [0,0],
    [1,6],
    [-3,-4],
    [5,3],
    [-1,-11]
]
Test=Solution()
print(Test.isFarthest(
    inputs
))

'''
tc: O(n) - iterating through keys, values of length n of tracker in order to store -> find farthest distanced points
sc: O(n) - data structure that worst case has to iterate through all elements in list points of length n (grows linear with # of points)
'''

class Solution:
    def containsDuplicate(self, nums: list[int]) -> bool:
        count={}
        for num in nums:
            if num in count:
                return True
            else:
                count[num]=1
        return False

# optimized solution
class Solution2:
    def containsDuplicate2(self, nums: list[int]) -> bool:
        unique = set(nums)
        if(len(nums)!=len(unique)):
            return True
        return False

TestCase = Solution2()
print(True==TestCase.containsDuplicate2([1,2,3,1]))
print(False==TestCase.containsDuplicate2([1,2,3,4]))
print(True==TestCase.containsDuplicate2([1,1,1,3,3,4,3,2,4,2]))

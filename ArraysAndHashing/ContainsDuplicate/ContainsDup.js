/**
 * @param {number[]} nums
 * @return {boolean}
 */
var containsDuplicate = function (nums) {
    const count_occurrences = new Map()
    for (let num of nums) {
        count_occurrences.set(num, (count_occurrences.get(num) || 0
        ) + 1);
    }
    if (Object.values(count_occurrences).includes(2)) {
        return true;
    }
    return false;
};

console.log(containsDuplicate([1, 2, 3, 4]))

//optimized solution
// time complexity of O(n) as creating a set and checking size has linear time complexity

/**
 * @param {number[]} nums
 * @return {boolean}
 */
var containsDuplicate2 = function (nums) {
    const unique = new Set(nums)
    return unique.size !== nums.length
};

console.log(true == containsDuplicate2([1, 2, 3, 1]))
console.log(false == containsDuplicate2([1, 2, 3, 4]))
console.log(true == containsDuplicate2([1, 1, 1, 3, 3, 4, 3, 2, 4, 2]))

/*

var containsDuplicate = function(nums) {
    const uniqueNums = new Set();
    for (let num of nums) {
        if (uniqueNums.has(num)) {
            return true;
        } else {
            uniqueNums.add(num);
        }
    }
    return false;
};

can use .has in order to return boolean as to whether a set has or doesn't have a value
.add to add to a set
*/

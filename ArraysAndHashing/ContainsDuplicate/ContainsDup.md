# 06/14/23 - 217

## [Contains Duplicate](https://leetcode.com/problems/contains-duplicate/)

### Problem
> Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct.

- Input: Array that contains integer elements
- Output: Return boolean of either true or false
    - True if any value appears at least twice in array
    - False if every element is unique

- Algo Process
    - Restate Problem
        - If a number occurs twice in array return True, if a number never reoccurs return False
    - Goal of Function
        - Evaluate if an array contains duplicates or not
    - Types
        - Input is array, Output is a boolean
    - Assertions
        - Every element in array is an integer, array length does not change, wont be given an empty array
    - Edge Cases
        - array that has only duplicates
            - [1,1,1,1,1,1]
        - array that has non-integer elements
        - X (array that is empty)

#### Example \#1
```
Input: nums = [1,2,3,1]
Output: true
```

#### Example \#2
```
Input: nums = [1,2,3,4]
Output: false
```

#### Example \#3
```
Input: nums = [1,1,1,3,3,4,3,2,4,2]
Output: true
```

#### Constraints
- `1 <= nums.length <= 105`
- `-109 <= nums[i] <= 109`

### Preliminary Solution
- __Time Complexity__: O(n)
- __Space Complexity__: O(n)

#### _Backside_
- Initial Approach to this problem would involve iterating through each number in the given nums array, store number and its number of occurrences as key and value -> If a 2 exists in dictionary then return true else every element has a 1 then return false

- More optimized approach is to create a set from the input array which contains only non duplicate elements -> If the set is not equal to the input return true, else false

#### Additional Details
- Alt Solution
    - Then establish a conditional that evaluates the length of the set object with the length of the input array
    - If those two are not equal, we know that there are duplicates so return True, else return False meaning there is only unique elements in the input array

#### Solution Code
- [Java Script](ContainsDup.js)
- [Python](ContainsDup.py)

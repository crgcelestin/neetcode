/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function (nums, target) {
    const remainder = {}
    for (const [index, element] of nums.entries()) {
        if (remainder[element] !== undefined) {
            return [remainder[element], index]
        } else {
            remainder[target - element] = index
        }
    }
    // return empty array if no matches are found
    return [];
};

console.log(JSON.stringify(twoSum([1, 2, 3], 3)) == JSON.stringify([0, 1]))
console.log(JSON.stringify(twoSum([2, 7, 11, 15], 9)) == JSON.stringify([0, 1]))
console.log(JSON.stringify(twoSum([3, 2, 4], 6)) == JSON.stringify([1, 2]))
console.log(JSON.stringify(twoSum([3, 3], 6)) == JSON.stringify([0, 1]))

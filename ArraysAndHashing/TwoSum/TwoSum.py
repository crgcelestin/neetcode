class Solution:
    def twoSum(self, nums: list[int], target: int) -> list[int]:
        tracker={}
        for index, element in enumerate(nums):
            if element in tracker:
                return [tracker[element], index]
            else:
                tracker[target-element]=index
        return []
# Test Cases
TestCase = Solution()
print(TestCase.twoSum([1, 2, 3], 3)==[0,1])
print(TestCase.twoSum([2,7,11,15], 9) == [0, 1])
print(TestCase.twoSum([3,2,4], 6) == [1, 2])
print(TestCase.twoSum([3,3], 6) == [0, 1])

# LC Solution
# Optimal in terms of memory
'''
class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        for i in range(len(nums)):
            rest = target - nums[i]
            for j in range(i+1, len(nums)):
                if nums[j] == rest:
                    output = [i,j]
        return output
'''

# 06/14/23 - 1

## [Two Sum](https://leetcode.com/problems/two-sum/)
### Problem
> Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

> You may assume that each input would have exactly one solution, and you may not use the same element twice.

> You can return the answer in any order.

### Analysis
- Input: Array of numbers and a target number
- Output: Indices of two nums that add to target (returned in any order)

- Algo Process
    - Restate Problem
        - We are given a list of numbers and a target value, we have to develop an algo that allows us to know where those two numbers that add up to target are in the list
    - Goal of Function
       - Return two indices representing location of numbers in list that add up target and output them in any order
    - Types
       - input contains an array with type integers as elements, target is an integer
    - Assertions
        - There is only one solution as it pertains to indices of numbers that can add up target, same element can't be used twice meaning if the target is 20 and we have 10 in our list, it can't be used twice
        - Indices answer can be returned regardless of what number is encountered first
    - Edge Cases
        - Array that is empty, an array that contains elements where none are = to the target when summed together, an array that contains the same number duplicated, 

#### Example \#1
```
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
```

#### Example \#2
```
Input: nums = [3,2,4], target = 6
Output: [1,2]
```
#### Example \#3
```
Input: nums = [3,3], target = 6
Output: [0,1]
```

#### Constraints
 - `2 <= nums.length <= 104`
- `-10**9 <= nums[i] <= 109`
- `-10**9 <= target <= 109`

### Preliminary Solution
- __Time Complexity__: O(n)
    - Solution I provided strongly optimized for time complexity
- __Space Complexity__: O(n)

#### _Backside_
 - Initial Approach
    - Test for initial constraints
    - Initialize a map/dict structure in order to store elements as keys, index as values
    - as we iterate through nums traversing both values and indices, we want to evaluate if our given element is in the tracker 
        - if it is in tracker, return the value(index) of that element in tracker, and the index of the current element
        - else we want to store the difference between the target and that element in tracker as a key and store the index as the value
    - if we don't have two numbers that equal to target just return an empty array
    

#### Additional Details
- [LC Notes]

#### Solution Code
[JS Solution](TwoSum.js)
[Python Solution](TwoSum.py)

#### Notes
- Anything additional goes here in bullets

### [Best Available Solution](SOLUTION_LINK)
- __Time Complexity__: O(n^2)
- __Space Complexity__: O(1)

#### _Backside_
Leetcode Solution:
    - iterate through nums using range function
    - store in a rest variable the difference between the target and the current number
    - with a second pointer, iterate through nums starting from 1 after 0th index
        - if the current element stored by second pointer is equal to rest i.e the desired second number, then output the indices stored by the pointers in an array

#### Solution Code
[LC Solution](TwoSum.py)

#### Notes
- Sacrifices time for space complexity efficiency

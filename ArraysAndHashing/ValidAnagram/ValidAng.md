# 06/14/23 - 242

## [Valid Anagram](https://leetcode.com/problems/valid-anagram/)

### Problem
> Given two strings s and t, return true if t is an anagram of s, and false otherwise.

> An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.

### Analysis
- Input: Two strings s and t
- Output: Return boolean of either true or false
    - True if t is an anagram of s
    - False if t is not an anagram of s

- Algo Process
    - Restate Problem
        - if it is possible to make t out of the characters in s -> return true
        - if it is not possible to make t ouf ot the chars in s -> return false
    - Goal of Function
        - Return boolean to evalute if the string t can be made from chars in s
    - Types
        - Input is two strings, output is boolean
    - Assertions
        - Every char/element contained in both strings are characters and aren't unexpected symbols/elements, all characters in the two strings are lowercase (do we care about capitalization?), if they are to be anagrams they must be = in length
        - each character in s can only be used once for t
    - Edge Cases
        - s and t are not equivalent in length, s is empty, t is empty, s or t contains unexpected elements in their strings

#### Example \#1
```
Input: s = "anagram", t = "nagaram"
Output: true

```

#### Example \#2
```
Input: s = "rat", t = "car"
Output: false
```

#### Constraints
- `1 <= s.length, t.length <= 5 * 104`
- s and t consist of lowercase English letters.

### Preliminary Solution
- __Time Complexity__: O(n)
- __Space Complexity__: O(n)

### CODE
- [Python](ValidAng.py)
- [JavaScript](ValidAng.js)

#### _Backside_ and Additional Details
- Initial Solution
    - Naive approach: (s iteration) create an array that contains all the letters of s via push, then pop (t iteration) from s created array and return true if array that is being popped from after t iteration has a length of 0

    - Time Complexity: O(s*t)
        - Worst case, they aren't anagrams, check each string
    - Space Complexity: O(s)
        - Worse case, the aren't a match, then nothing is popped off

- Alt Solution
    - Check for initial constraints, length of s and t have to be the same
    - initialize an array to represent all alphabet chars(26) with 0
    ```
        char_count = [0] * 26
    ```
    - map with constant of ord(a) subtracted from each ord in s and t, iterate through both strings, increment at index if char exists in s and decrement at index if char exists in t
        - if t == s meaning they are anagrams of each other, then each element should be 0
    - iterate through the array with values being either 0, 1, -1
    - if there is any instance of a non-zero element, return False i.e they are not anagrams, else return True they are anagrams

- ### LC SOLUTION
 ![LC Solution](Images/isAnagram.png)

- LC NOTES
    - First make sure that input s and t pass constraints,
        - length of string s must be greater than 1 and length of string t must be less than 5 times 10 raised to the 4th power

    - Secondly, s and t must be = in length, if they are not then return false

    - Now if they pass both constraints and are equal in length

    - Tc: O(s*t), Sc: O(s)

#### Solution Code
- [Java Script](ValidAng.js)
- [Python](ValidAng.py)

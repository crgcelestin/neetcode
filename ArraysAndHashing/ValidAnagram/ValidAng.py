class Solution:
    def isAnagram(self, s: str, t:str) -> bool:
        if len(s)!=len(t):
            return False
        char_list=[]
        for char in s:
            char_list.append(char)
        for char1 in t:
            if char1 in char_list:
                char_index=char_list.index(char1)
                char_list.pop(char_index)
        if len(char_list)==0:
            return True
        return False

# Test Cases
TestCase=Solution()
print(True==TestCase.isAnagram('anagram','nagaram'))
print(False==TestCase.isAnagram('rat','car'))

# Optimized Solution
class Solution2:
    def isAnagram(self, s: str, t: str) -> bool:
        if len(s) != len(t):
            return False

        # quick check, anagram strings must be the same length

        char_count = [0] * 26

        # initialize list 'char_count' of size 26 to represent count of each char in input strings, allocate array with 26 elements
        # each element represents A-Z

        for i in range(len(s)):
            char_count[ord(s[i]) - ord('a')] += 1
            char_count[ord(t[i]) - ord('a')] -= 1

        # iterate through each char in s and t, for each char increment count at corresponding index based on ascii value - ascii 'a' value
        # allows for mapping of each chart to index in list

        for count in char_count:
            if count != 0:
                return False
        return True

        # after iteration in both strings, if there is any instance of a non-zero count, there are thus difference in char frequencies between both strings
        # Return False if there is a diff in frequencies else return True

'''
TC is O(s) linear, s being input string length assuming s === t can be O(s), O(t)

SC is O(1) as char_count is a 26 element fixed array, independent of input size
'''

# Test Cases
TestCase=Solution2()
print(True==TestCase.isAnagram('anagram','nagaram'))
print(False==TestCase.isAnagram('rat','car'))

// Brute Force Approach 1
function isAnagram_naive(s, t) {
    if (s.length != t.length) {
        return false
    }
    const char_list = []
    for (let char of s) {
        char_list.push(char)
    }
    for (let char1 of t) {
        if (char1 in char_list) {
            let char_index = char_list.indexOf(char1)
            char_list.pop(char_index)
        }
        if (char_list.length === 0) {
            return true
        }
        return false
    }
}
// Brute Force Approach 2
function isAnagram(s, t) {
    if (s.length != t.length) {
        return false
    }
    const char_count = Array(26).fill(0)
    for (let i in s) {
        char_count[s.charCodeAt(i) - 'a'.charCodeAt(0)] += 1
        char_count[t.charCodeAt(i) - 'a'.charCodeAt(0)] -= 1
    }
    for (let count of char_count) {
        if (count !== 0) {
            return false
        }
    }
    return true
}

// GTP AIDED using Python solution with .count
function isAnagram(s, t) {
    if (s.length !== t.length) {
        return false;
    }

    const charCount = new Map();

    for (let i = 0; i < s.length; i++) {
        const char = s[i];
        charCount.set(char, (charCount.get(char) || 0) + 1);
    }

    for (let i = 0; i < t.length; i++) {
        const char = t[i];

        if (!charCount.has(char) || charCount.get(char) === 0) {
            return false;
        }

        charCount.set(char, charCount.get(char) - 1);
    }

    return true;
}

console.log(true == isAnagram('anagram', 'nagaram'))
console.log(false == isAnagram('rat', 'car'))

// Explanation
/*
    Here's how the code works:

    The function begins by checking if the lengths of s and t are not equal. If they have different lengths, it immediately returns false because two strings with different lengths cannot be anagrams.

    The function creates a new Map object called charCount, which will be used to keep track of the count of each character in s.

    It then iterates through the characters of s using a for loop. For each character, it performs the following steps:

    It retrieves the current count of the character from the charCount map using charCount.get(char).
    If the count is undefined (meaning the character is not yet present in the map), it sets the count to 0 using (charCount.get(char) || 0).
    It increments the count by 1 and sets the updated count in the charCount map using charCount.set(char, (charCount.get(char) || 0) + 1).
    After this loop, the charCount map will contain the counts of each character in s.

    The function then iterates through the characters of t using another for loop. For each character, it performs the following steps:

    It checks if the character is not present in the charCount map (!charCount.has(char)) or if the count of the character is already 0 (charCount.get(char) === 0).
    If either of the above conditions is true, it means the current character in t is not present in s or its count has been exhausted, so it returns false.
    Otherwise, it decrements the count of the character in the charCount map by 1 using charCount.set(char, charCount.get(char) - 1).
    If this loop completes without returning false, it means all characters in t have corresponding characters in s with the same count.

    Finally, the function returns true, indicating that s and t are anagrams.

*/

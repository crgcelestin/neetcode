# 08 - 18 -2023

## [128. Longest Consecutive Sequence](https://leetcode.com/problems/longest-consecutive-sequence/)

### Problem
- Given an unsorted array of integers `nums`, return length of longest consecutive elements sequence
- Write algo that runs in O(n) [TC]

### Analysis
- Input: array of integers, list[int]
- Output: integer, int

#### Algo Process
- Restate Problem
    - essentially we are given an array of random numbers that aren't sorted and we are asked to write an an algorithm that will take said unsorted array -> sort (intermediate process) -> return a integer that communicates the length of an array that contains the elements in sequence

- Goal of Function
    - so we are given a list like [10,5,0,4,1,2,3] -> the sorted consecutive elements array is arr=[0,1,2,3,4,5] -> return len(arr), which is 6

- Types
    - integers, list

- Assertions and Assumptions
    - the assertion is that we are only given integers and no empty spaces or characters or non-integer types
    - ensured to be given some sequential order to be made(?)
    - we may have to define an intermediate array containing a potential sequential sorted order

- Edge Cases
    - empty input array, array containing all non expected types
    - repeated integers array
    - array with no sequential order possible
    - can also have multiple sequential sequences in an array
        * example being [1,2,3,6,7,8]

#### Example \#1
```py
input = [100, 4, 200, 1, 3, 2]
output = 4
'''
longest consecutive sequence is [1,2,3,4]
'''
```

#### Example \#2
```py
input = [0,3,7,2,5,8,4,6,0,1]
output = 9
```
#### Example \#3 & \#4
```py
input3 =  [0,0,1 ]
output = 2

input4 = [ 1,8,5 ]
output = 0
```

#### Constraints
- `0 <= nums.length <=10^5`
- `-10^9 <= nums[i] <=10^9`

### Preliminary Solution
- __Time Complexity__:

- __Space Complexity__:

#### _Backside_
- My initial solution would involve:
    - in order to determine if there is a sequential I need to find the minimum integer in a list and determine if there is a directly sequential number after it
    - albeit, that approach has a deficiency esp with examples like [1,4,5,6,]
    - so what we should do is iterate and find min, if there is a next sequential then pop min and go to next potential min, if there is sequential continue and append numbers
    - else break and return length of sequential

- Second attempt:
    1. ask if nums is empty, if so return 0 as there are no possible sequences
    2. then we initialize a nums_set that contains unique elements in nums array, longest_streak storing the longest streak of consecutive #s encountered
    3. iterate through nums_set
        1. if there is a present prior number to the number we start at in nums_set
            1. set current_num to be num
            2. add to number to return length of current consecutive streak
        2. if given the current_num if we detect next consecutive
            1. increment current, add to current_streak
        3. if we reach the end and find that our current streak is greater than longest i.e. updating value to be returned
            1. update if
        4. return longest


#### Additional Details
- Reflecting on First Attempt:
    * First attempt was quite developed, but wrong. My thought process is that we can initialize a pointer starting at first element, 0 index and an empty array to be returned
    * define a helper function,
        - takes in three parameters,
        - Then we ask * while our pointer is still in bounds and not the current minimum* is the number at the pointer index the next sequential after the minimum
            - if it is then append that min and the next number to the list
                - pop min, move pointer, recursive call
            - else pop min, move pointer, recursive call
    > - essentially i made a function that gives me stack overflow
    > - approached seemed valid

    * Corrected attempt but it gives me 1 more than i want in cases even twice the desired number
        - moving on to correct implementation

- Reflecting on Second Attempt:
    - this solution is closer to the ideal with the idea that we decipher more in regards to returning a consecutive sequence
    - use set to get only uniques req to ignore duplicates that would skew the returned integer length of longest consecutive sequence
        - then at start element, ask if there are any prior in order to update current num, if there after then continue to add to number and streak
        - update streak vars
    - this solution however failed on lc with a larger input test case, several integers long

#### Solution Code
[JS Solution]()
[Python Solution](longest.py)

### [Neetcode](https://www.youtube.com/watch?v=P6RZZMu_maU)
- [Neetcode Solution](nc.py)

#### TC, SC
- Run in O(N) time complexity

- #### NC Notes
* imagine problem by drawing out imaginary number line with nums placed on it to recognize patters
    - [100,4,200,1,3,2]
    ```
    1,2,3,4  100 200
    <---------------->
    ```
    * first seq has len 4, 2nd has len 1, 3rd has len 1
    - detecting sequences, each has start value
        - look at left neighbor, start does not have a left
        - check set does it have prior, no
    - 100 does not have left neighbor, does 101 exist no (extent of seq)
    - 4 not start (has left)
    - 200 (no left, no 199 -> no 201, extent of sequence)
    - 1 has no left it is start, 2 exists then extend seq, 3 exists then extend, 4 exists extend
    - is 3 start no as it has left, same with 2

##### Implementation
- initialize set of input array
- set a longest to store len of longest seq
- iterate through nums
    - check if start of sequence via left neighbor check
    - check n+length as that checks current #
        * as length grows check more consecutive numbers
    - at end we couldve found longest, so update longest so taking max of length computed and what longest was
- return longest

class Solution:
    def longestConsecutive(self, nums:list[int]) -> int:
        numSet=set(nums)
        longest=0
        for n in nums:
            print(f'current {n}')
            if (n-1) not in numSet:
                length=0
                while (n+length) in numSet:
                    length+=1
                    print(length)
                print(f'{longest}')
                if length>longest:
                    longest=length
        return longest

TestW=Solution()
input0 = [100, 4, 200, 1, 3, 2]
input1 = [0,3,7,2,5,8,4,6,0,1]
input2 = [0,0,1]
input3 = [1,8,5]

print(
    TestW.longestConsecutive(
        input0
    ) == 4
)
print(
    TestW.longestConsecutive(
        input1
    )== 9
)

print(
    TestW.longestConsecutive(
        input2
    ) == 2
)

print(
    TestW.longestConsecutive(
        input3
    ) == 1
)

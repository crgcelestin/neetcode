class Solution:
    def longestConsecutive(self, nums: list[int]) -> int:
        seq= []
        def return_next(nums, seq):
            if not nums:
                return 0
            min_num=min(nums)
            if min_num+1 in nums:
                seq.append(min_num)
                seq.append(min_num+1)
                nums.remove(min_num)
            else:
                nums.remove(min_num)
            return return_next(nums, seq)
        return_next(nums, seq)
        return len(seq)

Test1=Solution()

# # fail
# print(Test1.longestConsecutive(
#     [ 1,4,5 ]
# ))

# # fail
# print(Test1.longestConsecutive(
#     [0,0,1 ]
# ))
# # passed
# print(Test1.longestConsecutive(
#     [100, 40, 200, 1, 3, 2]
# ))
# # Fail
# print(Test1.longestConsecutive([0,3,7,2,5,8,4,6,0,1]))

class SolutionW:
    def longestConsecutive(self, nums:list[int]) -> int:
        if not nums:
            return 0
        unique=set(nums)
        longest_s=0
        for num in unique:
            if num-1 not in unique:
                current=num
                current_s=1
                while current+1 in unique:
                    current+=1
                    current_s+=1
                if current_s>longest_s:
                    longest_s=current_s
        return longest_s

TestW=SolutionW()
input0 = [100, 4, 200, 1, 3, 2]
input1 = [0,3,7,2,5,8,4,6,0,1]
input2 = [0,0,1 ]
input3 = [1,8,5]

print(
    TestW.longestConsecutive(
        input0
    ) == 4
)
print(
    TestW.longestConsecutive(
        input1
    )== 9
)

print(
    TestW.longestConsecutive(
        input2
    ) == 2
)

print(
    TestW.longestConsecutive(
        input3
    ) == 1
)

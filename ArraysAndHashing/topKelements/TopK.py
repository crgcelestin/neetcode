'''
Approach would be to create a frequency counter using a hashmap
Then grab k most frequent numbers from frequency counter

(1) iterate through and add to counter
(2) frequency counter?
    counter = {
        1: 3,
        2: 2,
        3: 1
    }
    and if given k=2, return the 2 largest in this counter
  (3)have a counter that either decrements from k to 0, or a counter that increments from 0 to k
        while counter<k:
            <code>
           counter++
  (4) append max through each iteration, delete max following the appending to modes (perform k times)
  for key, value in counter:
        if counter[key]===max of counter
          append key to modes array
          delete max key from counter
  return modes

potential implementation
(1) initialize an empty list called modes
(2) using dict, store int as key, frequency as value
(3) after gaining frequency count, then use a while loop
(4) while a counter initialized as 0 is less than input k -> increment during loop
(5) find max regarding frequency -> max function used with key param set to tracker.get / Max key is determined based on corresponding value as max() applies tracker.get to each key to return values associated with key in dictionary
-> Then we are deleting that max key determined by value from tracker for unique considerations
'''
class Solution:
  def TopK(self, nums, k):
    counter=0
    tracker={}
    modes=[]
    for num in nums:
      tracker[num]=1+tracker.get(num, 0)
    while counter<k:
      # sub for max()
      '''
      max_value=0
      for key, value in tracker.items():
        if value>max_value:
            max_key=key
            max_value=value
      '''
      max_key=max(tracker, key=tracker.get)
      modes.append(max_key)
      del tracker[max_key]
      counter+=1
    return modes


Test=Solution()
print(Test.TopK([1,1,1,2,2,3], 3)==[1,2,3])
print(Test.TopK([1],1)==[1])

'''
In regards to time complexity this solution has O(n*k), n being the array length and k being the desire number of frequent numbers
    - Worst case, find max key in count_dict takes O(n) time to search through all keys, loop iterates k times

Space req is proportional to # of unique elements in array, thus sc is O(N) storing counts of each element in array
'''

# practice
class Solution:
  def topK(nums,k):
    count={}
    freq=[[] for i in range(len(nums)+1)]
    for num in nums:
      count[num]=1+count.get(num, 0)
    for n, c in count.items():
      freq[c].append(n)
    res=[]
    for num in range(len(freq)-1,0,-1):
      for ele in freq[num]:
        res.append(ele)
        if len(res)==k:
          return res

print(Solution.topK([1,2,3,1],2))

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var topKFrequent = function (nums, k) {
    let counter = 0
    const tracker = {}
    const modes = []
    nums.forEach(num => {
        tracker[num] = (tracker[num] || 0) + 1;
    });
    while (counter < k) {
        let max_value = 0
        let max_key = 0
        for (const [key, value] of Object.entries(tracker)) {
            if (value > max_value) {
                max_key = key
                max_value = value
            }
        }
        modes.push(max_key)
        delete tracker[max_key]
        counter += 1
    }
    return modes
};

/*

    Tc:
        (1) Loop through every num in nums is O(n)
        (2) Find top k frequent element in while loop with nested for loop
            O(k), O(n) [worst case each element is unique]
        (2 - overall) worst time tc is O(n^2) with k being close to n [n being length of array], search through entire tracker object to find element with max frequency

    Sc:
        Overall is O(n), worst case all elements being unique and array returned has n elements


*/
console.log(topKFrequent([1, 2, 3, 3], 2))

// minHeap alt - need to revisit
class MinHeap {
    constructor() {
        this.heap = [];
    }

    insert(val) {
        this.heap.push(val);
        this.bubbleUp(this.heap.length - 1);
    }

    bubbleUp(index) {
        const parentIdx = Math.floor((index - 1) / 2);
        if (parentIdx >= 0 && this.heap[index].count < this.heap[parentIdx].count) {
            [this.heap[index], this.heap[parentIdx]] = [this.heap[parentIdx], this.heap[index]];
            this.bubbleUp(parentIdx);
        }
    }

    extractMin() {
        const min = this.heap[0];
        const last = this.heap.pop();
        if (this.heap.length > 0) {
            this.heap[0] = last;
            this.bubbleDown(0);
        }
        return min;
    }

    bubbleDown(index) {
        const leftIdx = 2 * index + 1;
        const rightIdx = 2 * index + 2;
        let smallestIdx = index;

        if (leftIdx < this.heap.length && this.heap[leftIdx].count < this.heap[smallestIdx].count) {
            smallestIdx = leftIdx;
        }

        if (rightIdx < this.heap.length && this.heap[rightIdx].count < this.heap[smallestIdx].count) {
            smallestIdx = rightIdx;
        }

        if (smallestIdx !== index) {
            [this.heap[index], this.heap[smallestIdx]] = [this.heap[smallestIdx], this.heap[index]];
            this.bubbleDown(smallestIdx);
        }
    }
}

const topKFrequent = function (nums, k) {
    const counter = {};
    nums.forEach(num => {
        counter[num] = (counter[num] || 0) + 1;
    });

    const minHeap = new MinHeap();
    for (const num in counter) {
        minHeap.insert({ num: parseInt(num), count: counter[num] });
        if (minHeap.heap.length > k) {
            minHeap.extractMin();
        }
    }

    const result = [];
    while (minHeap.heap.length > 0) {
        result.push(minHeap.extractMin().num);
    }

    return result.reverse();
};

console.log(topKFrequent([1, 2, 3, 3], 2)); // Output: [3, 1]

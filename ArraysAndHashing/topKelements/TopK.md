# 06/29/23 - #347: Top K Frequent Elements
! NEED TO REVIEW !
## [Top K Frequent Elements](https://leetcode.com/problems/top-k-frequent-elements/)

### Problem
> - Given an integer array `nums ` and integer `k`, return `k` most frequent elements
> - Can return the answer in any order

### Analysis
- Input: array of integers ( nums: list[ints] ), integer ( k )
- Output: return the k most frequent elements, i.e given list of integers and ask to return 2 most frequent -> return 2 most frequent integers in an array regardless of order of appearance or numerical sort

- Algo Process
    - Restate Problem
        - given an array of integers as well as int k, return k number of frequent integers (n=number, k=frequencies - starting from most frequent) [n_(k-1),...,n_(k-k)] (entities) i.e frequency
            - [1,1,2,2,3], k=2
            - return [1,2]

    - Goal of Function
        - Function must return an array that has k number of elements of most frequent numbers

    - Types
        - int (k), list[int] (input, output)

    - Assertions
        - k is in range of array, ie there won't be an out of bonds regarding the provided array
            - [1,2,3], k has to be <=3, answer is guaranteed to be unique
        - time complexity needs to be better than O(n log n)

    - Edge Cases
        - empty input array, k not provided, array can potentially have strings, if  user inputs k=0 or no k -> should return an empty array

#### Example \#1
```
Input: nums=[1,1,1,2,2,3], k=2
Output: [1,2]
```

#### Example \#2
```
Input: nums=[1], k=1
Output: [1]
```

#### Constraints
- `1 <= nums.length <= 10^5`
- `-10^4 <= nums[i] <= 10^4`
- `k` is in range `[1, number of unique array elements]`
- Answer guaranteed to be unique

### Preliminary Solution
(python)
- __Time Complexity__: Algo TC must be better than O(n log n), n being the array size - O(n*k)

- __Space Complexity__: O(n)

(javascript)
- __Time Complexity__: O(n^2)
- __Space Complexity__: O(n)

#### Additional Details
- [LC Notes]
    - Solution submitted: [First attempt](TopK.py)
    - Performed well in terms of less memory but had a slow runtime
    - Apparently a minHeap (Priority Queue) can be used in order to keep track of k most frequent elements more efficiently (will have to revisit)

#### Solution Code
- [JS Solution](TopK.js)
- [Python Solution](TopK.py)


### [Neetcode](https://www.youtube.com/watch?v=YPTqKIgVk-k)
- [Neetcode Solution](NC_TopK.py)

- #### Notes
    - Array will always be non empty
        * [1,1,1,2,2,3] where 1->3, 2->2, 3->1
    - Count number of occurrences ^ and add each pair to max Heap with key be # of occurrences then pop from heap k times
        - Use Heapify that will add to heap in O(n) time with each pop taking log n time so O(k log n)
    - Naive: Values in an array and then using buckets to store values via indices correlating to a number
    ```
    [1,1,1,2,2,100]
    |0|1|2|...|100|
    | |3|2|...|1|
    ```
    -  Naive approach using Bucket Sort doesn't take into account that array can be unbounded i.e. lowest value being 1 and greatest being 1,000,000
        - Naive would have resulted in an array with buckets would be unbounded, and doesn't reveal top k most frequent elements

    - Potential alt solution that has tc of O(n) and sc of O(n)
        - Can be solved in linear time using Bucket Sort
            - Alt approach
                * Frequency is mapped to the index, and values store a list of which values have the specified index
                * Length of collection of buckets is proportional to size of input array
                * Size of resulting array is k where [k<n] so after looping through freq array of n i.e performing an operation of slotting in number while iterating through array with n items
            ```
            [1,1,1,2,2,100]
            |0|1|2|...|100|
            | |3|2|...|1|
            ```

- __Time Complexity__: O(n)
    - iterate through nums array of length n in order to derive frequencies for count, O(n)
    - freq list stores elements at indices of frequencies with length O(n+1) simplifies to O(n)
    - res list stores at most k elements, so it is bounded by O(k)
    - Overall TC being O(n)
- __Space Complexity__: O(n)
    - Arrays have upper bound that increases with greater n, worst case k is the same length as array?

class Solution:
    def topKFrequent(self, nums:list[int], k: int)-> list[int]:
        # hash map counts # frequencies of each value
        count={}
        # index = count of element, value is list of values that have frequencies that match index
        # empty array with list of len(nums)+1
        freq= [[] for i in range(len(nums)+1)]
        for n in nums:
            count[n]=1+count.get(n, 0)
        # get back number and count via .items
        # append numbers via index c amount of times
        for n, c in count.items():
            freq[c].append(n)

        '''
        initialize a res array that will return top k frequent elements
        start from end as we know indices are representative of occurences and walk backwards, append each going backward until the length of that resulting array is equal to k, then return resulting array storing numbers indexed by frequencies
        '''
        res=[]
        for i in range(len(freq)-1, 0, -1):
            for n in freq[i]:
                res.append(n)
                if len(res)==k:
                    return res

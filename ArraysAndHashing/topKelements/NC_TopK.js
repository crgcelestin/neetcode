/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var topKFrequent = (nums, k) => {
    let count = new Map()
    /*
    previous approach
    freq = Array(nums.length+1).fill([])

    leads to js incorrectly creating a single array that is filled with reference to the same array instance

    below is correct approach
    */
    let freq = Array(nums.length + 1).fill(null).map(() => []);
    for (let num of nums) {
        count.set(num, (count.get(num) || 0) + 1);
    }
    for (const [num, c] of count.entries()) {
        freq[c].push(num);
    }
    let res = []
    for (let i = freq.length - 1; i > 0; i--) {
        for (let n of freq[i]) {
            res.push(n)
        }
        if (res.length === k) {
            return res
        }
    }

};

console.log(topKFrequent(
    [1, 2, 3, 1, 1, 2], 3
))

/*
    var function_name = function() {
    }
*/

class Solution:
    def encode(self, strs: list[str]) -> str:
        res = ""
        for s in strs:
            res+= str(len(s)) + "#" + s
        return res

    def decode(self, str: str) -> list[str]:
        res, i = [], 0
        while i<len(str):
            j=i
            while str[j]!="#":
                j+=1
            length=int(str[i:j])
            res.append(str[j+1:j+1 + length])
            i = j + 1 + length
        return res

input1 = ["lint","code","love","you"]
Test1 = Solution()
output1=Test1.encode(input1)
print(output1)
output2=Test1.decode(output1)
print(output2)

print(output2==input1)

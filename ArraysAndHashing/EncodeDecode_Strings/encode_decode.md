# 08/16/23

## [659 · Encode and Decode Strings](https://www.lintcode.com/problem/659/)

### Problem
- Design an algo
    1. Encode a list of strings -> (encode) -> encoded string
    2. encoded string is sent over network
    3. encoded string -> (decode) -> original list of strings

### Analysis
- Input:
    * first function called encode takes in a list of strings then outputs a str
        - input1: list[str]
    * second function called decode takes in a string then outputs a list of strings
        - input2: str

- Output:
    * encode outputs a 'encoded' string
        - output1: str
    * decode performs decoding of encoded string and outputs decoded list of strings
        - output: list[str]

- Algo Process
    - Restate Problem
        * implement algorithm where there is one encode fxn that performs encoding of a list of strings to a string that the decode fxn can then decode

    - Goal of Function
        * we have two functions that when paired together provide an encoding mechanism similar to a sha, encryption method that involves some consistent 'hash'

    - Types
        * list of strings, string

    - Assertions and Assumptions
        * we assume that types will be enforced in algo design -> we will only have inputs and output that range from two being str, list[str]
        * encode and decode functions have to be inverse and maintain data integrity

    - Edge Cases
        * empty inputs
        * integers being inputted
        * change in input, output between encode and decode function skewing function

    - Note
        * In the examples, a encode method provided seems to be
        ` str1 + ':;' + str2 + :; + ... strn`


#### Example \#1
```py
input1 = ["lint","code","love","you"]
output1 = ["lint","code","love","you"]
# One possible encode method is: "lint:;code:;love:;you"
```

#### Example \#2
```py
input2 = ["we", "say", ":", "yes"]
output2 = ["we", "say", ":", "yes"]
# One possible encode method is: "we:;say:;:::;yes"
```

### Preliminary Solution
- __Time Complexity__: O(n) as you iterate through a string list that has n elements
- __Space Complexity__: O(n) as you are creating a new string in the encode method with concatenated strings and then decoding it  to an array of strings, with space being directly proportional to the string length

#### _Backside_
- My initial solution would involve:
    * First figure out a method where we encode, converting a list to a str can just be string concatenation
        - str + ... + str while iterating in the list

    * converting str to list is just using list.split(delimiter)

- Second attempt:


#### Additional Details
- Reflecting on First Attempt
    * worked pretty well this is most likely the easies lc question i have solved

- Reflecting on Second Attempt:
    * Can use pairing of .join and .split
        - given delimiter of x, perform strs.join(x) then str.split(x)

#### Solution Code
[JS Solution]()
[Python Solution](encode_decode.py)


### [Neetcode](./nc.py)
- [Neetcode Solution](https://www.youtube.com/watch?v=B1k_sxOSgv8)
##### Thoughts
- the nc solution is more sensical with consideration being made to that possibility that delimiters occur in the middle of strings and allows for communication of string character length
- good tc and sc as well

- #### NC Notes
    - Given an input of list strings -> string with decode being the reverse
    - in first function, can have any characters in input
        - how do we create a delimiter that allows us to detect where words begin and end
        - having a 1 char delimiter is not good as it may potentially show in one of the words
    - good solution allows us to track occurrences and characters per word in list
        - an example being `4#neet5#co#de` (where its count of characters, delimiter, word) where `neet` is the first word with 4 chars followed by `co#de` which is 5 chars

    - Solution Explanation
        1. encode
            - initialize empty string for return prod -> iterate through string
                - then while iterating append to empty string `string length` + `#` + `string`
            return that end product
        2. decode (given single encoded string)
            * create empty list, pointer
            * iterate char by char while staying in bounds
                1. then create a second pointer with initial position at i
                - first conditional is going to be the circumstance of detecting an integer i.e number prior to delimiter that signifies a given string length
                        - increment until we hit pound symbol
                    1. once we get to pound char, character length is going to be equivalent to a subsection of string starting from i and ending at j
                    2. length var tells how many positions we have to read to decode original string at a given position
                    3. actual string is equivalent to the section of str starting from after j till the index that is the additive sum of the index after j and length
                    4. finally updated pointer at end, i will now start at index that is the index equivalent to the last element in the previous deciphered string
            * return strings decoded

#### TC, SC
- TC: O(n) iterate through total chars in list of words that is n words long
- SC: O(n) with strings, array space reqs proportional to length of input list with elements n

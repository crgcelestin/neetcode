class Solution:
    """
    @param: strs: a list of strings
    @return: encodes a list of strings to a single string.
    """
    def encode(self, strs: list[str]) -> str:
        encode_str = ""
        for i in range(0,len(strs),1):
            if i == len(strs)-1:
                encode_str+=strs[i]
            else:
                encode_str+= f'{strs[i]}:;'
        return(encode_str)


    """
    @param: str: A string
    @return: decodes a single string to a list of strings
    """
    def decode(self, str:str) -> list[str]:
        return(str.split(':;'))

input1 = ["lint","code","love","you"]
Test1 = Solution()
output1=Test1.encode(input1)
print(output1)
output2=Test1.decode(output1)
print(output2)

class Solution2:
    def encode(self, strs: list[str]) -> str:
        dele = '#'
        return dele.join(str)
    def decode(self, str:str) -> list[str]:
        dele = '#'
        return str.split(dele)

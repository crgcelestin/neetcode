/**
 * @param {number[]} nums
 * @return {number[]}
 */
var productExceptSelf = function (nums) {
    const res = Array(nums.length).fill(1)
    console.log(res)
    let prefix = 1
    for (let i = 0; i < nums.length; i++) {
        res[i] = prefix
        prefix *= nums[i]
    }
    let postfix = 1
    for (let j = nums.length - 1; j > -1; j--) {
        res[j] *= postfix
        postfix *= nums[j]
    }
    return res
};

console.log(productExceptSelf([1, 2, 3, 4]))

# 07/31/23

## [238.Product of Array Except Self](https://leetcode.com/problems/product-of-array-except-self/)

### Problem
- Given an array of ints `nums` return an array `answer` where `answer[i]`!== product of all elements in nums except itself `nums[i]`
- product of `nums` prefix/sufix will fit in a 32-bit integer
- write an algo w/ TC: O(n), no use of division operation

### Analysis
- Input: `nums` [] with values of type `int`
- Output: `answer` [] with values of type `int`

- Algo Process
    - Restate Problem
        - Given an array of number return an array where, in the output, each element is the product of all the other numbers that doesn't include the number at the given index in the corresponding input array
        - If given [1,2,3], we return each product in the same spot excluding the number in the input
            - the output would be [6,3,2]
                * answer[1] = input[2] * input[3]
                * answer[2] = input[1] * input[3]
                * answer[3] = input[1] * input[2]

    - Goal of Function
        - Function must return an array `answer` that at each index must contain the product of all numbers except the corresponding number in the input array at the same index

    - Types
        - Input, Output: List[int]

    - Assertions and Assumptions
        - Output and Input array have to be of the same size (len(input)=len(answer))
        - Problem needs to be solved in O(1) space complexity

    - Edge Cases
        - input array contains elements that are not integers  ('abc', '/sd', '..'), not eligible for math ops

#### Example \#1
```
nums = [1,2,3,4] -> answer = [24,12,8,6]
```

#### Example \#2
```
nums = [-1,1,0,-3,3] -> answer = [0,0,9,0,0]
```
#### Example \#3
```
nums = [2,1,3,-2,-1,-3] -> answer = [-18,-36, -12, 18, 36, 12]
```

#### Constraints
> * 2 <= nums.length <=10^5
> * -3 <= nums[i] <= 30
> * Product of prefix, suffix of nums is guaranteed to fit in 32-bit integer

### Preliminary Solution
- __Time Complexity__: O(n^2)

- __Space Complexity__: 0(n)

#### _Backside_
 - My initial solution would involve:
    1. Iterate through the input array and for the first index to be returned in output, skip first and then perform operation (k_2 * k_3*...*k_n), append to answer
    2. For second index which would model the other, we need to skip the second but ensure that we multiply that resultant product by the first element
        - or if index!==1 then start at first, skip current, then multiply rest of elements (k_1 * k_3*...*k_n) -> append to answer
    3. For third index, start at 1st then skip third
        - if index!==1, start at first, skip current, multiply by rest
            - (k_1 * k_2 * k_4 * ... * k_n)
    4. and so on
- [First Attempt](PAES.py)
    * ! Below in comments in file !
    - Attempted to sort of leap frog the element of the index we don't want to include as product in the current index for the resultant array
    - Was able to pass the first test case but other test cases involving a larger set or event negative elements, result in failure
- [Second Attempt](PAES.js)
    -  creates a process where we determine that product at an index using a left and right product to store the multiplicative result exclusive

#### Additional Details
- First attempt had bad time complexity, would be O(n^2) with space complexity being O(n)
- Second attempt has time complexity of O(n) as there are two for loops iterating over an array of length n in order to return products of multiplication and space complexity is O(N) as there is one array created storing multiplicative sums proportional to input array

#### Solution Code
- [JS Solution](PAES.js)
- [Python Solution](PAES.py)


### [Neetcode](https://youtu.be/bNvIQI2wAjk)
- [Neetcode Solution](NC_PAES.py)
#### TC, SC
- O(n) space and time complexity

- #### Notes
    - If able to use division operator, could take the product of all numbers in entire array and divide by number at index, create an array containing all quotients
    - First attempt
        - (1) Get product of every value before element then (2)
        get product of every value after element
        - Put prefix and postfix as separate arrays
        - Ex:
            ```
                        [1,2,3,4]  = i (len of p)

            prefix:     [1,2,6,24]  = n (len of k)
                        [ i_1, i_1*i_2, n_1*i_3, n_k-1*i_p ]

            postfix:    [24, 24, 12, 4] = m (len of k)
                        [ m_k-3*i_p-3, m_k-2*i_p-2, i_p-1*i_p, i_p]

            output:     [24,12,8,6]
                        - > [1][1,2,6]
                            [24,12,4][1]

            First index: prefix [1] (before the first element in prefix) * post [24] -> 24
            Second index: prefix[1] * postfix[12] -> 12
            Third index: prefix[2] * postfix[4] -> 8
            Fourth index: prefix[6] * postfix[1] -> 6
            ```
        - Requires additional memory as a result of storing three arrays
    - Alternative attempt ( ! MUCH BETTER ! )
        1. Prefix
            - start from beginning of array index 0 in [1,2,3,4] then proceed towards end
                - start with default of [1]
                - end of prefix compute will be [1,1,2,6] with [24] being outside
                    - 1
                    - 1*1 -> 1
                    - 1*2 -> 2
                    - 2*3 -> 6
        2. Postfix
            - start from end and compute products
                - starting with [1,1,2,6]
                - postfix default start is 1
                    - 1*6 -> 6
                    -> 4*1 -> 4 -> 4*2 -> 8
                    -> 4*3 -> 12 -> 12*1 -> 12
                    -> 2*12 -> 24 -> 24*1 -> 24
                - ends in [24,12,8,6]

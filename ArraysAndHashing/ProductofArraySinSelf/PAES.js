/**
 * @param {number[]} nums
 * @return {number[]}
 */
var productExceptSelf = function (nums) {
    const n = nums.length
    const result = []
    let left_product = 1
    for (let i = 0; i < n; i++) {
        result.push(left_product)
        left_product *= nums[i]
    }
    let right_product = 1
    for (let j = n - 1; j > -1; j--) {
        result[j] *= right_product
        right_product *= nums[i]
    }
    return result
};

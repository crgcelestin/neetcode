# second attempt
class Solution(object):
    def productExceptSelf(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        n = len(nums)
        result=[]
        left_product=1
        # calc product of elements to left of current index
        for i in range(n):
            result.append(left_product)
            left_product*=nums[i]
        # calc product of elements to right of current index
        # have to reset product for this loop or else we have incorrect numbers due to the resultants from the first for loop
        right_product=1
        for i in range(n-1,-1,-1):
            result[i]*=right_product
            right_product*=nums[i]
        return result

Test = Solution()
# passed
print(Test.productExceptSelf([1,2,3,4]))

#passed
print(Test.productExceptSelf([-1,1,0,-3,3]))

#passed
print(Test.productExceptSelf([2,1,3,-2,-1,-3]))

# 1st attempt
'''
attempted to use if, else loop in a way to skip index to be passed and not returned in `answer` array

worked for smaller sets, but not larger arrays, time complexity is most likely O(n^2) even though it does not fully pass
'''

'''
class Solution(object):
    def productExceptSelf(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        result=[]
        for i in range(len(nums)):
            if i==0:
                starting_index=1
                product=1
                cut=nums[starting_index:]
                for num in cut:
                    product*=num
                result.append(product)
            else:
                starting_index=0
                product=nums[starting_index]
                for num in nums:
                    if nums[i]==num:
                        pass
                    else:
                        product*=num
                result.append(product)
        return result

Test = Solution()
# passed
print(Test.productExceptSelf([1,2,3,4]))
# failed
print(Test.productExceptSelf([-1,1,0,-3,3]))
# failed
print(Test.productExceptSelf([2,1,3,-2,-1,-3]))
'''

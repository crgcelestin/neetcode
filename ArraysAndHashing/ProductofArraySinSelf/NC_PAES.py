class Solution(object):
    def PAES(self, nums: list[int]) -> list[int]:
        res = [1] * (len(nums))
        prefix = 1
        for i in range(len(nums)):
            res[i] = prefix
            prefix *= nums[i]
        print(res)
        # start, stop, step (start at end of array, stop at beginning index of -1 not inclusive, go in reverse i.e -1)
        postfix = 1
        for i in range(len(nums), -1, -1):
            res[i] *= postfix
            postfix *= nums[i]
        return res


Test = Solution()

# [24,12,8,6]
print(Test.PAES([1, 2, 3, 4]))
# [0, 0, 9, 0, 0]
print(Test.PAES([-1, 1, 0, -3, 3]))
# [-18, -36, -12, 18, 36, 12]
print(Test.PAES([2, 1, 3, -2, -1, -3]))

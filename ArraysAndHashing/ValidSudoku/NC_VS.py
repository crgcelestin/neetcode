import collections
class Solution:
    def isValidSudoku(self, board: list[list[str]]) -> bool:
        # detect dups with hashset, hashmap where key = column number, set represents values in column
        cols = collections.defaultdict(set)
        rows = collections.defaultdict(set)
        # key is, key = (r/3, c/3)
        squares = collections.defaultdict(set)
        # loop in 9*9 grid, empty position represented by '.'
        for r in range(9):
            for c in range(9):
                if board[r][c]==".":
                    continue
                # has element been detected at a given row in rows being the set (uniques) of the elements in a row
                # has element been detected in a given column ...^
                # has element occurred in current square defined by the boundaries of r//3, c//3
                if (board[r][c] in rows[r] or
                    board[r][c] in cols[c] or
                    board[r][c] in squares[(r//3, c//3)]
                    ):
                    return False
                # adding elements that we have first seen/detected to hashsets
                cols[c].add(board[r][c])
                rows[r].add(board[r][c])
                squares[(r//3, c//3)]
        # no duplicates detected
        return True

# 08/12/23

## [238.Valid Sudoku](https://leetcode.com/problems/valid-sudoku/)

### Problem
> - Determine the validity of a 9*9 Sudoku board with string intgers
> - Filled cells need to be validated according to the specified rules:
    >   * Each row contains 1-9 w/o repetition
    >   * Each column has 1-9 w/o repetition
    >   * Each of the nine 3*3 sub-boxes has 1-9 w/o repetition

> * Notes
   >    - Partially filled board could be valid, not necessarily solvable
   >    - Only filled cells req to be validated according to rules

### Analysis
- Input: Sudoku board inputted as an array into the function, type arr
- Output: Boolean affirming/denying that inputted array is a valid Sudoku board (True/False), type boolean

- Algo Process
    - Restate Problem
        * Given a sudoku board (input) represented by a 9*9 grid with only string-integers filling in boxes being single digit with range of 1-9
        * (summarize) we are given this rule boundary:
            * Each row, column, and 3*3 sub-grids have 1-9 w/o repeats
        * If the provided input board fulfills the rules outlined above it is a valid Sudoku board

    - Goal of Function
        * Function must return true/false if the provided input is valid based on the provided constraints 'rules'

    - Types
        * Input: type list[list[str]]
            - input is a list of lists containing numbers
            - 9 sub arrays in 1 big array with strings-integer representations
        * Output: boolean
            - return either true or false

    - Assertions and Assumptions
        *   Input array contains only valid strings to be compared along rows and columns

    - Edge Cases
        * Empty matrix, matrix with same values (only duplicates), potential matrix with non strings, matrix containing string-integer representations outside range of 1-9, matrices that have strange dimensions i.e are not 9 by 9

#### Example \#1
```py
board1 =
[["5","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]
output = True
```

#### Example \#2
```py
board2 =
[["8","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]
output = False
# 2 8s in the top left corner which violates the rule that there can't be duplicates in a 3*3 area
```
#### Example \#3 & \#4
- Make up 3rd and 4th example, more #s that shows true and false
```py
board3 =
[["5","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,["7",".",".",".","9",".",".","7","9"]]
output = False

board4 =
[["3","2",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","1","9"]]
output = True
```

#### Constraints
> - board.length == 9 ( board has to have 9 rows )
> - board[i].length == 9 ( board has to have 9 columns )
> - board[i][j] is a 1-9 digit or '.'

### Preliminary Solution
- __Time Complexity__: O(n^2)

- __Space Complexity__: O(n)

#### _Backside_
 - Example board
    ```py
        [
            [5,3,.],
            [6,.,.],
            [.,9,8]
        ]
    ```
 - My initial solution would involve:
    1. we want to iterate through the given sudoku board
        ```py
            for c in range(len(board))
                for j in range(len(board[c]))
        ```
        * each integer is some x, y i.e board[c][j]
            - example being 6 which is board[1][0]
    2. Perform check for each rule, while ignoring empty spaces
        - `if sudoku[c][j]!=='.'`
        1. Does each row contain 1-9 w/o duplicates
            * we can initialize a dict, map `{}` that allows for tracking of numbers in order to view if there are duplicates in a row
            * Check if tracker has any duplicates i.e does a key value pair have a 2 wit ith
                - example being `[5] in the same row as [3]`
        2. Does each column contain 1-9 w/o dups
            * new dict, check if numbers in column have duplicates
                - an example being `[5] -> [6] (directly down 1 vertically)`
        3. Are there dups in 3*3 boxes in the input board
            - a 3 by 3 box example:
                ```py
                    [
                        [5,3,.],
                        [6,.,.],
                        [.,9,8]
                    ]
                    [0,0] -> [0,3]
                    [3,0] -> [3,3]
                ```

- Second attempt
    1. initialize a flag that will return boolean True at end of function if it is determined input is valid `is_valid=True`
    2. define a helper function for detecting duplicates titled `is_duplicates`
        - this function takes an input array, iterate through array and aggregate via list comp to get array of numbers that doesn't include empty spaces
        - determine if that array has duplicates with a conditional that asks is the length of nums the same as the set version of nums
    3. check if there are duplicates in a given row
        - iterate in sudoku, since we know that the helper checks in a row with list comprehension then if the returned boolean is False, is_valid -> F
    4. check if there are duplicates in a given column
        - for columns we are going to iterate through the transposed  (swap rows w/ columns)`sudoku` matrix by using `zip(*sudoku)`
            -  can actually see it get transposed
                - `print(f'original board {sudoku}')`
                - `print(f'transposed {list(zip(*sudoku))}')`
            - `zip(*sudoku)` takes row sub arrays and converts them to tuples where first element in column is first element in row, etc.
                - `*` allows for the unpacking of arguments which effectively passes each inner list as a separate argument
                - zip then takes elements from each row to create tuples
                    ```py
                    # if sudoku
                    s = [
                        [1,2,3],
                        [4,5,6],
                        [7,8,9]
                    ]
                    # zip(*sudoku) yields
                    s_transposed = [
                        (1,4,7),
                        (2,5,8),
                        (3,6,9)
                    ]
                    ```
                    - instead of zip, can potentially do
                        ```py
                        for col in range(9):
                            column=[sudoku[row][col] for row in range(9)]
                            if not is_duplicates(column):
                                is_valid=False
                        ```
        - now with columns being row equivalents, input into is_duplicates
    5. check if there are duplicates in any of the 3*3 boxes by iterating over string-integers and ensure they are unique
        - first, iterate over rows in increments of 3 using range(start, stop, step)
            - start is from 0
            - stop is at end of len(row) = 9
            - step is 3, iterate by elements of len = 3
        - then iterate over columns by 3s using the same method
        - next we utilize list comprehension, which allows us to create a new list 'box' in the iteration of rows and columns, generating arrays of elements within current region
            - `for i in range(r, r+3)` i.e. [3,4,5]
                - iterate over rows in range of r, so 3 consecutive rows in the box
            - `for j in range(c, c+3)` i.e [3,4,5]
                - iterate over columns in range c, so 3 consecutive columns in box
        - then use the helper function to check if the 3*3-row equivalent has any duplicates, set flag to False if there are


#### Additional Details
- Reflecting on First Attempt
    * I know that you can use set in order to decipher if an array has duplicates and then by checking the original array length as compared to the set we can determine if there are any existing duplicates
        - idea would be to perform this for rows and columns
    * For the 3*3 box would require analysis in provide regions and iterating through all potential boxes in said matrix
        - in a 9*9 there are 9 3*3s

- Reflecting on Second Attempt
    - list comprehension and set can be extremely handy and allows for detection of duplicates, if given an array (elegant) with spaces
        - `actuals = [num for num in nums if num!='.' ]`
        - `len(actuals)==len(set(actuals))` (really handles duplicate detection)
    - rows in a matrix are just a for row in x, columns can just be for col in zip(*matrix)
        - 3*3 boxes are more complex with there needing to be two for loops for iterating followed by a list comprehension for getting elements in specified spaces
            - an example for 4*4 potentially
                ```py
                    for r in range(0,16,4):
                        for c in range(0,16,4):
                            box = [board[i][j] for i in range(r, r+4) for j in range(c, c+4)]
                            # and now elements of 4*4 boxes are now condensed into sub arrays for duplicate checking
                ```

#### Solution Code
- [JS Solution](VS.py)
- [Python Solution](VS.js)


### [Neetcode](https://www.youtube.com/watch?v=TjFXEUCMqI8)
- [Neetcode Solution](NC_VS.py)
#### TC, SC
- Tc: O(n)
    - within each cell, ops involving hashsets (additions, lookups) are performed in constant time
- sc: O(n)
    - worst case each hashset has all 9 possible elements

- #### NC Notes
    - a board with row, col
        ```
        [1,2,3,4,5,6,7,8]
                         [2,
                         3,
                         4,
                         5,
                         6,
                         7,
                         8,
                         9
                         ]
        ```
        is still considered valid
    - will do standard approach using hashsets, to determine if row has any filled in duplicates and same for columns
        - adding element to hashset is O(1) and checking duplicates is also O(1)
    - need to check for 9 boxes in 3 by 3 grids, to check for duplicates
        - Solution being TC and SC: 0(9^2) -> O(1)
        - Need to treat all boxes in 9 by 9 grid in a 0,1,2 row/col index
            - ```py
                board = [
                            0               1            2
                    0 [ 3 by 3 grid] [3 by 3 grid] [3 by 3 grid]
                    1 [ 3 by 3 gird] [3 by 3 grid] [3 by 3 grid]
                    2 [ 3 by 3 grid] [3 by 3 grid] [3 by 3 grid]
                ]
              ```
            - divide the position coordinates by 3
                - so a number at 2,2 -> [2/3, 2/3] -> [0,0] is in the first 3 by 3 grid at [0,0]

# First attempt at valid sudoku solution in 30 minutes
class FirstAttempt:
    def isValidSudoku(self, sudoku: list[list[str]]) -> bool:
        tracker={}
        for c in range(len(sudoku)):
            for j in range(len(sudoku[c])):
                tracker[sudoku[j][c]]=1+tracker.get(sudoku[j][c],0)
        print(tracker)
                # tracker[sudoku[j]]=tracker.get(0,sudoku[j])
        '''
        def is_duplicates()
            for c in range(len(row)):
                if row[c]==row[c+1]:
                    return True
            return False
        flag=False
        iterate through board rows
            if is_duplicates(rows)
                flag=True
        iterate through columns
            if is_duplicates(columns)
                flag=True
        iterate through boxes (or maybe that happens above)
            if is_duplicates(sub 3*3 boxes):
                flag=True
        if flag
            return false
        return true
        '''

# Second attempt at valid sudoku
class SecondAttempt:
    def isValidSudoku(self, board: list[list[str]]) -> bool:
        is_valid=True
        def is_duplicates(nums: list[str]) -> bool:
                nums = [num for num in nums if num!='.']
                return len(nums) == len(set(nums))
        for row in board:
            if not is_duplicates(row):
                is_valid=False
        for col in zip(*board):
             if not is_duplicates(col):
                  is_valid=False
        for r in range(0,9,3):
             for c in range(0,9,3):
                  box = [board[i][j] for i in range(r, r+3) for j in range(c, c+3)]
                  print(box)
                  if not is_duplicates(box):
                       is_valid=False
        return is_valid


Test2=SecondAttempt()
First2 = Test2.isValidSudoku(
[["5","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]
)

# Test algo
print(First2)

# Validate w/ print assertion for true board
print(First2==True)

Second2 = Test2.isValidSudoku(
[["8","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]
)

# Test algo
print(Second2)

# Validate w/ print assertion for false board
print(Second2==False)

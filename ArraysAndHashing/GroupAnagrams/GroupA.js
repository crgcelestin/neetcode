/**
 * @param {string[]} strs
 * @return {string[][]}
 */
var groupAnagrams = function (strs) {
    const tracker = new Map()
    for (let s of strs) {
        let count = Array(26).fill(0)
        for (let c of s) {
            count[c.charCodeAt(0) - 'a'.charCodeAt(0)] += 1
        }
        const key = count.toString()
        if (!tracker.has(key)) {
            tracker.set(key, [])
        }
        tracker.get(key).push(s)
    }
    return Array.from(tracker.values())
}

console.log(groupAnagrams(['ate', 'tae']))

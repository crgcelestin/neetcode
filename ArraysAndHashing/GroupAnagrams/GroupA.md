# 06/14/23 - 1

## [Group Anagrams](https://leetcode.com/problems/group-anagrams/)

### Problem
> Given an array of strings strs, group the anagrams together. You can return the answer in any order.

> An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.

### Analysis
- Input: array of strings (strs) ['first','second']
- Output: array of strings with anagrams grouped, array of subarrays [[1st array], [2nd array]]

- Algo Process
    - Restate Problem
        - Given an array of strings, return an array that has subarrays that are anagram groups, ie strings that are spelled with the same characters and have the same length
        - Original letters are used exactly once amongst all strings
            - ['ate','tea','eat']
    - Goal of Function
        - Function must return an array that only has the anagram groups once
        - Give me all the words that are anagrams of each other
    - Types
        - List/arr, strings contained in arr
    - Assertions
        - Anagram groups only repeat once
        - Only lower case letters
        - input contains array that only has strings, no integers in the string
    - Edge Cases
        - empty input array, array that contains strings that all don't have other anagram possibilities
        - [], ['bat','ate','done'], ['1sew','2fdgfh'], [1,23,4]

#### Example \#1
```
Input: strs = ["eat","tea","tan","ate","nat","bat"]
Output: [["bat"],["nat","tan"],["ate","eat","tea"]]
```

#### Example \#2
```
Input: strs = [""]
Output: [[""]]
```
#### Example \#3
```
Input: strs = ["a"]
Output: [["a"]]
```

#### Constraints
- `1 <= strs.length <= 104`
- `0 <= strs[i].length <= 100`
- `strs[i] consists of lowercase English letters`

### Best Available Solution
- __Time Complexity__: O(s*n)
- __Space Complexity__: O(n)
- n being avg string length, s being number of strings


#### Additional Details
[Neetcode - Group Aanagrams](https://www.youtube.com/watch?v=vzdNOK2oB2E&t=2s)
- Initial Setup
    - Given the constraint of lowercase a-z
    - Have an array called count, and for each string count number of each character from a-z
    - Use Hashmap DS w/ key being the count of each character in order to id anagrams, Values are list of anagrams that contain count
    ```py
        # Hypothetical DS
        count = {
            '1e, 1a, 1t' : 'tea','ate', 'eat'
        }
    ```
    - Overall tc: O(s*n)
        - s = len(strs), n = avg length of string (need to count of each char for each string in strs)
    - Overall sc: O(n)
        - dict storing the keys of the characters of each possibilities of anagrams grows linearly, worst case they are not anagrams of each other so it has to store the number of chars of each string in the array of length n

#### Solution Code
[JS Solution](GroupA.js)
[Python Solution](GroupA.py)

#### Notes
- LC/Neetcode solution is convoluted, but provides for a decently optimized time and space complexity

class Solution:
    def groupAnagrams(self, strs: list[str]) -> list[list[str]]:
        # mapping charCount to anagrams list
        tracker = {}
        for s in strs:
            # lowercase a to z
            count = [0] * 26

            # store char count using ord, maps letters to a numerical value
            for c in s:
                count[ord(c) - ord('a')] +=1

                #tuple() as list can't be a key as tuples are not mutable
            if tuple(count) not in tracker:
                # if key doesn't exist yet, avoid key error in appending step
                tracker[tuple(count)]=[]
            tracker[tuple(count)].append(s)
            # return as type list using list()
        return list(tracker.values())


Test=Solution()
print(Test.groupAnagrams(["eat","tea","tan","ate","nat","bat"]))

# Test Cases
print(Test.groupAnagrams(["eat","tea","tan","ate","nat","bat"])==[["eat","tea","ate"],["tan","nat"],["bat"]])
print(Test.groupAnagrams([""])==[[""]])
print(Test.groupAnagrams(["a"])==[["a"]])


# First brute force approach
# Broken - repeats anagram duos, strings, no groups of 3 for all equivalent anagram possibilities
'''
class Solution:
    def groupAnagrams(self, strs: list[str]) -> list[list[str]]:
        anagrams=[]

        def isAnagram(t,s):
            if len(t)==len(s):
              chars=set(t)
              for char in chars:
                  if char not in s or s.count(char)!=t.count(char):
                      return False
            return True

        for prev_str in range(len(strs)):
            for  next_str in range(prev_str+1, len(strs)):
                    if isAnagram(strs[prev_str], strs[next_str]):
                        anagrams.append([strs[prev_str], strs[next_str]])
                    else:
                        anagrams.append(strs[next_str])
        return anagrams

Test=Solution()
print(Test.groupAnagrams(["eat","tea","tan","ate","nat","bat"]))
'''

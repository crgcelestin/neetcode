# Solutions
[Master Template](MASTER_TEMPLATE.md)

## Spaced Repetition, Active Recall Spreadsheet
[Tracking Spreadsheet](https://docs.google.com/spreadsheets/d/1JWfczTcqX3vH-sk5egZd3fCIXwA_hE-6eYkmptX2aZE/edit#gid=0)

| Group    | Subsection                                                     | Problem                      | Notes and Solutions                                                       | Difficulty | Date       | Status      |
| -------- | --------------------------------------------------------------------------------------------------------- |---------------------------------------------------------------- | ---------- | ---------- | ----------- | ---- |
| NeetCode 150 | Arrays and Hashing | [Contains Duplicate](https://leetcode.com/problems/contains-duplicate/) | [Done](/ArraysAndHashing/ContainsDuplicate/ContainsDup.md) | Easy | 06/14 | ⭕ Complete
| - |   Arrays and Hashing| [Valid Anagram](https://leetcode.com/problems/valid-anagram/) | [Done](/ArraysAndHashing/ValidAnagram/ValidAng.md) | Easy | 06/15 | ⭕ Complete
| - |  Arrays and Hashing | [Two Sum](https://leetcode.com/problems/two-sum/) | [Done](/ArraysAndHashing/TwoSum/TwoSum.md) | Easy | 06/17 |  ⭕ Complete
| - |  Arrays and Hashing | [Group Anagrams](https://leetcode.com/problems/group-anagrams/) | [Done](/ArraysAndHashing/GroupAnagrams/GroupA.md) | Medium | 06/17 |  ⭕ Complete
| - |  Arrays and Hashing | [Top K Frequent Elements](https://leetcode.com/problems/top-k-frequent-elements/) | [Done](/ArraysAndHashing/topKelements/TopK.md) | Easy | 07/20 |  ⭕ Complete
| - |  Arrays and Hashing | [Product of Array Except Self](https://leetcode.com/problems/product-of-array-except-self/) | [Done](./ArraysAndHashing/ProductofArraySinSelf/PAES.md)| Medium | 07/31 | ⭕ Complete
| - |  Arrays and Hashing | [Valid Sudoku](https://leetcode.com/problems/valid-sudoku/) |[Done](./ArraysAndHashing/ValidSudoku/ValidSudoku.md) | Medium | 08/17 | ⭕ Complete
| - |  Arrays and Hashing | [Encode and Decode Strings](https://www.lintcode.com/problem/659/) | [Done](./ArraysAndHashing/EncodeDecode_Strings/encode_decode.md) | Medium | 08/18 | ⭕ Complete |
| - |  Arrays and Hashing | [128. Longest Consecutive Sequences](https://leetcode.com/problems/longest-consecutive-sequence/) | [Done](./ArraysAndHashing/LongestConseuctiveSequence/longest_sequence.md) | Medium | 08/19 | ⭕ Complete |
| - | Stack | [20.Valid Parentheses](https://leetcode.com/problems/valid-parentheses/) | [Done](./Stack/Valid_Pararentheses/ValidParentheses.md) | Easy | 08/23 |⭕ Complete
| - | Two Pointers | [125.Valid Palindrome](https://leetcode.com/problems/valid-palindrome/) | [Done](./TwoPointers/Valid_Palindrome/ValidPalindrome.md) | Easy | 08/24 |⭕ Complete
| - | Stack | [155. Min Stack](https://leetcode.com/problems/min-stack/) | [Done](./Stack/min_stack/min_Stack.md) | Medium | 09/9/23 | ⭕ Complete
| - | Two Pointers | [167. Two Sum II - Input Array Is Sorted](https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/description/) | [Done](./TwoPointers/Two_SumII/2SumII.md) | Medium | 9/9/23 | ⭕ Complete
| - | Stack | [150. Evaluate Reverse Polish Notation](https://leetcode.com/problems/evaluate-reverse-polish-notation/description/) | [Done](./Stack/eval_rev/eval_rev_polish_notation.md) | Medium | 9/29/23 | ⭕ Complete |
| - | Two Pointers | [15. 3Sum](https://leetcode.com/problems/3sum/description/) | [Done](./TwoPointers/ThreeSum/threeSum.md)| Medium | 9/29/23 | ⭕ Complete |
| - | Stack | [22. Generate Parentheses](https://leetcode.com/problems/generate-parentheses/description/) | [Done](./Stack/gen_P/gen_P.md) | Medium | 9/29/23 | - |
| - | Two Pointers | [11. Container with Most Water](https://leetcode.com/problems/container-with-most-water/description/) | [Done](./TwoPointers/ContainerWithMostWater/CWMW.md) | Medium | 10/01/23 | ⭕ Complete |
| - | Two Pointers | [12. Trapping Rain Water](https://leetcode.com/problems/trapping-rain-water/) | [Done](./TwoPointers/TrappingRainH2O/trw.md) | Hard | 10/02/23 | ⭕ Complete |
| - | Stack | [739. Daily Temperatures](https://leetcode.com/problems/daily-temperatures/description/) | [Done](./Stack/daily_T/) | Medium | 12.26.23 | ⭕ Complete |
| - | Stack | [853. Car Fleet](https://leetcode.com/problems/car-fleet/description/) | [Done](./Stack/car_fleet/carFleet.md) | Medium | 12.26.23 | ⭕ Complete
| - | Stack | [84. Largest Rectangle in Histogram](./Stack/largest_rect_in_histogram/) | [Done](./Stack/largest_rect_in_histogram/largestRect.md) | Hard | 12.26.23 | ⭕ Complete
| - | Binary Search | [704. BINARY SEARCH](./BinarySearch/BS/) | [Done](./BinarySearch/BS/BS.md) | Easy | 1.6.24 | ⭕ Complete
| - | Sliding Window | [121. Best Time to Buy and Sell Stock](./SlidingWindow/SW/) | [Done](./SlidingWindow/SW/sw.md) | Easy | 1.7.24  | ⭕ Complete  |
| - | Binary Search | [74. Search a 2D Matrix](./BinarySearch/S2DM/) | [Done](./BinarySearch/S2DM/S2DM.md) | Medium | 1.29.24 | ⭕ Complete |
| - | Sliding Window | [3. Longest Substring Without Repeating Characters](./SlidingWindow/LSWRC/) | [-](./SlidingWindow/LSWRC/) | Medium | |
| - | Linked Lists | [21. Merge Two Sorted Lists](./LinkedList/M2SL/) | [-](./LinkedList/M2SL/) | Easy |

⭕ Complete

## Move Repo from Gitlab -> Github
[source](https://documentation.its.umich.edu/node/4001)

class Solution:
    def isMontonic(self, nums: list[int]) -> bool:
        increasing = decreasing = True
        for i in range(1, len(nums)):
            if nums[i-1]>nums[i]:
                increasing=False
            if nums[i-1]<nums[i]:
                decreasing=False
        return increasing or decreasing

Test = Solution()
Test1 = Test.isMontonic(
    [1,2,3]
)
Test2 = Test.isMontonic(
    [6,5,4]
)
Test3 = Test.isMontonic(
    [3,0,2]
)
Test4 = Test.isMontonic(
    [0,3,2]
)
print(Test1)
print(Test1==True)
print(Test2)
print(Test2==True)
print(Test3)
print(Test3==False)
print(Test4)
print(Test4==False)

// my attempt at a solution

/**
 * @param {string} s
 * @param {number} k
 * @returns {number}
 */
var characterReplacement = function (s, k) {
    let [l, r] = [0, 1]
    let max_substr = 0
    let substr;
    while (r < s.length) {
        console.log([l, r])
        if (s[r] === s[l]) {
            substr = r - l + 1
            r += 1
        } else {
            substr = max_substr
            l += 1
            r += 1
        }
        max_substr = Math.max(max_substr, substr)
    }
    return max_substr
}



// corrected

/**
 * @param {string} s
 * @param {number} k
 * @return {number}
 */
var characterReplacement2 = function (s, k) {
    let count = new Map()
    let [maxF, res, l] = [0, 0, 0]
    for (r = 0; r < s.length; r++) {
        count[s[r]] = (count[s[r]] || 0) + 1
        console.log(count)
        maxF = Math.max(maxF, count[s[r]])
        while ((r - l + 1) - maxF > k) {
            count[s[l]] -= 1
            l += 1
        }
        res = Math.max(res, r - l + 1)
    }
    return res
}


const input1 = {
    s: "BABABA",
    k: 2
}
console.log(
    characterReplacement2(
        input1.s, input1.k
    )
)

# 03.05.2024

## [424. Longest Repeating Character Replacement](https://leetcode.com/problems/longest-repeating-character-replacement/description/)

### Problem
- Given a `str: s` and `int:k`
- Can choose any char of string, change to any other uppercase English char
- Perform this operation at most `k` times

- Return __length of longest substr containing same latter able to be obtained after performing the above operations__

### Analysis
- Input: function takes in two inputs, s of type string, k of type int `string and integer`
- Output: returns length of longest substring `integer`

- Algo Process
    - Restate Problem: given two inputs where you can perform a replace operation at most k times and are asked to optimize substring length

    - Goal of Function: return most optimal i.e longest length substring with consecutive alike characters

    - Types: string, integer

    - Assertions and Assumptions: only considering length of substrings with consecutive same characters, all uppercase (constrained by alphabet of 26 possible chars meaning O(26n)-> O(n))

    - Edge Cases: empty string, string with only consecutive chars

#### Example \#1
```
input1: {
    s: 'ABAB",
    k: 2
}
output1 = 4
explanation: replace two a's with 2 b's
```

#### Example \#2
```
input2: {
    s: 'AABABBA',
    k: 1
}
output2 = 4
explanation: replace one 'A' in middle with 'B' to form 'AABBBBA', substring 'BBBB' has longest repeating letters being 4 -> there are other alt methods to achieve this result
```

#### Constraints
* `1<= s.length <= 10^5`
* `s` only has uppercase English letters
* `0 <= k <= s.length`

### Preliminary Solution
- __Time Complexity__: worst case scenario the characters we need to replace are at the end of a really long string, so we have iterate through string and based on value of k
    * 'AAAAA...B' with k=1 and so on,
    * O(s*k), s being length of string to search for replacing chars and k being number of replacements, may need to iterate through string, k times

- __Space Complexity__:

#### _Backside_
- My initial solution would involve:
    * as is sliding window problem, want to initialize l,r with l encountering first char then r continuing along encountering chars that = l until it reaches a different character, this will be the frame for the max substring (r-l+1)
    * need to store the max substring length given only k times to convert 1 char to another char i.e a->b or vice versa


#### Additional Details
- Reflecting on Attempt:

#### Solution Code
- [JS Solution](lrcrl.js)
- [Python Solution]()

### [Neetcode](https://www.youtube.com/watch?v=gqXU1UyA8pk)
- [Neetcode Solution](nc.py)
#### TC, SC
tc: O(n), iterating through each character in input string, worst case longest substr occurs at end of input string
sc:

- #### NC Notes
* can't compare each substring in string, if we want to be optimal as that would be O(n^2)
* only know if a substring is valid, longest substring containing a single character and we are only allowed at max 'k' replacements -> look at single substring, do we want to replace dominant or non-dominant term, we want to replace characters that are more common in current window

* take length of window and count of most frequent character: `windowLen - count[B]`, allows us to understand number of characters we need to replace and then we need to check if the needed replacements are <= k
* perform sliding window process with l,r

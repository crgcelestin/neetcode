/**
 * @param {string} s
 * @param {string} t
 * @returns {string}
 */
var minWindow = function (s, t) {

    // if either not present, t is longer than s
    if (
        (!s.length || !t.length) ||
        (t.length > s.length)
    ) {
        return ""
    }
    // if they are equal to each other
    if (s === t) { return t }
    let [t_map, s_window] = [{}, {}];
    for (let char of t) {
        t_map[char] = (t_map[char] || 0) + 1
    }

    let [have, sum] = [0, 0];
    const need = Object.values(t_map).reduce(
        (acc, val) => acc + val,
        sum,
    )
    let res = [-1, -1]
    let reslen = Infinity
    let l = 0

    for (let r = 0; r < s.length; r++) {
        let char = s[r]
        // update count to understand chars contained in substr window
        s_window[char] = (s_window[char] || 0) + 1
        // is char in countT and does count of window satisfy that of t for particular char
        if (Object.keys(t_map).includes(char) && s_window[char] === t_map[char]) {
            have += 1
        }
        while (have === need) {
            if ((r - l + 1) < reslen) {
                // if above is true can update i.e new ideal window
                res = [l, r]
                reslen = (r - l + 1)
            }
            // try to minimize current window, pop from left of window
            s_window[s[l]] -= 1
            if (Object.keys(t_map).includes(s[l]) && s_window[s[l]] < t_map[s[l]]) {
                have -= 1
            }
            l += 1
        }

    }
    [l, r] = res
    if (reslen !== Infinity) {
        return s.slice(l, r + 1)
    } else {
        return ""
    }
}

const input1 = {
    s: "ADOBECODEBANCC",
    t: "ABC"
}

const input2 = {
    s: 'bbaa',
    t: 'aba'
}
console.log(
    minWindow(
        input1.s,
        input1.t
    )
)
console.log(
    minWindow(
        input2.s,
        input2.t
    )
)

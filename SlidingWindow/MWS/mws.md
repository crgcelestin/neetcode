# 03.11.24

## [76. Minimum Window Substring](https://leetcode.com/problems/minimum-window-substring/description/)

### Problem
- Given two strings `s`, `t` of lengths `m`, `n` respectively, return min window __substring__ of `s` so every char in `t` (including duplicates) is in window
    * if no such substring, return empty string `""`

### Analysis
- Input: s, t (string)
- Output: min_window_substr (string)

- Algo Process
    - Restate Problem:
        - we need to return min window substr such that all elements of t are found in said portion of s
    - Goal of Function
        - return min substring given t that has found all chars in s, if such does not exist return empty string
    - Types
        - str
    - Assertions and Assumptions
        * if s is less than t, then a return is not possible
        * if not s or not t, then return
        * string s at min has to be greater or equal to t

    - Edge Cases
        * empty strings, incorrect relative lengths

#### Example \#1
```
input1 = {
    s = "ADOBECODEBANC",
    t = "ABC"
}
output1 = "BANC"
# BANC contains all chars and is min length possible
```

#### Example \#2
```
input2 = {
    s = 'a',
    t = 'a
}
output2 = 'a'
# entire string s is min window
```
#### Example \#3 & \#4
```
input3 = {
    s = "a",
    t = "aa"
}
output3 = ""
// impossible as t is greater than s
```

#### Constraints
- `m == s.length`
- `n == t.length`
-  `1 <= m, n <= 10**5`

#### _Backside_
- My solution would involve:
    * we need to find min frame that has count of chars equal to whats found in t
    * so while ADOBECODEBANC has several possible short substrings we need to find the exact combination of substr, that contains all chars if possible

#### Additional Details
- Reflecting on Attempt:

### Preliminary Solution
- __Time Complexity__:

- __Space Complexity__:

#### Solution Code
- [JS Solution]()
- [Python Solution]()

### [Neetcode]()
- [Neetcode Solution]()
#### TC, SC


- #### NC Notes
* 2 hashmaps of current window and input string to search for desired substring
    * (need) for t, (have) for s
    - Brute Force
        * compare the have and need, if missing char count in have as compared to need then we expand window, to find substring that min count
    - Linear Solution
        Keep track of have and need lengths, instead of comparing counts for all chars in need and have, just have the previous comparisons cached
        * we first find initial string that includes chars required from t, find len using the window size and continue to perform comparisons with other min substrings throughout and update if shorter string

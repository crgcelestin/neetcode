class Solution:
    def minWindow(s: str, t: str):
        if not len(s) or not len(t) or len(t) > len(s):
            return ""
        if s == t:
            return t

        t_map, s_window = {}, {}
        # get count of chars in t string
        for char in t:
            t_map[char] = 1 + t_map.get(char, 0)
        have = need = 0
        for value in t_map.values():
            need += value
        l, res = 0, [-1, -1]
        reslen = float("infinity")

        for r in range(len(s)):
            # create count for current window with chars, count
            char = s[r]
            s_window[char] = 1 + s_window.get(char, 0)

            # update have if we encounter char that is in t, char occurs same # of times
            if char in t_map and s_window[char] == t_map[char]:
                have += 1

            while have == need:
                # if we reached a case where we have an ideal window in terms of char count, update above values (pointers, window length)
                res = [l, r]
                reslen = r - l + 1

                # min current window, pop from left
                s_window[s[l]] -= 1

                # update have, if popping from left
                # asking if current s[l] char in t_map and if that s_window map now has less compared to count in t_map, decrement have
                if s[l] in t_map and s_window[s[l]] < t_map[s[l]]:
                    have -= 1
                l += 1

        l, r = res
        return s[l : r + 1] if reslen is not float("infinity") else ""


print(Solution.minWindow("ADOBECODEBANC", "ABC"))

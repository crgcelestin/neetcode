# profit = local_max of latter days - local_min of former days
class Solution:
    def maxProfit(self, prices: list[int]) -> int:
        max_profit = 0
        if len(prices) < 2 or not prices:
            return max_profit
        # for i in range(0, len(prices) - 1):
        #     for j in range(1, len(prices)):
        #         if prices[j] - prices[i] > max_profit and prices[j] > prices[i]:
        #             profit = prices[j] - prices[i]
        #             max_profit = max(profit, max_profit)
        """
        initialize minimum price at first price -> for iteration start at second element, if we encounter a price larger than minimum then we will store a current max_profit determined by max()
        the 2 params of max being current profit [price - min_price] and max_profit
        max_profit stores a running total of max profit found in array so far
        else
            we have found a new lower price
        basically return stored running total
        """
        min_price = prices[0]
        for price in prices[1:]:
            if price > min_price:
                max_profit = max(price - min_price, max_profit)
            else:
                min_price = price
        return max_profit


Tests = Solution()
Test1 = Tests.maxProfit([7, 1, 5, 3, 6, 4])
Assert1 = 5
if Test1 == Assert1:
    print("Test Passed")
else:
    print(f"expected: {Assert1}, got: {Test1}")

Test2 = Tests.maxProfit([7, 6, 4, 3, 1])
Assert2 = 0
if Test2 == Assert2:
    print("Test Passed")
else:
    print(f"expected: {Assert2}, got: {Test2}")

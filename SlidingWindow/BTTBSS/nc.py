class Solution:
    def maxProfit(self, prices: list[int]) -> int:
        left, right = 0, 1  # left is buying, right is selling
        max_profit = 0
        while right < len(prices):
            if prices[left] < prices[right]:
                profit = prices[right] - prices[left]
                max_profit = max(max_profit, profit)
            else:
                left = right
            right += 1
        return max_profit


Tests = Solution()
Test1 = Tests.maxProfit([7, 1, 5, 3, 6, 4])
Assert1 = 5
if Test1 == Assert1:
    print("Test Passed")
else:
    print(f"expected: {Assert1}, got: {Test1}")

Test2 = Tests.maxProfit([7, 6, 4, 3, 1])
Assert2 = 0
if Test2 == Assert2:
    print("Test Passed")
else:
    print(f"expected: {Assert2}, got: {Test2}")

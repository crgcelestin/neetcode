/**
 * @param {number[]}
 * @return {number}
 */
var maxProfit = function (prices) {
    let [l, r] = [0, 1]
    let maxProfit = 0
    while (r < prices.length) {
        if (prices[r] > prices[l]) {
            const profit = prices[r] - prices[l]
            maxProfit = Math.max(profit, maxProfit)
        } else {
            l = r
        }
        r += 1
    }
    return maxProfit
}

const Test1 = maxProfit(
    [7, 1, 5, 3, 6, 4]
)
const assert1 = 5
if (JSON.stringify(Test1) === JSON.stringify(assert1)) {
    console.log('test passed')
} else {
    console.log('test failed')
}

# 01.07.24

## [121. Best Time to Buy and Sell Stock](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/description/)

### Problem
- Given an array `prices` list[int], where `prices[i]` int is the price of a given stock on the ith day
- Desire to maximize profit by choosing a single day to buy one stock, choose different day in future to sell same stock
- return max profit to be achieved form transaction
    * if one can't achieve profit then return 0

### Analysis
- Input: list[int] being prices that is unsorted
- Output: int being max profit or 0

- Algo Process
    - Restate Problem
        * determine when a max profit can be achieved by a single transaction when given select days and prices on said days
    - Goal of Function
        * return maximum profit if possible or 0 if there is no profit to be achieved
    - Types
        * int, list[int]
    - Assertions and Assumptions
        * we are constrained on our desired day to buy a stock and desired day to sell a stock, buy a stock occurs prior to selling
        * we are given a non-null array that may contain the possibility of a larger value after we have decided a day as the ideal buying date or the possibility of a smaller value prior to the ideal selling date - ideal selling date is predicated on the ideal buying date
    - Edge Cases
        * null array, non integer values, array with less than 2 values

#### Example \#1
```py
prices1 =  [7,1,5,3,6,4]
assert1 = 5
# Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
# Note that buying on day 2 and selling on day 1 is not allowed because you must buy before you sell.
```

#### Example \#2
```py
prices2 = [7,6,4,3,1]
assert2 = 0
# Explanation: In this case, no transactions are done and the max profit = 0.
```

#### Constraints
-   `1 <= prices.length <= 10^5`
-   `0 <= prices[i] <= 10^4`

#### _Backside_
- My solution would involve: the typical sliding window implementation involving two pointers that hold a max profit scenario and we compare it to any other profit instances
* basically to find desired max_profit, we need to obtain all potential +profit instances to get the largest


#### Additional Details
- Reflecting on Attempt: attempted a naive sliding window approach, able to pass first test but failed second example
- Revised code in first attempt, with overhaul of for loop, have min_price and max_price; both are to be updated accordingly as we iterate via `if price>min_price`, max_profit being updated via `max(price-min_price)` and `min_price = price`

### Preliminary Solution
- __Time Complexity__: O(N) iterate through array once with N being length of prices

- __Space Complexity__:O(1), two pointers

#### Solution Code
- [JS Solution](sw.js)
- [Python Solution](sw.py)

### [Neetcode](https://www.youtube.com/watch?v=1pkOgXD63yU)
- [Neetcode Solution](nc.py)
#### TC, SC

- #### NC Notes
* buy low, sell high
* solution is sensical with there being a two pointer approach and a storage of max-Profit
    - we want to have a while loop that only runs as right is in bound of prices array, if the element at left is smaller than right then we can calculate max_profit and determine if we have found local greatest
    - else we know we've found a prime purchasing point

# For Reviewing after sustained periods of time
## 08/19/23

1. contains duplicates, use set and compare to inputted array of nums

2. anagram = word/phrase created by rearranging letters of other word/phrase
    * valid anagrams, check if two strings are not equal in length, we want to initialize a char count array to represent char occurrences found in both strings `count = [0]*26` , (js) use Array(26).fill(0)
    - increment using ords of characters in 1st string and ord('a')
    - decrement using ords of 2nd string and ord('a')
        - use charCodeAt as opposed to 'ord' (js)
    - iterate through char count and ask if there incidents of non zero. they are not anagrams if there is a non zero value

3. group anagrams,
    * initialize empty map -> iterate over input array of strings (string)-> create char count for each string in array that has a length of 26 with 0s (py) `count = [0]*26` (js) `Array(26).fill(0)`-> create a binary of a string using ord by iterating over string chars`for char in string`, use (py)ord(char) - ord('a') +=1. (js) `c.charCodeAt(char) - 'a'.charCodeAt(0)` in order to create binary of 1s
    * convert binary of 1st to a (py)tuple, (js)string to be stored in a map as a key
        1. ask if count doesn't have this (py) tuple key `if tuple(count) not in tracker`, (js) string yet `(!tracker.has(key))`, initialize count with key and empty array as value `py(tracker[count[])`
        2. if it does have (py) binary tuple `tracker[tuple(count)].append(s)`, (js) `tracker.get(key).push(s)`binary string append string that matches binary to the value array
    * return `tracker.values()`, `Array.from(tracker.values())`

4. two sum, py involves enumerate() and js uses .entries()
    * create tracker -> iterate over index, element in original list -> start with else, we need to store the desired remainder for each number we iterate through that will get us to target `target-num` as a key `count[]`, value is the index of the element -> then with if statement we ask if given num is in count as well as remainder to target -> return if two number combos are found
    ```py
    tracker={}
    for index, num in enumerate(nums):
        if num in tracker:
            return [index, tracker[num]]
        else:
            tracker[num-target]=index
    ```

5. for frequent elements [ as input we are provided a list of elements of varying frequency and a desired integer 'k' of which we are to return k number of frequent elements -> return array that contains k frequent elements ]
    1. we create a empty dict to store elements and corresponding frequency, create empty array that is to have buckets of ascending frequency with elements sorted by occuring frequency using list comp w/ Python `freq = [[] for n in range(len(nums)+1)]`, we need +1 as we are starting from a n=1 bucket and JS `Array(nums.length+1).fill(null).map(() => [])`
    2. iterate through nums -> using .get() create a count storing frequency (py)`count[num]=1+count.get(num, 0)`, (js) `count.set(num, (count.get(num) || 0) +1)`
    3. iterate through resulting frequency store (python) `for n,c in count.items()`, (js) `for [num, frequency] of count.entries()`
        * append to freq array by bucket number which is the frequency with element so `freq[c].append(n)`
    4. initialize empty array `res` to store desired k frequent elements, iterate backwards through freq array `for i in range(len(freq)-1,0,-1)`
        1. since each freq index, has a bucket of elements at a given frequency bucket
            - iterate through array at freq[i] so `for n in freq[i]`
            - append/push to res
        2. if we reach a point where res array length is that of k then we stop adding `if len(res)==k: return res`


## 08/23/23
6. product of array except self [ input takes in a list of numbers of which we are to return products at the same indices inclusive of the number at said index in input ]

    * fulcrum of solution involves four comps being 1 middle array, 1 resultant array, postfix and prefix multipliers

    1. create res result that holds 1s (as we are multiplying) with the array being the length of the original array `res = [1]*len(nums)`
    2. initialize a prefix starting at 1 `prefix=1`
    3. iterate through nums `for i in range(len(nums))` till end
        1. in res array, we will update the element at indices to be prefix `res[i]=prefix`
        2. in order to calculate prefix, multiplicative sums we multiply prefix by the element at current index `prefix*=nums[i]`
        (loop continues and we multiply the multiplicative output of prefix times nums[i] and update that to be element at index in resultant array `res`)
    4. initialize postfix starting at 1 `postfix=1`
    5. we reached the end of the prefix calculative process
    6. using range, iterate from array end to start `for i in range(len(nums), -1, -1)`
        1. update element in resultant array to be multiplicative product using postfix multiplier `res[i]*=postfix`
        2. update postfix using the element at a given index in nums `postfix*=nums[i]`
    7. `return res`

7. valid sudoku ( def secondattempt, VS.py ) [ function takes in board represented by a 9*9 grid with '.' as spaces and integers and asks to determine if a board with given integers is valid i.e fulfills 3 roles which means no dups in required perspectives of rows, columns, sub grids ]
    1. initialize flag, with default of true 'board is valid sudoku' `is_valid=True`
    2. define helper function to determine if there are duplicates in a given dimension (i.e we can't have duplicates in a series) `def is_duplicates`
        1. we are going to use list comprehension in order to return an array of numbers without spaces `nums = [num for num in nums if num != "."]`
        2. then use set in order to eval if a given array has dups or not `len(nums)==len(set(nums))` -> return boolean
    3. [first iteration is for rows] iterate through board for each row and ask if any of rows contain dups `is_duplicates(row)` returns False 'not valid'
    4. [second iteration is for columns] iterate through board by column, ask if any of them contain dups using `for col in zip(*board)` 'transposing' and again `is_dups(col)`
    5. [third iteration for 3 by 3 squares] r and c will be dimensions of all the 3*3 boxes in the the 9*9 grid (ex:[0,0] is the first 3*3 [3,3] is the last grid) and are defined by range `for r in range(0,9,3)` and `for c in range(0,9,3)`
        1. using list comp, define/store each of the grids `box=board[i][j] for i in range(r, r+3) for j in range(c, c+3)`
            - [0,1,2][3,4,5][6,7,8]
        2. input in `is_duplicates(box)` which tests if there are any duplicate integers in the sub-grids
    6. return output of boolean flag `false` == non valid board, `true` else

8. encode, decode strings
    * two functions, the first `encode` taking in a list of strings to return an encoded string that has been __hashed__ and the second function `decode`
        - encode involves taking in list of strings
            1. variable storing empty string `res=""`
            2. iterate through strings list, append the length of string + delimiter + string to previous variable, to be returned
            ```py
            count=0
            for i in s:
               res+=str(len(s))+'#'+s
            ```
        - decode taking in encoded string
            1. create vars storing empty returned to be returned and pointer starting at 0 `res, first_pointer= [], 0`, the first pointer allows us to understand where we are in string i.e starting index for decoding string as well
            2. while first pointer is less than entire string length, we want j to start at i, secondly we want to determine when we encounter a delimiter, if we don't then we can continue to increment j
                ```py
                while first_pointer is less than len(str)
                    second_pointer=first_pointer
                    while element at second_pointer is not delimiter
                        increment j by 1
                ```
            3. when we do encounter the delimiter, we know
                1. the length of the string is the character from the start index to the second_pointer `length=str[i:j]`
                2. we need to append the string, starting from after the second pointer to the string end, being the length plus the start `res.append(str[j+1:j+1+length])`
                3. we know set the first pointer of the end of the last string decoded `i=j+1+length` in order to decode rest of string if applicable
                ```py
                        # first code block
                        length=str[i:j]
                        res.append(str[j+1:j+1+length])
                        i=j+1+length
                # return resulting array
                return res
                ```

## 09/28/23
9. longest consecutive sequence (def longestCon(array of integers))
    * function takes in an array of integers with the goal of outputting an integer representing the number of ints in the longest possible consecutive sequence
        1. initialize variable storing set of original array i.e unique integers `set(nums)`
        2. variable to store count of longest consecutive seq to be returned `longest=0`
        3. iterate through nums `for n in nums`
            4. we want to start at a number where preceding integers don't exist in array i.e if we start at 8 and there is a 7, we don't want to start at 8 to calculate longest
            5. if there is no preceding integer `if (n-1) not in nums:` else continue to next number
                6. store length of previous found sequence `length=0`
                7. check for next num existing in set and increment using length if next num is found
                sequence would theoretically go:
                    10 -> 10+0 is in nums -> increment length to 1 -> length is 1 -> 11 exists -> we are now at 11 -> length is now ...
                ```py
                    if (n+length) in nums:
                        length+=1
                ```
                8. if current length is greater that what is stored in longest length, then set longest to our found length of consecutive int sequence
                ```py
                    if length>longest:
                        longest=length
                ```
        9. return integer stored in longest `return longest`

/**
 * @param {list[number]} nums
 * @param {number} k
 * @returns {list[number]}
 */

// BAD TC - O(k*(n-k))
var maxSlidingWindow = function (nums, k) {
    let max_nums = []
    if (!nums) {
        return max_nums
    }
    if (nums.length == 1) {
        return nums
    }
    if (k > nums.length) {
        return Math.max(nums)
    }
    let [l, r] = [0, k]

    while (r < nums.length + 1) {
        const window = nums.slice(l, r)
        max_nums.push(Math.max(...window))
        l += 1
        r += 1
    }
    return max_nums
}

var maxSlidingWindow2 = function (nums, k) {
    let [output, deque] = [[], []] // d stores indices
    let [l, r] = [0, 0]
    while (r < nums.length) {
        /*
          3 cases
          (1) need to check if values in deque, and if last value added is less than current number
            then we can pop
          append values as we know we don't have any elements

          (2) need to update indices array to account for left most element exiting current window as we shift

          (3) asking if current window is greater than or equal to k
          (we are 0 indexed, if right most pointer is greater than or equal to desired frame length, we are at proper frame
            so push in max knowing that leftmost is largest )
             -> appending to output process (largest num)
             increment l (we've added largest number in current range)
        increment r
         */

        while (deque.length > 0 && nums[deque[deque.length - 1]] < nums[r]) {
            deque.pop()
        }
        deque.push(r)

        if (l > deque[0]) {
            deque.shift()
        }
        console.log(deque)
        if ((r + 1) >= k) {
            output.push(nums[deque[0]])
            l += 1
        }

        r += 1

    }
    console.log('this is output')
    return output

}

console.log(maxSlidingWindow2(
    [1, 3, -1, -3, 5, 3, 6, 7], 3
))
console.log(maxSlidingWindow2(
    [1], 1
))

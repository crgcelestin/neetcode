from collections import deque


class Solution:
    def maxSlidingWindow(nums: list[int], k: int) -> list[int]:
        output = []
        q = deque()  # contains indices
        l = r = 0
        print(nums)
        while r < len(nums):
            print(q)
            # before appending make sure q contains vals and top q value is less than nums[r]
            while q and nums[q[-1]] < nums[r]:
                q.pop()
            q.append(r)

            # if left val is out of bonds pop from left as we are shifting window
            if l > q[0]:
                q.popleft()

            # edge case - check if window is at least size k, since we are popping from leftmost, we will append max at current window
            if (r + 1) >= k:
                output.append(nums[q[0]])
                l += 1

            r += 1
        return output


Test1 = Solution.maxSlidingWindow([1, 3, -1, -3, 5, 3, 6, 7], 3)
print(Test1)

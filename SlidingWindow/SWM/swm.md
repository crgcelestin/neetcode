# 03.11.24

## [239. Sliding Window Maximum](https://leetcode.com/problems/sliding-window-maximum/description/)

### Problem
- Given an array of integers `nums`, there is sliding window of size `k`, moving from left of array to right -> Can only see `k` #s in window
- Each time sliding window moves right by 1 position
- Return max sliding window

### Analysis
- Input: list[int] `nums`, int `k`
- Output: list[int] `min_window`

- Algo Process
    - Restate Problem
        * need to decipher maxes in k size windows in nums array
    - Goal of Function
        * return maxes append to result array
    - Types
        * list, int
    - Assertions and Assumptions
        * nums is usually going to be a non-empty list, k is always a default integer values
    - Edge Cases
        * empty array, array with 1 element, array with length less than that of k

#### Example \#1
```py
input1 = [1,3,-1,-3,5,3,6,7]
k = 3
output1 = [3,3,5,5,6,7]

Window position                Max
---------------               -----
[1  3  -1] -3  5  3  6  7       3
 1 [3  -1  -3] 5  3  6  7       3
 1  3 [-1  -3  5] 3  6  7       5
 1  3  -1 [-3  5  3] 6  7       5
 1  3  -1  -3 [5  3  6] 7       6
 1  3  -1  -3  5 [3  6  7]      7
```

#### Example \#2
```py
input2 = [1]
k = 1
output = [1]
```

#### Constraints
- `1 <= nums.length <= 10**5`
- `-10**4 <= nums[i] <= 10**4`
- `1<= k <= nums.length`

### Preliminary Solution
- __Time Complexity__: O(k*(n-k))

- __Space Complexity__:O(n)

### [Neetcode](https://www.youtube.com/watch?v=DfljaUwZsOk)
- [Neetcode Solution](nc.py)
#### TC, SC


- #### NC Notes
* we know that the array provided is sorted/assumption is sorted integer array, can take adv of this to perform a O(n) solution as we can take advantage of eliminating lower #s
    * going to use deque
* with deque we are going to pop values that are lesser than observed max in defined range, then add highest to output -> if we find a higher value then perform the same op
    * efficient as we can perform O(1) op in O(n) time

- deque ops
    * monotonically decreasing queue where you can add/remove from end in O(1) time, remove from beginning in O(1)
    * for each shift in window, pop element that is no longer in window elements
    * perform comparisons (find greatest element) and pop lower values

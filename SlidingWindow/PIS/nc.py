class Solution:
    def checkInclusion(s1: str, s2: str) -> bool:
        # if first string is longer, its impossible for s2 to contain a permutation
        if len(s1) > len(s2):
            return False
        # initialize buckets for both i.e array of subarrays that are of len(alphabet)
        s1Count, s2Count = [0] * 26, [0] * 26

        # iterate through s1
        for n in s1:
            # we are going to record s1 char occurrences
            s1Count[ord(n) - ord("a")] += 1

        # iterate through s2 now
        for i in range(len(s2)):
            # record s2 char occurrences
            s2Count[ord(s2[i]) - ord("a")] += 1
            # only want to look in window of s1 length, decrement the character we lost with moving window i.e update window as we move across string
            if i >= len(s1):
                print(f"before${s2Count}")
                s2Count[ord(s2[i - len(s1)]) - ord("a")] -= 1
                print(f"after${s2Count}")
            # ask if both are identical
            if s1Count == s2Count:
                return True
        return False


input1 = {"s1": "ab", "s2": "eidabooo"}
print(Solution.checkInclusion(input1["s1"], input1["s2"]))

# 03.11.2024

## [567. Permutation in String](https://leetcode.com/problems/permutation-in-string/description/)

### Problem
- Given two strings: `s1`,`s2` -> return true if `s2` contains permutation of `s1`, or false otherwise
    * return `true` if 1 of `s1`'s permutations = `s2` substring

### Analysis
- Input: str (s1,s2)
- Output: boolean (true, false)

- Algo Process
    - Restate Problem
        * need to detect if permutation of one string is present in another that is 'longer'
    - Goal of Function
        * return true if permutation of s1 meaning a potential rearrangement of its characters are found in s2
    - Types
        * `str`, `boolean`
    - Assertions and Assumptions
        * s1 and s2 should always contain some length, lowercase english characters so no need for .lower()
    - Edge Cases

#### Example \#1
```
input1 = {
    s1:"abc",
    s2:"eidbaooo"
}
assert1 = True
//ex: s2 has 1 permutation of s1 ('ba')
```

#### Example \#2
```
input2 = {
    s1 = "ab",
    s2 = "eidboaoo"
}
assert2 = False
//ex: no permutation of s1 in s2
```

#### Constraints
- `1 <= s1.length, s2.length <= 10^4`
- `s1` and `s2` involve lowercase english letters

#### _Backside_
- My solution would involve:
    1. need to iterate through s2, create a stack with chars in s1
    2. we need to find subsequent chars involved in s1, if we don't then we have to break outside the loop and increment left pointer


#### Additional Details
- Reflecting on Attempt: Need to revise attempt to incorporate ideas of anagram to fit with constraints of longer string having a window of size of s1, other chars besides whats in s1, and the permutation having chars in no particular order

### Preliminary Solution
- __Time Complexity__:O(s2) -> O(n)

- __Space Complexity__: O(1)

#### Solution Code
- [JS Solution](pis.js)
- [Python Solution](nc.py)

###
- [Neetcode Solution](https://www.youtube.com/watch?v=UbyhOgBN834)
#### TC, SC

- #### NC Notes (NC solution ended up being overcomplicated, so I simplified it)
* Going to aim for O(n) tc, for both strings provide a hashmap that has count of characters in each string
- Approach:
    - Matches variable will allow us to skip having to compare hash maps, total number of = chars in each (this var is initialized at 26)
    - Have to iterate through both hashmaps at least once which is O(26) so in aggregate O(26)+O(n)->O(n)
    - if we don't encounter an exact match, then we have to shift window and update each counts respectively
    - __END__: if we get to matches = 26, we return true as we have reached a part in s2 that contains a permutation else we return False if we don't

    - handle edge case: if s1 string is longer than s2

/**
 * @param {string} s1
 * @param {string} s2
 * @returns {boolean}
 */
var checkInclusion = function (s1, s2) {
    if (!s1 || !s2) return;
    if (s1.length > s2.length) return false;
    let l = 0;
    let s1_copy;
    for (let r = 0; r < s2.length; r++) {
        s1_copy = s1.split("")
        while (s1_copy[l] == (s2[r])) {
            l += 1
        }
        // if subsequent chars in s2 are found in s1 then we increment until we iterate through entire stack and stack is empty
    }
    return l + 1 == s1.length ? true : false
}

const input1 = {
    s1: "abc",
    s2: "eidbaooo"
}
const input2 = {
    s1: "ab",
    s2: "eidboaoo"
}

console.log(
    checkInclusion(
        input1.s1,
        input1.s2
    )
)

// console.log(
//     checkInclusion(
//         input2.s1,
//         input2.s2
//     )
// )

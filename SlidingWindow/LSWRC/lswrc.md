# 1.31.24

## [3. Longest Substring Without Repeating Characters](https://leetcode.com/problems/longest-substring-without-repeating-characters/description/)

### Problem
- Given string `s`, find length of longest substr w/o repeating chars

### Analysis
- Input: str `s`
- Output: int

- Algo Process
    - Restate Problem
        * need to decipher longest sub-str with all unique chars in string
    - Goal of Function
        * return the integer representing # of chars in substring
    - Types
        * str, int
    - Assertions and Assumptions
        * string can either contain non-unique chars, or unique chars / no ints or non-str elemets
    - Edge Cases
        * string that has non-str chars
        * empty string

#### Example \#1
```
input: s = "abcabcbb"
output:3
Explanation: The answer is "abc", with the length of 3.
```

#### Example \#2
```
Input: s = "bbbbb"
Output:1
Explanation: The answer is "b", with the length of 1.
```
#### Example \#3
```
Input: s = "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.
```

#### Constraints
- `0<= s.length <= 5 *10^4`
- `s involves English letters, digits, symbols, spaces`

#### _Backside_
- My solution would involve:
    1. iterate through string and while doing so observe what the longest occurring substring is
    2. keep track using var `longest` that will be updated if we find more subsequent longer, unique character strings with non-repeats
    3. if we find using sliding window, that there aren't any segments of unique non-repeats, then we exist with answer of 1

#### Additional Details
- Reflecting on Attempt:
    * it worked, had to revise and remove flags, but tc is bad

### Preliminary Solution
- __Time Complexity__: O(i*j), j being i-1 so its O(i*2) -> n^2

- __Space Complexity__: O(1), constant memory

#### Solution Code
- [JS Solution](lswrc.js)
- [Python Solution](nc.py)

### [Neetcode](nc.py)
- [Neetcode Solution](https://www.youtube.com/watch?v=wiGpQwVHdE0)
#### TC, SC
TC, SC are both O(N)

- #### NC Notes
- Check every substring starting from 0 index with non-repeating chars
- make sure that sub-array is always composed of unique chars
    * Can utilize set in order to confirm that we have no duplicates and allows for cutting down tc to O(n)

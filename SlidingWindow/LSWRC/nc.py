# actual nc solution
class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        charSet = set()
        l = 0
        res = 0
        for r in range(len(s)):
            while (s[r]) in charSet:
                charSet.remove(s[l])
                # increment frame if we find char that repeats
                l += 1
            charSet.add(s[r])
            # finding max length involves performing comparison between current storing of max string and window of substr being the difference between the two pointers + 1
            res = max(res, r - l + 1)
        return res


# transferred from js attempts
"""
class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        longest = 0
        for i in range(len(s)):
            substr = ""
            for j in range(i, len(s), 1):
                if s[j] in substr:
                    break
                substr += s[j]
                longest = max(longest, len(substr))
        return longest
"""

/**
 * @param {string} s
 * @returns {number}
 */
// failed final test case
var lengthOfLongestString = function (s) {
    // store length of longest non-repeat substr
    let longest = 0
    // understand if we are encountering a substr with repeating chars or not
    let flag = true
    while (flag) {
        let substr = ""
        for (let i = 0; i < s.length; i++) {
            if (!substr.includes(s[i])) {
                longest += 1
                substr += s[i]
            } else {
                flag = false
            }
        }
    }
    return longest
};


var lengthOfLongestString2 = function (s) {
    let longest = 0;
    for (let i = 0; i < s.length; i++) {
        let substr = "";
        for (let j = i; j < s.length; j++) {
            if (substr.includes(s[j])) {
                break;
            }
            substr += s[j];
            longest = Math.max(longest, substr.length);
        }
    }
    return longest;
}


const s1 = "abcabcbb"
const a1 = 3
const test1 = lengthOfLongestString2(s1)
if (JSON.stringify(test1) === JSON.stringify(a1)) {
    console.log('passed')
} else {
    console.log(`expected ${a1}, got ${test1}`)
}

const s2 = "bbbbb"
const a2 = 1
const test2 = lengthOfLongestString2(s2)
if (JSON.stringify(test2) === JSON.stringify(a2)) {
    console.log('passed')
} else {
    console.log(`expected ${a2}, got ${test2}`)
}

const s3 = "pwwkew"
const a3 = 3
const test3 = lengthOfLongestString2(s3)
if (JSON.stringify(test3) === JSON.stringify(a3)) {
    console.log('passed')
} else {
    console.log(
        `expected ${a3} , got ${test3}`
    )
}

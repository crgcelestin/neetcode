"""
Given an array of N integers, and an integer K, the task is to find the number of pairs of integers in the array whose sum is equal to K.

Input: arr[] = {1, 5, 7, -1}, K = 6
Output:  2
Explanation: Pairs with sum 6 are (1, 5) and (7, -1).

Input: arr[] = {1, 5, 7, -1, 5}, K = 6
Output:  3
Explanation: Pairs with sum 6 are (1, 5), (7, -1) & (1, 5).
"""

"""
input: list[int], output: int (return # of pairs)
edge cases: arr = {}, exepected to return possible pairs -> []
iterate you would push in pairs that sum up to k
matches  = 0
1 5 7 -1   // 6
^ (l)
  ^ (r) PUSH
    ^  ^ PUSH
two pointer
while l is less than array,
  if r = k - l,

1 5 7 -1
r
{
 1: 5
 7: -1
}

{1,5,1,5} // 6
->
{
1: [5,5]
5: [1,1]
}
"""


# proved verbose, non-optimal
def findPairs(arr, k):
    if not arr:
        return 0

    count = {}

    for i in range(len(arr)):
        remainder = k - arr[i]
        if remainder in count:
            count[remainder].append(arr[i])
            # {1,5,1}, 6 -> {5:[1,1]}, {1:[5,5(1)]}
        if not count[arr[i]]:
            count[arr[i]] = []
        # account for duplicates
    # for j in range(0,len(arr)):
    #   if arr[j] == remainder:
    #     count[arr[i]].append(arr[j])

    # sum up lengths of arrays pertaining to keys
    sum_length = 0
    for key in count:
        sum_length += len(count[key])

    return sum_length

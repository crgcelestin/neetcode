"""
Given an array of N integers, and an integer K, the task is to find the number of pairs of integers in the array whose sum is equal to K.

Input: arr[] = {1, 5, 7, -1}, K = 6
Output:  2
Explanation: Pairs with sum 6 are (1, 5) and (7, -1).

Input: arr[] = {1, 5, 7, -1, 5}, K = 6
Output:  3
Explanation: Pairs with sum 6 are (1, 5), (7, -1) & (1, 5).
"""


# n being arr length, k being target
# tc of O(n)
def findPairs(arr, k):
    # establish dict to track pairs
    counts = {}
    # track # of pairs
    output = 0

    # iterate through array, at every element we are going to track what's required to get to target via remainder
    for a in arr:
        counts[a] = 1 + counts.get(a, 0)
        remainder = k - a
        # if we find that remainder, i.e. matching # to get to target
        if remainder in counts:
            # then we want to update the number of pairs possible given the occurrence of said matching number
            output += counts[remainder]
    return output


print(findPairs([1, 5, 1, 5], 6))
# 4

print(findPairs([], 1))
# 0

print(findPairs([1, 5, 7, -1], 6))
# 2

print(findPairs([1, 5, 7, -1, 5], 6))
# 3

class Star {
    constructor(name, distance_from_earth) {
        this.name = name
        this.distance_from_earth = distance_from_earth
    }
}

/**
 * @param {list[Star]} stars
 * @param {number} K
 * @returns {list[number]}
 */

var compare_distances = function (stars, K) {
    let result = []
    if (!stars) {
        return result
    }
    let i = 0;
    let distances = []
    stars.map((star) => {
        distances.push(star.distance_from_earth)
    })
    while (result.length !== K && i < stars.length) {
        let min_star = Math.min(...distances)
        if (stars[i].distance_from_earth < min_star.distance_from_earth) {
            min_star = stars[i].distance_from_earth
        }
        result.push(min_star)
        stars = stars.filter((star) => star.distance_from_earth !== min_star)
        distances = distances.filter((distance) => distance !== min_star)
        i++
    }
    return result
}

console.log(
    compare_distances(
        [
            new Star('first', 1),
            new Star('third', 3),
            new Star('second', 2),
        ],
        2
    )
)


var compare_distances2 = function (stars, K) {
    if (!stars) {
        return []
    }
    const sorted_stars = stars.sort((a, b) => a.distance_from_earth - b.distance_from_earth)
    return sorted_stars.slice(0, K)
}

console.log(compare_distances2(
    [
        new Star('first', 1),
        new Star('third', 3),
        new Star('second', 2),
    ],
    2
))

# Feb, 16 2024
- First Skilled Interview Session

## Problem Details
- Problem provided was similar to return k greatest, least elements in array problem

### Screenshots
![First Screen](s1.png)

![Second Screen](s2.png)

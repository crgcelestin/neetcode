# 08/24/23

## [125. Valid Palindrome](https://leetcode.com/problems/valid-palindrome/)

### Problem
* A phrase is a palindrome if after converting uppercase to lowercase letters -> removing non-alphanumeric characters -> string reads the same forward and backward
* alphanumeric include letters, numbers
- given string `s`, true if palindrome, false otherwise

### Analysis
- Input: we are given a string `s` as input that can be a mess i.e. it can contain a mixture of upper case letters as well as non-alpha numeric characters, type:`str`

- Output: we need to return if a given string is a palindrome so a boolean, `True` or  `False`

- Algo Process
    - Restate Problem
        - check if a word reads the same forward as it does backward

    - Goal of Function
        * we are given a string input that we need to clean i.e standardize to a lowercase string that has no non alphanumeric characters
            - use char.lower(), char is
        * after said cleaning we need to check if same string is forward as is backwards

    - Types
        * input is str, output is boolean

    - Assertions and Assumptions
        * input will only be string with str characters

    - Edge Cases
        * provided strings with integers, given an empty string

#### Example \#1
```py
input1 = 'a man, a plan, a canal: panama'
output1 = True
explanation = 'amanaplanacanalpanama is a palindrome'
```

#### Example \#2
```py
input2 = 'race a car'
output2 = False
explanation = 'raceacar is not a palindrome'
```
#### Example \#3 * \#4
```py
input3 = " "
output3 = True
explanation = 'an empty string " " after non-alpha removal is empty which is same forwards as is backward, thus a palindrome'

input4 = "1321"
output = False
explanation = 'not same number forward as backward'

input5 = "01010"
output = True
explanation = 'same number forward as backward'
```

#### Constraints
- `1<= s.length <= 2*10^5`
- s only has printable ASCII chars

### Preliminary Solution
- __Time Complexity__:

- __Space Complexity__:

#### _Backside_
- My initial solution would involve:
    - first convert each character to lower case -> remove all nonalphanumeric
        * `[char.lower() for char in s]` and utilize `char.isalnum()`
    - then with cleaned string, we can check using a binary search type strategy where there are two pointers, one starting from the end, the other from the start
        * each character with given pointers at index, need to match or else return False immediately

- Second attempt:
    - for the sake of readability
        * use list comprehension for alnum for each char, then join said only alnum chars from list to string
    - initialize two pointers, one from 0 index start, one from end list `len(list)-1`
    - while start is greater than end, we just need to check at given element for each pointer if we notice a discrepancy in order to return False else we continue until they meet at middle then return True as we know its a palindrome

#### Additional Details
- Reflecting on Attempts:
    * practicing using list comprehension in order to return characters that are only alpha numeric allowing me to then join them to avoid spaces and lowercase them
    * then use two pointers starting from end and beginning to make sure all characters are equal

#### Solution Code
- [JS Solution]()
- [Python Solution](./ValidPalindrome.py)

### [Neetcode](https://www.youtube.com/watch?v=jJXJ16kPFWg)
- [Neetcode Solution](./nc.py)
#### TC, SC

- #### NC Notes
    * 1st version
        - has deficiencies, as interview doesn't want use of .`isalnum()` or use extra memory for new string, reversal of original input string
    * 2nd version
        - two pointers from right and left ends, meet if string is equivalent
        - use ascii values to determine if value is alpha numerical
            * 0 has ascii of 48, 9 has 57
            * A starts at 65 to 90
            * a starts at 70 to 122
        - establish while with conditional `while left pointer is less than right pointer`
            * while above is still true, and we have not see an alpha numeric character then increment left pointer by 1
            * while above is still true `r>l == l<r` and no alphanum char has been seen, decrement right pointer

class Solution:
    def isPalindrome(self, s: str) -> bool:
        string = [char for char in s if char.isalnum()]
        s_cleaned = "".join(string).lower()
        """
        can also do
        s_cleaned = ''.join(char for char in s if char.isalnum()).lower()
        """
        start, end = 0, len(s_cleaned) - 1
        while start < end:
            if s_cleaned[start] != s_cleaned[end]:
                return False
            start += 1
            end -= 1
        return True


Test = Solution()
input1 = "race a car"
input2 = " "
input3 = "A man, a plan, a canal: Panama"
input4 = "1321"
input5 = "01010"
First = Test.isPalindrome(input1)
print(First == False)
Second = Test.isPalindrome(input2)
print(Second == True)
Third = Test.isPalindrome(input3)
print(Third == True)
Fourth = Test.isPalindrome(input4)
print(Fourth == False)
Fifth = Test.isPalindrome(input5)
print(Fifth == True)

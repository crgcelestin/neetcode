class Solution1:
    def isPalindrome(self, s: str) -> bool:
        newStr = "".join(char for char in s if char.isalnum()).lower()
        return newStr == newStr[::-1]


class Solution2:
    def isPalindrome(self, s: str) -> bool:
        def alphaNum(c: str):
            return (
                ord("A") <= ord(c) <= ord("Z")
                or ord("a") <= ord(c) <= ord("z")
                or ord("0") <= ord(c) <= ord("9")
            )

        # forgot you can perform spreading
        l, r = 0, len(s) - 1
        while l < r:
            while l < r and not alphaNum(s[l]):
                l += 1
            while r > l and not alphaNum(s[r]):
                r -= 1
            if s[l].lower() != s[r].lower():
                return False
            l += 1
            r -= 1
        return True

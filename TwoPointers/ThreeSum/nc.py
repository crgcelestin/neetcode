class Solution:
    def ThreeSum(self, nums:list[int]) -> list[list[int]]:
        triplets = []
        nums.sort()
        #iterate through index, value
        for i, a in enumerate(nums):
            # avoid duplicates by checking if values is same as before
            if i>0 and a==nums[i-1]:
                continue
            l, r = i+1, len(nums)-1
            # pointers can't be =
            while l<r:
                threeS = a + nums[l] + nums[r]
                # if sum is too much bigger than 0, decrement right pointer
                # else if sum is less than 0, increment left pointer
                if threeS > 0:
                    r-=1
                elif threeS < 0:
                    l+=1
                else:
                    triplets.append([a, nums[l], nums[r]])
                    # accounting for the desire of only unique triplets - check for edge
                    l+=1
                    while l<r and nums[l]==nums[l-1]:
                        l+=1
        return triplets

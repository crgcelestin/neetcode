/**
 * @param {number[]} nums
 * @return {number[][]}
 */
class Solution {
    ThreeSum(nums) {
        nums.sort()
        let triplets = []
        if (nums.length < 3) {
            return triplets
        }
        for (let i = 0; i < nums.length - 2; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue
            }
            let [j, k] = [i + 1, nums.length - 1]
            while (j < k) {
                let current = nums[i] + nums[j] + nums[k]
                if (current == 0) {
                    let triplet = [nums[i], nums[j], nums[k]]
                    triplets.push(triplet)
                    while (j < k && nums[j] == nums[j + 1]) {
                        j += 1
                    }
                    while (j < k && nums[k] == nums[k - 1]) {
                        k -= 1
                    }
                    j += 1
                    k -= 1
                }
                else if (current < 0) {
                    j += 1
                } else {
                    k -= 1
                }
            }
        }
        return triplets
    }
}

const Global = new Solution()
const input1 = [-3, 3, 4, -3, 1, 2]
let Test1 = Global.ThreeSum(
    input1
)
console.log(Test1)

# terrible
class first_Attempt:
    def ThreeSum(self, nums: list[int]):
        nums.sort()
        # maybe have i start in middle?
        i,j,k = 0, len(nums)-2, len(nums)-1
        current_sum = nums[i] + nums[j] + nums[k]
        if current_sum > 0:
            k-=1
        if current_sum < 0:
            i+=1
            j+=1
        else:
            return [i,j,k]

# aided
class Solution:
    def ThreeSum(self, nums: list[int]):
        nums.sort()
        triplets = []
        if len(nums)<3:
            return []
        for i in range(len(nums)-2):
            if i>0 and nums[i]==nums[i-1]:
                continue
            j, k = i+1, len(nums)-1
            while j < k:
                current_sum = nums[i] + nums[j] + nums[k]
                if current_sum == 0:
                    triplet = [nums[i], nums[j], nums[k]]
                    triplets.append(triplet)
                    while j<k and nums[j] == nums[j+1]:
                        j+=1
                    while j<k and nums[k]==nums[k-1]:
                        k-=1
                    j+=1
                    k-=1
                elif current_sum<0:
                    j+=1
                else:
                    k-=1
        return triplets

Test = Solution()
input1=[-3,3,4,-3,1,2]
print(input1[0:-2])
ThreeSum1 = Test.ThreeSum(
    input1
)
print(ThreeSum1)
print(ThreeSum1==[
    [[
        -3,1,2
    ]]
])

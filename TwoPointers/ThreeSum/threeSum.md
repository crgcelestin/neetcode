# 10.26.23

## [15. 3Sum](https://leetcode.com/problems/3sum/description/)

### Problem
Given an integer array `nums`, return all triplets
`[nums[i],nums[j], nums[k]]` where `i!=j, i!=k, j!=k`, and `nums[i] + nums[j] + nums[k] == 0`
- solution set can't contain duplicate triplets
### Analysis
- Input: provided an array of integers with the (assumed that array is in a random order) with the need of a triplet i.e any assortment of three integers in array adding up to 0

- Output: expected to return unique triplets with numbers that add up to 0, containing integers that are from input array

- Algo Process
    - Restate Problem
        * design function that takes in a number of integers in an unsorted array and returns indices of numbers that add to goal

    - Goal of Function
        * array indices returned in array triplets must add up to desired target of 0 i.e. all arrays must accumulate to 0 if added up [no desired sort for return]

    - Types
        - input: nums being the array, output: array containing subarrays that contain unique triplets

    - Assertions and Assumptions
        - provided valid array of only integers where we may have 1, 0 (edge case), or >1 array containing integer triplets

    - Edge Cases
        - no possible triplets that sum to 0, given an empty array
            - return empty array `[]` meaning no possible 0-sum triplet possibilities
        - given an array of 0s

#### Example \#1
```js
const input1 = [-1,0,1,2,-1,-4]
let output1 = [
    [-1,-1,2],
    [-1,0,1]
]
```
- explanation:
    - `nums[0] + nums[1]+ nums[2] = (-1+0+1) = 0`
    - `nums[1] + nums[2] + nums[4] = (0+1+-1) = 0`
    - `nums[0] + nums[3] + nums[4] = (-1) + 2 + (-1) = 0`
        * distinct triplets include [-1,0,1] and [-1,-1,2]
        * order is not a concern in output or triplets


#### Example \#2
```py
input=[0,1,1]
output=[]
```
- exp:
    - no possible triplet summing up to 0

#### Example \#3 & \#4
```py
input = [0,0,0]
output = [[0,0,0]]
```

- exp:
    - only 1 possible triplet adding to 0

#### Constraints
- `3<=nums.length<=3000`
- `-10^5<=nums[i]<=10^5`

### Preliminary Solution
- __Time Complexity__:

- __Space Complexity__:

#### _Backside_
- My initial solution would involve:
    * potentially have three pointers that track possibilities of triplets
    * we are looking for all possible combos of triplets that sum to desired target of 0, i.e. all numbers cancel each other out
    * create hashmap to store all possibilities at a given start number
        - we can iterate through set of numbers and set them as keys in a dictionary
        - key will act as start for traversal in order to find right options for next 2 numbers
    * we can fix the first number and have that as start, then we can search for the opposite of the first number we start from
        - these next two numbers must sum to the opposite of the first i.e if the start is -2, then we must find a combo of two numbers possible that sum up to 2
        - if we find that combo then add that to empty array that is stored at a given key being the start, so `tracker[2]=[[-1,-1]]` where the 2 was the start and -1, -1 where other found #s to get to 0 (all integers appearing in input)
        - then iterate to find other number possibilities while checking to make sure they are unique
            * if `[num[j],num[k]]` are not in `tracker[start]`, append to array, else continue


#### Additional Details
- Reflecting on First Attempt
    * terrible

- Reflecting on Second Attempt:
    - Second attempt:
        * Upon reflection, the second attempt was more thought out and allowed for better optimization
            1. Start with an already sorted array (known)
            2. declare array structure
            3. determine if array can form a triplet
                - array length can't be less than 3
            4. iterate through array starting from 0 and ending at a position that excludes the last two elements in given array
                5. avoiding duplicates involves checking to see if position is past 1st element and then asking via conditional, is the element at current index the same as the one at prior index
                    - if so skip via continue
                6. initialize two pointers, 1 starts at element after current index, 1 is the last element in the array
                    - `j = i+1`
                    - `k = len(nums)-1`
                7. while `j<k`, meaning as our second pointer is less than the third starting at the last element we will loop
                    8. we want to store our running sum, meaning aggregate elements at provided pointers -> store in var `current_sum=elem@j, elem@k, elem@i`
                        9. if we have reached our desired goal of the sum being 0, we want to append that current triplet combo to the `triplets` array
                        10. we also want to check that while the third pointer is greater than the second, `j<k` that there are no duplicates as it concerns our second pointer `nums[j]==nums[j+1]`, go to next element for j `j+=1`
                        11. the same as above, we want to check for the same first condition and that there are no duplicates as it concerns the third pointer, `nums[k]==nums[k-1]`, go to element prior to its initial start being the last element `k-=1`
                        11. during this entire loop sequence we want to be moving the third pointer inwards and the second pointer outwards, `j+=1, k-=1`
                    12. (as we know array is sorted) if the sum is less than the desired 0, we have to move the second pointer out `j+=1`, if its greater, then move the third pointer in `k-=1`
            13. returning triplets as we know we have accounted for a number of edge cases
    - much better approach that passes test cases and accounts for edge cases, optimizes properly


#### Solution Code
- [JS Solution](3Sum.js)
- [Python Solution](3Sum.py)

### [Neetcode](https://www.youtube.com/watch?v=jzZsG8n2R9A)
- [Neetcode Solution](nc.py)
#### TC, SC
* Time: O(n log n) + O(n^2)
    - Sorting requires onlogn tc, two nested arrays to determine two other values from first to gain O(n^2)
    - space can be O(1) or O(n) depending on library used

- #### NC Notes
* 1st step in solving the issue of duplicate triplets (existent in this problem) is to first sort array
    - if we find duplicate values, (understanding that we are tracking three pointers), do not reuse them again
    - two more elements left after finding a start, then it is a twosum problem
        * two nested iterations within one iteration using first pointer
    - iterate through both elements, indices in provided array that has been sorted

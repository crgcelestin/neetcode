/**
 * @param {number[]} numbers
 * @param {number} target
 * @return {number[]}
 */
var twoSumII = function (numbers, target) {
    const tracker = {}
    for (const [index, num] of numbers.entries()) {
        if (tracker[num] != undefined) {
            return [tracker[num] + 1, index + 1]
        } else {
            tracker[target - num] = index
        }
    }
}

console.log(JSON.stringify(twoSumII([1, 2, 3], 3)) === JSON.stringify([1, 2]));
console.log(JSON.stringify(twoSumII([2, 7, 11, 15], 9)) === JSON.stringify([1, 2]));
console.log(JSON.stringify(twoSumII([3, 2, 4], 6)) === JSON.stringify([2, 3]));
console.log(JSON.stringify(twoSumII([3, 3], 6)) === JSON.stringify([1, 2]));

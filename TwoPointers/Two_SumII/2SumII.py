class Solution:
    def twoSum(self, numbers: list[int], target: int) -> list[int]:
        tracker={}
        for index, num in enumerate(numbers):
            if num in tracker:
                return [tracker[num]+1, index+1]
            else:
                tracker[target-num]=index

Test = Solution()
input1 = [2,7,11,15]
target1 = 9
TwoSum1 = Test.twoSum(
    input1, target1
)
print(TwoSum1==[1,2])
input2 = [2,3,4]
target2 = 6
TwoSum2 = Test.twoSum(
    input2, target2
)
print(TwoSum2==[1,3])
input3 = [-1,0]
target3 = -1
TwoSum3=Test.twoSum(
    input3, target3
)
print(TwoSum3==[1,2])

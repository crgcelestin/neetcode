class Solution:
    def twoSumII(self, numbers: list[int], target:int) -> list[int]:
        l,r = 0, len(numbers)-1
        while l<r:
            current_sum=numbers[l]+numbers[r]
            if current_sum>target:
                r-=1
            if current_sum<target:
                l+=1
            else:
                return [l+1, r+1]
        return []

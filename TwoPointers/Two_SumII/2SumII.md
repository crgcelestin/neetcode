# 09/09/23

## [167. Two Sum II - Input Array Is Sorted](https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/)

### Problem
- provided a 1-indexed array of non-decreasing order sorted integers `numbers`, find 2 numbers that add up to `target` number
- 2 numbers are numbers[index_1], numbers[index_2] where `1 <= index_1 < index_2 < numbers.length`

- Return indices of 2 nums, `index_1` and `index_2` added by 1 as integer array `[index_1, index_2]` of length 2
- Only one exact solution, don't use same element twice

- Solution only uses O(1) space

### Analysis
- Input: provided 1-indexed array of integer elements that are sorted as well as a target integer that elements at specific indices in return array are to add up to
- Output: return array that contains indices of two numbers with 1 added of length 2 that reaches target

- Algo Process
    - Restate Problem:
        * design function that takes in array of int elements sorted and return indices

    - Goal of Function
        * array indices returned in order of elements in input array that add to target (index starting at 1)

    - Types
        * two inputs: array of numbers, int target
        * output: array of numbers

    - Assertions and Assumptions
        - provided valid and possible target, provided only ints and array of ints where applicable

    - Edge Cases
        - Empty array, target is - or 0, target is not provided
        - target is larger than any combos, target is smaller than any combos

#### Example \#1
```
Input: numbers = [2,7,11,15], target = 9
Output: [1,2]
Explanation: The sum of 2 and 7 is 9. Therefore, index1 = 1, index2 = 2. We return [1, 2].
```

#### Example \#2
```
Input: numbers = [2,3,4], target = 6
Output: [1,3]
Explanation: The sum of 2 and 4 is 6. Therefore index1 = 1, index2 = 3. We return [1, 3].
```
#### Example \#3
```
Input: numbers = [-1,0], target = -1
Output: [1,2]
Explanation: The sum of -1 and 0 is -1. Therefore index1 = 1, index2 = 2. We return [1, 2].
```

#### Constraints
- `2 <= numbers.length <= 3*10^4`
- `-1000 <= numbers[i] <= 1000`
- `numbers` sorted in non-decreasing order
- `-1000 <= target <= 1000`
- tests generated to return 1 solution

### Preliminary Solution
- __Time Complexity__: O(n) time complexity, iterating through

- __Space Complexity__: O(n) tracker can grow in terms of key-value pairs in the worst case scenario

#### _Backside_
 - Approach: Used the original TwoSum solution which involves iterating through an array of integers via element, index
    * initialize map -> use enumerate to iterate via number, index -> start from else which is storing the `desired target - current` number as a key with the value being the current `index`
    * as we know that the desired number (remainder) to get to target is now matched with the original number encountered, `if num in target`, just return the value at said key and current index while accounting for it being 1-index `return [tracker[num]+1, index+1]`


#### Additional Details
- Attempt seems to be simplistic and similar to original two sum solution just with accounting for starting at 1 index, maybe there is a more optimized approach

#### Solution Code
[JS Solution](2SumII.js)
[Python Solution](2SumII.py)

### [Neetcode](https://www.youtube.com/watch?v=cQ1Oz4ckceM)
- [Neetcode Solution](nc.py)
#### TC, SC
TC and SC of O(n)

- #### NC Notes
* look at all potential combos starting from first element,
- when we find a max then we can cross off other elements that would only create a sum > target from consideration
- when finding solution, don't have to consider elements following after said current element
- Brute force solution where we iterate over length n, n times for each element -> O(n^2)
- Array is sorted so use two-pointers in order to optimize time complexity

- initialize two pointers, one starting from 1st element in numbers array and one starting from end
    - while the first pointer is less than second,
        - we want to track the current sum with elements located at pointers
        - if current sum is greater than target and we know array is sorted, then decrement second
        - if current sum is less than t, increment first
        - if we get to target with elements at both pointers we can return indices at both pointers, since we know both numbers when summed = target
    - else we can't get to target with both numbers, return empty array

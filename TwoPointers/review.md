# 12.9.23
- valid palindrome
    - goal of valid palindrome is to test if an input string reads forward as it is backward, for both solutions we want to avoid non alphaNumeric characters
        * naive approach
            1. initialize variable to store result of .join operations("".join())
                - .join(input).lower(), input is going to be a list comprehension where we iterate through provided string while asking if char is an alphanumeric character and we will make sure they are lowercased * `char for char in s if char.isalnum()`
            * this variable now contains the cleaned string without alphanumeric characters and all elements in lowercase
            2. this string with all edge cases eliminated allows us to return a boolean
                * check if image reversed `newStr[::-1]` is equal to the actual string `newStr`
        * non-naive approach
            1. we will define a helper function that tests if character is/isn't an alpha-numeric character, which will involve ord `ord(`{alpha-numeric char}`)` contained in a return statement
                - ```py
                    return(
                        ord('A')<= ord(`input`) <= ord('Z')
                        or ord('a') <= ord(`input`) <= ord('z')
                        or ord('0') <= ord(`input`) <= ord('9')
                    )
                  ```
                - test character to be within these required ord boundaries
            2. process of iterating through to determine palindrome with helper function defined above
                1. initialize 2 pointers, one starting at 0, other starting at string's end `l,r = 0, len(str)-1`
                2. initialize while loop where l has to continue to be less than r `while l<r`
                    1. then two while loops to determine if first conditional is still valid and that we are not considering chars that aren't alphanumeric
                        - for l pointer, `while l<r and not alphanum(s[l]) l+=1`
                        - for r pointer, `while r>l and not alphanum(s[r]) r-=1`
                    2. for each pointer, we want to check if corresponding element if lowered are the same
                        - if we get any occurrence where its not true, `return False`
                    3. throughout, we increment left, decrement right
            3. `return True`


- two sum ii
    * two sum ii is two sum i but indices are now starting at index of 1
        - for this we are using a two pointer approach instead,
        `l, r = 0, len(numbers)-1`
        - `while l<r`(check if they dont point to same index)
            1. we make sure to check the current sum as we iterate, `numbers[l] + numbers[r]` with `current_sum=numbers[l]+numbers[r]`
            - the assumption is that we are searching for a sum === target in a sorted array
            2. check if we are greater than target
                `if current_sum>target: r-=1`
            3. check if we are less than target
                `if current_sum<target: l+=1`
            4. check if we are equal to target
                `else: return [l+1,r+1]`
        - else we can't find results `return []`

- 3sum
    - the goal is to return all possible triplets where they add up to 0
        0. initialize triplets array, `triplets = []`
        1. sort nums, `nums.sort()` or with some other sorting algo
        2. enumerate through nums array, `for i, a in enumerate(nums)`, `for const [i,a] in nums.entries()`
            - we need to know we aren't at first element in order to check that our next element is not the same as the previous `if i>0 and a==nums[i-1]`, if this conditional is true, we will continue
            - initialize two pointers, left @ after 1st element, right @ end of list `l, r = i+1, len(nums)-1`
            3. while l is less than r `while l<r`
                4. we will have a var store sum @ the current elements `threeS = a + nums[l] + nums[r]`
                5. ask if we are greater than 0 or if we are less than 0 else we will append that combo as it reaches 0 target
                    - `if threeS > 0: r-=1` decrement right pointer
                    - `if threeS < 0: l+=1` increment left pointer
                    - we have reached a triplet with sum of 0
                        6. `triplets.append([a, nums[l], nums[r]])`
                        * we also want to account for edge case where we have duplicates
                        `l+=1` increment left pointer
                        `while l<r and nums[l]==nums[l-1]` check if still in bounds and if we find that we get a number that is same at previous index of l, `l+=1` increment left pointer
        7. return triplets

# 01.10.23
- container w/ most h2o
    * the goal being to find the largest possible water container you can calculate given two lines forming a container with x-axis
        0. initialize variable to store max area `res = 0`
        1. then initialize two pointers pointing to l,r indices starting from end and start of array, [l,r] -> 0, end of array
        2. we want to continue to make sure our left pointer is prior right, `while l<r`
            - area of a container = w * h, `area = w(r-l) * h[ min(heights[l], heights[r]) ]`, height being the minimum between heights at respective indices
            - res will continue to be updated (in the event) that we discover containers with larger areas, `res = max(res, area)`
            - we want to continue to find all potential areas using the conditional `if heights[l]<heights[r]` that is dependent on which pointer we find the larger height on
                * if left pointer is larger, increment towards end of array, else increment right towards beginning, `l+=1 else r-=1`

- trap rain h2o
    * while seeming similar, there is a demonstrable difference between trap rain water and container w/ most h2o. Trap rain water involves an elevation map with non-fillable blocks, i.e water is contained on surface with `n` blocks being equivalent to an `h` height at each respective index and container with most h2o we are just concerned about water fitting in between two boundaries with close heights over a distance
        - i.e. we are concerned about water being captured top of blocks over provided region as opposed to water constrained by two rod boundaries without consideration for intermediate elements
        1. test for no heights or if heights has less than 3 elements, i.e we can't trap water `if not h, h<3`
        2. establish two pointers at start, end `l, r -> 0, len(heights)-1`
        3. store heights at pointer indices, `lMax, rMax = heights[l], heights[r]`
        4. initialize sum to store total `sum = 0`
        5. left pointer constrained by right, `while l<r`
            0. we want to start at the pointer that has the lesser height for the sake of simplicity thus a conditional to determine where that start should occur `if lMax<rMax`
                1. if we encounter the height of left being less than that of right, then we increment left pointer -> find max between current lMax and at current index -> finally add it to running total of trapped water which is the gap difference between the previous max block and current if it is larger
                    ```py
                    l+=1
                    lMax = max(lMax, heights[l])
                    sum+=lMax-heights[l]
                    ```
                2. if we encounter a situation where height at right pointer is less -> we decrement right pointer -> we determine max between height at right pointer and previous rightMax -> add difference to running total of trapped water
                    ```py
                    r-=1
                    rMax = max(rMax, heights[r])
                    sum+=rMax-heights[r]
                    ```
        6. return total `return sum`

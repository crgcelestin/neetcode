# 11/05/23

## [Trapping Most Rain H2O](https://leetcode.com/problems/trapping-rain-water/description/)

### Problem
* Given `n` non-negative integers that represent elevation map where bar width is each `1`, calculate quantity of h2o that can be trapped following a rain event

### Analysis
- Input: given an array of type integers `height`
- Output: return an integer of max h2o said grid of heights `n` in heights `list[int]`

- Algo Process
    - Restate Problem
        * return max volume that a grid with heights at indices defined by provide input array
    - Goal of Function
        * we need to accurately asses what the max volume of h2o can be trapped amid blocks defined by boundaries given by specified heights
    - Types
        * list[int]
        * int
    - Assertions and Assumptions
        * assume that only input is of type array, that the elements in said array are of type integer
        * we assume that 0 is the ground and that water can be fit in said spaces amid other blocks
        * we will not be given negative numbers
    - Edge Cases
        * array that has all 0s or an array that contains elements with same non-0 integer
        * array that has elements where they increase from start hit an apex in middle and then decrease (a hill can't store water only valleys)
        * array that has less than 3 integers i.e can't trap water with only 2 boxes or 1 box or 0 boxes

#### Example \#1
![Alt text](image.png)
```
heights1 = [0,1,0,2,1,0,1,3,2,1,2,1]
output = 6
```
* above array is the numerical representation of the elevation map (image) and 6 units of rain water (blue) is trapped

#### Example \#2
```
heights2 = [4,2,0,3,2,5]
output = 9
```
#### Example \#3 & \#4
```
heights3 = [0,2,0,2,1,0,1,3,2,1,2,1]
output = 7

heights4 = [4,1,0,3,2,5]
output = 10
```

#### Constraints
- `n == height.length`
- `1 <= n <= 2 * 10^4`
- `0 <= height[i] <= 10^5`

#### _Backside_
- My initial solution would involve:
    * looking at what returns output 6 for first input, first stretch involves first non-zero block is 1 then 0 -> 2
        - results in 1 block of water
            * min(2,1) * 1 - 1 = 1
    * 2nd stretch, involves 2 then 1 -> 0 -> 1 -> 3
        - results in 4 blocks
            * min(2,3) * 3 - 2*1 = 4
    * 3rd stretch, 2 then 1 -> 2
        - results in 1 block
            * min(2,2) * 1 - 1 = 1
    * my thought process would be to find the minimum height as it relates to the gap between non zero heights * distance to a minimum equal after a deviation then subtract existing blocks that would reduce expected block space

##### Solution Notes
- solution appears to utilize memoization in order to perform precalculations
- starting from line 8
    * `left_max, right_max = [0] * n`
        - intialize 2 array of input array length only containg 0s
        - store max height left and right of each element
    * `left_max[0]=heights[0]`
        - base case for left_max is 1st element
    * 1st for loop
        - start from the second element (index 1) till end of array
        - set left_max pointer to be the max between the current element and previous obtained max
        - computer max height that is left of each element
    - `right_max[n-1] = heights[n-1]` is base case for right_max pointer being the last element
    * 2nd for loop
        - iterate through array in reverse order using i for right_max
        - inner comparison, allows for computing max height to right of each element `i+1` being the previous if iterating in reverse

    - declare trapped var in order to store volume of trapped h2o to be returned
    * 3rd for loop
        - iterate through precomputed left and right, find min which will be the actual amount of volume trapped between blocks and then we have to subtract current block height
        - this subtraction ensures that we aren't incorrectly assuming water is trapped when it can't be


#### Solution Code
- [JS Solution]()
- [Python Solution](trw.py)

### [Neetcode](https://www.youtube.com/watch?v=s9fokUqJ76A)
- [Neetcode Solution](nc.py)
#### TC, SC
* with 2 given solutions, tc is going to be O(n)

- #### NC Notes
*  like previous problem involving container with most water we need to take a min depending on respective heights
    - min(height[l], height[r])
    - equation for finding trapped h2o at given height, `min(l, r) - h[i]` used to generate individuals sections of trapped water and trapped volume sum
* 1st method involves O(n) memory
    - for every position to understand potential h2o that is to be able to stored, make an array and store calculations made
    - iterate through array to gain maxLeft value where we check preceding value as we navigate to the right as well as maxRight using 2 pointers
    - then have a tracker of minium height, based on either pointer
    - using min of (l, r) subtract from input elements in order to determine total trapped volume
* O(1) sc method
    - l, r (0, end of array) as well as a maxL, maxR pointers for keeping track of maxes so far at respective pointers
    - with given example, its concluded that maxL is going to be the bottleneck as it was shifted as a result of the equivalence of l<r

* 2nd methods involves O(1) memory - shown in `nc.py` file

# TOO COMPLICATED
class Solution:
    def trap(self, heights: list[int]) -> int:
        h = len(heights)
        if not heights or h < 3:
            return 0
        left_max, right_max = [0] * h, [0] * h
        left_max[0] = heights[0]
        for i in range(1, h):
            left_max[i] = max(left_max[i - 1], heights[i])
        right_max[h - 1] = heights[h - 1]
        for i in range(h - 2, -1, -1):
            right_max[i] = max(right_max[i + 1], heights[i])
        trapped = 0
        for i in range(h):
            trapped += min(left_max[i], right_max[i]) - heights[i]
        return trapped


TestCases = Solution()
heights1 = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
heights2 = [4, 2, 0, 3, 2, 5]
heights3 = [0, 2, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
heights4 = [4, 1, 0, 3, 2, 5]
print(TestCases.trap(heights1))
print(TestCases.trap(heights2))
print(TestCases.trap(heights3))

class Solution:
    def trap(self, heights: list[int]) -> int:
        if not heights or len(heights) < 3:
            return 0
        l, r = 0, len(heights) - 1
        leftMax, rightMax = heights[l], heights[r]
        sum = 0
        while l < r:
            # amt of water trapped is dictated by the lower height of current volume boundary
            if leftMax < rightMax:
                """
                move pointer,
                if we observe current leftMax having < value compared to rightMax
                """
                l += 1
                # update leftMax on each respective move of left pointer
                leftMax = max(leftMax, heights[l])
                """
                retrieving subtractive product between observed leftmax height
                and height at current index of pointer
                to be added to sum
                in order to understand the viable volume of
                liquid able to be stored
                """
                sum += leftMax - heights[l]
            else:
                r -= 1
                rightMax = max(rightMax, heights[r])
                sum += rightMax - heights[r]
        return sum


TestCases = Solution()
heights1 = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
heights2 = [4, 2, 0, 3, 2, 5]
heights3 = [0, 2, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
heights4 = [4, 2, 0, 3, 2, 5]
print(TestCases.trap(heights1))
print(TestCases.trap(heights2))

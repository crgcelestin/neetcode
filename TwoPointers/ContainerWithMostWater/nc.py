class Solution:
    def maxArea(self, heights: list[int]) -> int:
        res = 0
        l, r = 0, len(heights) - 1
        while l < r:
            area = (r - l) * min(heights[l], heights[r])
            res = max(res, area)
            # updating left, right pointers to maximize both
            if heights[l] < heights[r]:
                l += 1
            else:
                r -= 1
        return res


Test1 = Solution()
input1 = [2, 4, 4, 5, 2]
assert1 = 12
input2 = [1, 1]
assert2 = 1
input3 = [1, 8, 6, 2, 5, 4, 8, 3, 7]
assert3 = 49

First = Test1.maxArea(input1)
print(First == assert1)

Second = Test1.maxArea(input2)
print(Second == assert2)

Third = Test1.maxArea(input3)
print(Third == assert3)

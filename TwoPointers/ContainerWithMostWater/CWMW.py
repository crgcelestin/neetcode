# BRUTE FORCE
class Solution:
    def maxArea(self, height: list[int]) -> int:
        combinations = []
        for i in range(len(height) - 1):
            for j in range(i + 1, len(height)):
                possible_h = min(height[i], height[j])
                diff = j - i
                rect = possible_h * diff
                combinations.append(rect)
        print(combinations)
        return max(combinations)


Test1 = Solution()
input1 = [2, 4, 4, 5, 2]
input2 = [1, 1]
input3 = [1, 8, 6, 2, 5, 4, 8, 3, 7]
First = Test1.maxArea(input1)
print(First)

Second = Test1.maxArea(input2)
print(Second)

Third = Test1.maxArea(input3)
print(Third)

# 10.30.23

## [11. Container With Most Water](https://leetcode.com/problems/container-with-most-water/description/)

### Problem
* We are provided with an array `height` containing integers of length `n`.
* There are `n` such vertical lines drawn where the two endpoints of `ith` line are `(i,0) and (i, height[i])`
    * ex: if given [1,8,6]
    * second hypothetical line is drawn by [2,0] and [2, `height[2]` 8]
    * line at x=2, with height of 8
* problem requires us to find the two lines that when forming the boundary of a container, contain the highest volume potential possible of water
    - no container slanting

### Analysis
- Input: provide an array of integers, where we can visualize with indices of elements being where lines occur on a graph and the elements corresponding to respective line heights

- Output: return max volume possible given the elements in the array, taking account index position and heights

- Algo Process
    - Restate Problem
        * design a function given an array of elements where there index-distance is 1:1 with theoretical rectangle width (indices = x position) and their values correspond to height (values = y position), return the highest possible value given these parameters

    - Goal of Function
        * we need to return the volume of the greatest container given height and width possible with theoretical lines on a graph

    - Types
        * integer, array, integer array

    - Assertions and Assumptions
        * container may not be slanted meaning we can't finesse the array numbers to give us a larger than possible container, we must retain a regular rectangular shape

    - Edge Cases
        * given an empty array, potentially an array that contains non-integer values, an array with only 1 value, array with only 0 values or values of 0

#### Example \#1
![Alt text](image.png)
```
height = [1,8,6,2,5,4,8,3,7]
output = 49
```
- picture includes vertical lines represented by given height array, max area of container is 49 when considering given elements and range
    * width is 7 [8,6,7,2,5,4,8,3,7] and height of 7 -> 7^2 -> 49
    * as opposed to thinking it was 8, where width is 5 [8,6,2,5,4,8] and height of 8 -> 40
- red lines are the boundaries of the max volume container desired

#### Example \#2
```
height = [1,1]
output = 1
```
#### Example \#3 & \#4
```
height = [2,4,4,5,2]
output = 8
```

#### Constraints
- `n == height.length`
- `2 <= n <= 10^5`
- `0 <= height[i] <= 10^4`

### Preliminary Solution
- __Time Complexity__: O(n^2)

- __Space Complexity__: O(n)

#### _Backside_
- My initial solution would involve: First I would have to look through the input array containing elements that represent heights at those prescribed indices. I would want to iterate through and find a max while taking into account that there will have to be an optimization as it relates to width and height, which means having to iterate in O(n) time where we are storing only max and looking for points that have close tall heights and the largest width respectively.


#### Additional Details
- Reflecting on First Attempt: was able to get the tests provided on page to run correctly provided with my implementation which is iterating through all possible options of boundaries via pointers and finding max of said combos to gain largest volume
    * exceeded time limit on LC meaning my tc is terrible

#### Solution Code
- [JS Solution]()
- [Python Solution](CWMW.py)

### [Neetcode](https://www.youtube.com/watch?v=UuiTKBwPgAo)
- [Neetcode Solution](nc.py)
#### TC, SC
- O(n) time complexity, O(1) space complexity as only max volume is being stored


- #### NC Notes
* Check each possible combination of left and right pointer
    - Brute Force: Try every single possible container to be made
    ```py
    class Solution:
        def MaxArea(self, height: List[int])-> int:
            res = 0
            for l in range(height):
                for r in range(l+1, len(height))"
                area = (r-1)*min(height[l], height[r])
                res = max(res, area)
            return res
    ```
    - has a tc of O(n^2) meaning we need to get towards a linear tc
    - O(N) approach
        * involves shifting pointer that has lowest height first
        * if first pointer has a height of 1, and last has a height of 7, it is sensical to shift left pointer in to generate probable max area
        * by shifting, the ultimate ending condition is that both pointers are equivalent (shifting occurs on the pointer that has larger height incoming)

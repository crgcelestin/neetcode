class Solution:
    def flip_to_target(self, target: str, initial="00000") -> int:
        flips = 0
        for i in range(len(target)):
            if initial[i] != target[i]:
                flips += 1
                initial = initial[:i] + target[i] + initial[i + 1 :]
                initial = initial[: i + 1] + "".join(
                    "1" if c == "0" else "0" for c in initial[i + 1 :]
                )
        return flips


Test = Solution()
print(Test.flip_to_target("0101"))

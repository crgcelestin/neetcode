# Question 2
[Solved](q2.py)
## Description
- Given filename denoting text file in current working dir
- Create output file w/ name `bytes_` prefixed to filename `bytes_filename` store info about large responses
- Write to output file:
    1. first line contains # of requests w/ >5000 bytes sent in response
    2. second line contains total sum of bytes sent by all responses sending >5000 bytes

## Example
```py
origin = 'hosts_access_log_00.txt'
'''
-> process records in origin -> create output file name
'''
output = 'bytes_hosts_access_log_00.txt'

'''
- Given log file with list of GET requests delimited with double quotes and spaces

- Sample and structure of text file containing log entires
    * sample log record
    unicomp6.unicomp.net
    -- [01/Jul/1995:00:00:06-0400]
    "GET /shuttle/countdown/HTTP/1.0" 200 3985
'''
```

## Approach
- researched post oa on how to read one file and then write to another file with different name i.e. make a copy of one file with a differing name
```py
with open(file1, 'r') as f1, open(file2, 'w') as f2:
    f2.write(f1.read())
```

# Question 1
## Description
1. Starting with initial string of 0s (5 zeros)
2. Choose a digit to flip
    - ex: 1000, then choose 1st digit, number is flipped to 0111
3. When digit is flipped, value and digits to the right of it are flipped between 0 and 1
* Goal: Given a provided target string of binary digits, determine min # of flips req to achieve target

## Example
```py
target = '01001'
'''
initial string - 00000
flip 3rd digit -> 00111
flip 2nd digit -> 01000
flip 4th digit -> 01011
3 flips required to reach target, return value is 3
'''
```

## Constraints
- `1<= target <= 10^5`
- `0<= target[i] <= 1`
- `target string consists of digits 0 and 1`

## Approach
- Given that we start at the same initial position of 5 zeros we need to be able to design an algorithm thats evaluates for each particular target (a) the number of flips and (b) the positions i.e integers required to be flipped to reach target
- input is target:int, output: flips:int

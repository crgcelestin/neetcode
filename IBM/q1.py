# non recursive implementation
class Solution:
    def min_flips(self, target: str) -> int:
        # initialize starting conditions
        # we know counter for flips starts at 0 initially, initial start is 5 0s (00000)
        flips = 0
        current = "00000"
        # iterate through string in order to understand what digits required in order to get target, from initial i.e whats difference between current and ending
        for i in range(len(target)):
            # if we encounter mismatch, increment flips
            # current will not encompass elements prior to desired flipped digit (current[:i]),desired flipped digit at desired index (target[i]), and digits after flipped (current[i:+1:])
            if target[i] != current[i]:
                flips += 1
                current = current[:i] + target[i] + current[i + 1 :]
                # flip digits to right of current position
                current = current[: i + 1] + "".join(
                    "1" if c == "0" else "0" for c in current[i + 1 :]
                )
        return flips


TestSuite = Solution()
target1 = "01001"
Test1 = TestSuite.min_flips(target1)
print(Test1)
print(Test1 == 3)

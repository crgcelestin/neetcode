class Solution:
    def process_request_bytes(self, input_f):
        output_f = f"bytes{input_f}"
        with open(input_f, "r") as f1, open(output_f, "w") as f2:
            # initialize counters
            large_resp = 0
            total_large_resp_bytes = 0
            # iterate through each line in input file
            for line in f1:
                # each log entry is on new line
                log_entry = line.strip()
                # parse log entry
                parts = log_entry.split()
                if len(parts) >= 10:
                    # obtain integer of response_code
                    response_code = int(parts[-2])
                    # obtain integer of processed bytes
                    response_bytes = int(parts[-1])
                    # only considering a response if the request responds with 200 ok and the response has more than 5000 bytes
                    if response_code == 200 and response_bytes > 500:
                        large_resp += 1
                        total_large_resp_bytes += response_bytes
            # output results
            """
            1st line contains number of requests
            2nd line has total sum of bytes
            """
            output_f.write(str(large_resp) + "\n")
            output_f.write(str(total_large_resp_bytes) + "\n")

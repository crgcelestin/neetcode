# LCA - neetcode notes
## [Problem](https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/submissions/1226369381/)
## [Video](https://neetcode.io/roadmap)

### Concept
- Given a Binary Search Tree and two nodes, find lowest common ancestor -> lowest node such that p and q are descendants or p and q are equal
- compare values of each node and compare start of each subtree
    * if two values are both greater than given root, then y(root.right) is a parent of both
    * if two values are less than given root, then x(root.left) is a parent of both
    * if one value is less and one value is greater than root, return root
    * if one node = root node, then return root as other node (if it exists) has to be a descendant of root

### tc, sc
tc: O(log n) -> height of tree (tree depth), sc: O(1), constant storage

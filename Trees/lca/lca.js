// Prior LCA solution
function LCA(root, node1, node2) {

    const find_ancestors = (tree, node, path = []) => {
        // end and return ancestors
        if (node === null) {
            return path
        }
        // exploring unexplored path
        if (node.right === null || node.left === null) {
            return null
        }
        let left_path = find_ancestors(node, node.left, path + node.value)
        let right_path = find_ancestors(node, node.right, path + node.value)

        //not unique
        if (left_path && right_path) {
            return
        }
        if (left_path === null) {
            return right_path
        }
        if (right_path === null) {
            return left_path
        }
        return null
    }

    let node1_ancestors = find_ancestors(root, node1)
    let node2_ancestors = find_ancestors(root, node2)
    // find intersection between node1_a, node2_a
    let common = node1_ancestors.intersection(node2_ancestors)
    // iterate backwards in node1_a and ask if in common
    for (let i = node1_ancestors.length; i > 0; i--) {
        if (common.includes(node1_ancestors[i])) {
            return node1_ancestors[i]
        }
    }
    return null
}

// nc
/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 */
var lowestCommonAncestor = function (root, p, q) {
    while (true) {
        if (root.val < p.val && root.val < q.val) {
            root = root.right
        } else if (root.val > p.val && root.val > q.val) {
            root = root.left
        } else {
            return root
        }
    }
}

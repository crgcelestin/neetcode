# 04/13/24

## [110. Balanced Binary Tree](https://leetcode.com/problems/balanced-binary-tree/description/)

### Problem
- Given a binary tree, determine if it is height balanced

### Analysis
- Input: array of nodes (Tree Node: val, left, right properties)
- Output: boolean (True, False)
    * height balanced = binary tree where depth of two subtrees  of every node does not differ by one
- Algo Process
    - Restate Problem: Need to determine if a tree adheres to the prescribed height-balanced definition

    - Goal of Function: return true, false depending on if given input tree adheres to definition

    - Types: list[treeNode], treeNode (self.val, self.right, self.left)

    - Assertions and Assumptions:
        * only given tree-array representations with vals that are numbers
        * assume empty array = empty tree

    - Edge Cases:
        * empty array, array with random assortment, array with values that are identical, array with 1 val


#### Example \#1
```js
root1 = [3,9,20,null,null,15,7]
output1 = true
```

#### Example \#2
```js
root2 = [1,2,2,3,3,null,null,4,4]
output2 = false
```
#### Example \#3 & \#4
```js
root3 = []
output3 = false
```

#### Constraints
- number of nodes in tree are in range [0,  5000]
- -10**4 <= Node.val <= 10**4

### Preliminary Solution
- __Time Complexity__:

- __Space Complexity__:

#### _Backside_
- My solution would involve:


#### Additional Details
- Reflecting on Attempt:

#### Solution Code
[JS Solution]()

### [Neetcode]()
- [Neetcode Solution](nc.py)
#### TC, SC

- #### NC Notes

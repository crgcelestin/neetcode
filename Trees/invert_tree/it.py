def invert_tree(self, root):
    if not root:
        return root

    # swap
    root.left, root.right = root.right, root.left

    # recurse on left, right branches
    left = self.invert_tree(root.left)
    right = self.invert_tree(root.right)

    return root

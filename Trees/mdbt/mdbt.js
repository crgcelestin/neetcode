/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */

// close but not quite
var maxDepth = function (root) {
    if (root == null) return 0;
    let depth = 1;
    let currentNode = root

    const findDepth = (node, depth) => {
        if (!node) return
        if (node) depth += 1
        if (node.right) findDepth(node.right, depth + 1)
        if (node.left) findDepth(node.left, depth + 1)
    }
    findDepth(currentNode, depth)
    return depth
};


// nc.py

var maxDepth = function (root) {
    if (root === null) return 0

    const findDepth = (node) => {
        let right = 0;
        let left = 0;
        if (node.right) right = findDepth(node.right)
        if (node.left) left = findDepth(node.left)
        let height = Math.max(left, right);
        return height + 1
    }

    return findDepth(root)
};

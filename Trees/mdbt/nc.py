# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


from typing import Optional


class Solution:
    def maxDepth(self, root: Optional[TreeNode]) -> int:
        # if we are not supplied a root/empty root, we know that there can't be a depth
        # edge case
        if root is None:
            return 0

        # helper function
        def find_depth(node):
            # initialize left, right tracker for left, side branch depth
            left = 0
            right = 0

            # recursive call for traversal for both left, right branches
            if node.left:
                left = find_depth(node.left)
            if node.right:
                right = find_depth(node.right)

            # we want to find max between left, right traversal as we need to eval entire tree
            height = max(left, right)

            # O-index
            return height + 1

        return find_depth(root)

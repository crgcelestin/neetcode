# [Max Depth of Binary Tree](https://leetcode.com/problems/maximum-depth-of-binary-tree/)

## NeetCode Notes
- intuitively we need to return 1 + max(l,r) where min depth if root exists is 1 i.e 1+ max(dfs(left), dfs(right)) as we continue to perform recursion then exit to root

## tc, sc
worst case, time is O(n) as we recurse through tree of n nodes and space being tree height, worse case n as we have an unbalanced tree with n nodes on 1 side, 0 on other branch

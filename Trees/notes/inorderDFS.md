# INORDER
## Disclaimer
- DFS includes various traversal types including: inorder, postorder, preorder
    * prioritization being that of depth of neighbor branches as opposed to BFS being tree levels/rows

### Notes
- Inorder traversal: left-root-right pattern
    1. left subtree traversed
    2. Root node traversed
    3. right subtree traversed
```PY
'''
        1
    2       3
4       5       6

4 -> 2 -> 5 -> 1 -> 3 -> 6
'''
class Node:
    def __init__(self, v):
        self.data = v
        self.left = None
        self.right = None
def printInorder(node):
    if node is None:
        return
    printInorder(node.left)
    print(node.data, end="")
    printInorder(node.right)
```
### TC and SC
- TC: O(n), n being number of nodes as it traverses each node once
- SC: O(1), with no recursion stack, else O(n) being tree depth
    * worst case, h being same as n, best case being logN

# [Find Duplicate Number](https://leetcode.com/problems/find-the-duplicate-number/)

## NeetCode Notes
- Easy way to solve problem is to use hashmap that allows us to understand number of occurrences per number __BUT__ that would be O(N) space
- As a result of the two constraints: no mods, constant extra space
- solve problem in O(n) time, O(1) space
* Involves two core concepts:
    - Linked List Cycle, Floyd's algorithm

## Fundamentals
- Length of array is n+1 pos, numbers are in range of [1,n]
- can detect cycle, if we treat elements like indices, never going to be an exit condition as multiple elements point to one particular index multiple times
    * identify start of cycle allowing us to understand where duplicate occurs

### Floyd's algo
- we will have slow, fast pointer start at start with slow jumping 1 node per turn, fast jumping 2 per turn
    - first phase find mutual intersection
- then have a third pointer, slow that starts at origin and jumps by 1 step
    - second phase find where slow pointer and 2nd slow pointer match up
- __INTUITION__
    * p portion between start and cycle, cycle = x, x = node of slow and cycle start, c-x = cycle prior to first intersection point (node prior to cycle and start of cycle)
    * 2*slow = fast, 2*slow = p+(c-x)+c = p+2c-x -> 2(p+c-x) ... resolves to p=x
    * if long p, then p+nc-x which still resolves to same result

class Solution:
    def findDuplicate(nums: list[int]) -> int:
        slow, fast = 0, 0
        while True:
            slow = nums[slow]
            # use index of fast twice in order to advance 2*slow, when we consider indices = elements
            fast = nums[nums[fast]]
            if fast == slow:
                print("break")
                break
        slow2 = 0
        while True:
            slow = nums[slow]
            slow2 = nums[slow2]
            if slow == slow2:
                return slow


print(Solution.findDuplicate([1, 3, 4, 2, 2]))

# 03.31.24
## [143. Reorder List](https://leetcode.com/problems/reorder-list/)

### Problem
- Given head of singly linked LL, list can be represented as `L0 -> L1 -> ... -> L(n-1) -> L(n)` reorder list to be in form of `L0 -> L(n) -> L1 -> L(n-1) -> L(2)-> L(n-2) -> ...`
* May not mod values in list's nodes, only nodes themselves can be changed


### Analysis
- Input: list[Node(int)]
- Output: list[Node(int)]

- Algo Process
    - Restate Problem: perform swapping where we have the original nodes in `0,1,2, ..., n-2, n-1, n` to now be `0, n, 1, n-1, n-2, ...`

    - Goal of Function: performing inner swaps where elements occur in order of (assuming first to be 0th index) first, last, second, second last, etc.

    - Types: Node(int), list[]

    - Assertions and Assumptions: no modification of values, only can change order of nodes themselves, nothing returned (perform mods in place)

    - Edge Cases: empty node,  node with 1 val, node with two vals (already in proper order for both last cases)

#### Example \#1
```
input1 = [1,2,3,4]
output1 = [1,4,2,3]
```

#### Example \#2
```
input2 = [1,2,3,4,5]
output2 = [1,5,2,4,3]
```

#### Constraints
- \# number of nodes in list are in range of 1 to 5*10^4, `1<=Node.val<=1000`

#### _Backside_
- My solution would involve: perform swapping operation, where left + right pointer are [0, ll] (ll being len(ll)) then we want to make sure that Node @ 0 and Node @ LL_length occur next to each other
    * 1,2,3,4,5 -> 1,5,2,3,4,

then [3, ll], ll is of size 5 (last element at 4), if l+1 = len(ll): swap, break
    * 1,5,2,3,4 -> 1,5,2,4,3


#### Additional Details
- Reflecting on Attempt: Understood that nodes were reversing based on 0<->last, 1<->n-1, ... but was unsure as to what the proper mech was


### [Neetcode Solution](nc.py)
#### TC, SC
- O(n), O(1)


- #### NC Notes
* Focus on solution that can be done in linear time, constant space
    - Take beginning of list and merge with alternating values 1/2 of rest of values
    - First portion is not reverse, second portion is reversed and then merge in order to get desired product
* in order to know second half, slow and fast pointer w/ slow at first, fast at second and the fast is going to be shifted by 1 and fast will be shifted by 2
    - even list, slow.next will be second half then odd list, same
* req to have temp variable to store Nodes that are being pointed to prior to breaking links
    - after last node in first list, we want it to point to null

* merging two lists
- second set to null, prev is going to be set to last node which is start of second half of list due to reversal process

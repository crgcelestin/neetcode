class ListNode {
    constructor(val, next) {
        this.val = val
        this.next = next
    }
}

/**
 * @param {ListNode} head
 * @return {void}
 */
var reorderList = function (head) {
    // find first, second halfs
    let [fast, slow] = [head, head.next]
    while (fast !== null) {
        fast = fast.next.next
        slow = slow.next
    }
    // now we need to store checkpoints
    let second_half = slow.next
    slow.next = null

    // reverse second half
    let prev = null;
    let next;
    while (second_half !== null) {
        next = second_half
        second_half.next = prev
        prev = second_half
        second_half = next
    }
    // merge two lists
    let [first, second] = [head, prev]
    let tmp1, tmp2;
    while (second !== null) {
        [tmp1, tmp2] = [first.next, second.next]
        first.next = second
        second.next = tmp1
        first, second = tmp1, tmp2
    }

}

/*
while (current) {
    next = current.next
    current.next = prev
    prev = current
    current = next
}
*/

class Solution:
    def reorderList(self, head: Optional[ListNode]) -> None:
        """
        Do not return anything, modify head in-place instead.
        """
        # create two halves
        slow, fast = head, head.next
        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next

        # reverse second half
        second = slow.next
        prev = slow.next = None
        while second:
            next = second.next
            second.next = prev
            prev = second
            second = next
        """
        second after this reversal is set to null, prev is going to be the first node in the reversed list
        """
        # merge both halves
        first, second = head, prev  # get to pointer where one of pointers is not null
        while second:
            # store next nodes, as we will breaking links
            tmp1, tmp2 = first.next, second.next
            first.next = second
            second.next = tmp1  # insert nodes between first and first.next
            first, second = tmp1, tmp2

class ListNode {
    constructor(val, next) {
        this.val = (val === undefined ? 0 : val)
        this.next = (next === undefined ? null : val)
    }
}

/**
 * @param {ListNode} head
 * @returns {ListNode}
 */
var iterative_reverseList = function (head) {
    let [prev, curr] = [null, head]
    let next;
    while (curr) {
        next = curr.next
        curr.next = prev
        prev = curr
        curr = next
    }
    return prev
}

/**
 * @param {ListNode} head
 * @returns {ListNode}
 */
var recursive_revList = function (head) {
    if (!head) {
        return null
    }
    let newHead = head
    if (head.next) {
        // steps in recursive call increase linearly with size of array
        newHead = self.recursive_revList(head.next)
        head.next.next = head
    }
    head.next = null
    return newHead
}

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


from typing import Optional


class Solution:
    def iterative_reverseList(self, head: Optional[ListNode]) -> Optional[ListNode]:
        prev = None
        size = 0
        curr = head
        while curr:
            curr = curr.next
            size += 1
        for i in range(size):
            next = head.next
            head.next = prev
            prev = head
            head = next
        return prev
        """
        prev, curr = None, head
        while curr:
            next = curr.next
            curr.next = prev
            prev = curr
            curr = next
        """

    def recursive_reverseList(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if not head:
            return None
        newHead = head
        if head.next:
            newHead = self.recursive_reverseList(head.next)
            head.next.next = head
        head.next = None
        return newHead

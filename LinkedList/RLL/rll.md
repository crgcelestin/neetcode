# 1.14.24

## [Reverse Linked List](https://leetcode.com/problems/reverse-linked-list/)

### Problem
- Given `head` of singly linked list, reverse list -> return reversed list

### Analysis
- Input: given the head as optional[listNode] which contains a val and .next parameter
- Output: asked to output as optional[listNode] which is input linked list reversed

- Algo Process
    - Restate Problem
        * given linked list, reverse the order of the nodes
    - Goal of Function
        * return nodes of singly LL reversed
    - Types
        * optional[listNode]
    - Assertions and Assumptions
        * entire linkedlist is in a single head array
    - Edge Cases
        * None

## Examples

- ![Alt text](image.png)
#### Example \#1
```
input1 = [1,2,3,4,5]
output1 = [5,4,3,2,1]
```

- ![Alt text](image-1.png)
#### Example \#2 & \#3
```
input2 = [1,2]
output2 = [2,1]

input3 = []
output3 = []
```

#### Constraints
- number of nodes in list is range [0, 5000]
- `-5000 <= Node.val <= 5000`

#### _Backside_
- My solution would involve: we need to start with initializing a var that contains Null and a var`curr` that stores what head is
    * while we have `curr` then we need to store the node that is pointed to next via `next = curr.next` , then we continue to reverse pointers and node order
    * (alt method that is unnecessary) or you can index a size that tracks the length of the array that contains nodes and then iterate over nodes as we are in range(size) and reverse pointers, nodes


#### Additional Details
- Reflecting on Attempt:
    * base of reversing linkedList:
    ```
    while curr
        next = curr.next
        curr.next = prev
        prev = curr
        curr = next
    ```

### Preliminary Solution
- __Time Complexity__: O(n), iterating over array length

- __Space Complexity__: O(1), just using pointers

#### Solution Code
- [JS Solution](rll.js)

### [Neetcode](https://www.youtube.com/watch?v=G0_I-ZF0S38)
- [Iterative - Neetcode](iterative.py)
- [Recursive - Neetcode](recursive.py)
#### TC, SC
- for iterative, solution is tc:O(n) and sc:O(1), for recursive, solution is O(n) for tc and sc

- #### NC Notes
* on recursive solution, instead of doing entire linked list we will do reversal in sections, base case is null
* involves popping out of recursive call as we perform reversals in increments

var removeNthFromEnd = function (head, n) {
    let [slow, fast] = [head, head]
    // if we get n = 0 (we know we are removing from end of list)

    for (let i = 0; i < n; i++) {
        fast = fast.next
    }

    // missed test case
    // check head
    if (head.next === null) {
        return null
    }

    // missed: has to be check fast, if fast is null then return head.next
    // handle [1,2] case
    if (fast === null) {
        return head.next
    }


    while (fast.next !== null) {
        // iterate normally
        fast = fast.next
        slow = slow.next
    }
    slow.next = slow.next.next
    return head
}

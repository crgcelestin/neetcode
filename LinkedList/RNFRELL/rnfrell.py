# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


from typing import Optional


class Solution:
    def removeNthFromEnd(self, head: Optional[ListNode], n: int) -> Optional[ListNode]:
        # want to use slow, fast pointer approach that are both set to head
        slow = fast = head
        """
        we want the fast pointer to be our desired
        n node position, so using range move pointer so that
        when we use slow pointer, we know its in a correct set position
        """
        for i in range(n):
            fast = fast.next

        # only 1 node
        if head.next is None:
            return None
        """
        if n = length of list, i.e we want to remove 1st node
        then just return list after first node
        """
        if fast is None:
            return head.next
        """
        list has more than 1 node
        and we are not removing the 1st element of the LL

        set slow pointer to not traverse
        we know that the slow pointer will get to
        position of n, so now we just want to tell it
        to point to the node following ie we skip desired
        node to remove
        """
        while fast.next is not None:
            fast = fast.next
            slow = slow.next
        slow.next = slow.next.next
        return head

# 3.31.24

## [Copy List - Random Pointer](https://leetcode.com/problems/copy-list-with-random-pointer/description/)
- LL = linkedList
### Problem
- LL with length `n` is given such that each node contains additional random pointer that can: (1) point to any node in list or (2) `null`
- Construct __deep copy__ of list that contains `n` brand new nodes where each new node has value set to value of corresponding original node
- `next` and `random` pointer of new nodes point to new nodes in copied list so that pointers in org list and copied list have same list state
    * __none of pointers in new list should point to nodes in org list__
    * if there are two nodes X and Y in org list, `X.random -> Y` then for two corresponding nodes x and y in copied list, `x.random -> y`
- return head of copied ll
- LL is represented in input/output as list of n nodes, each represented as pair of [val, random_index]
    * `val` = integer, `Node.val`
    * `random_index` = index of node that `random` pointer points to or `null` ir it does not point to a node
- Code: Only given `head` or original LL



### Analysis
- Input:
- Output:

- Algo Process
    - Restate Problem

    - Goal of Function

    - Types

    - Assertions and Assumptions

    - Edge Cases

#### Example \#1
```
Input: head = [[7,null],[13,0],[11,4],[10,2],[1,0]]
Output: [[7,null],[13,0],[11,4],[10,2],[1,0]]
```

#### Example \#2
```
Input: head = [[1,1],[2,1]]
Output: [[1,1],[2,1]]
```
#### Example \#3 & \#4
```
Input: head = [[3,null],[3,0],[3,null]]
Output: [[3,null],[3,0],[3,null]]
```

#### Constraints


### Preliminary Solution
- __Time Complexity__:

- __Space Complexity__:

#### _Backside_
- My initial solution would involve:
- Second attempt:


#### Additional Details
- Reflecting on First Attempt
- Reflecting on Second Attempt

#### Solution Code
- [JS Solution]()
- [Python Solution]()

### [Neetcode]()
- [Neetcode Solution]()
#### TC, SC


- #### NC Notes

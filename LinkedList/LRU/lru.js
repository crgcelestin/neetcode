/*

design DS that follows LRU cache constraint (last recently used)
(1) LRU cache is initialized with positive size capacity
(2) get(key:int): return val if keys exists, else return -1
(3) put(key:int, value:int): update value of key if it exists, else add key value pair to cache
    if # of keys makes cache exceed capacity, evict recently used key
Must run in O(1) avg capacity

 */
// require a node class for building linked list in order to track access
class Node {
    constructor(key, val) {
        this.key = key
        this.val = val
        this.prev = null
        this.next = null
    }
}
// creating lru cache that will have default capacity, a store for all nodes, cache for updating
class LRUCache {
    constructor(capacity) {
        this.capacity = capacity
        this.store = []
        this.cache = {}

        this.left = Node(0, 0)
        this.right = Node(0, 0)
        this.left.next = this.right
        this.right.prev = this.left
    }
    remove(node) {
        let prev = node.prev
        let next = node.next
        prev.next = next
        next.prev = prev
    }
    insert(node) {
        let prev = this.right.prev
        let next = this.right
        prev.next = node
        next.prev = node
        node.next = next
        node.prev = prev
    }
    get(key) {
        if (this.cache[key] !== undefined) {
            this.remove(this.cache[key])
            this.insert(this.cache[key])
            return this.cache[key].val
        }
        return -1
    }
    put(key, value) {
        if (this.cache[key] !== undefined) {
            this.remove(this.cache[key])
        }
        this.cache[key] = new Node(key, value)
        this.insert(this.cache[key])
        let lru;
        if (Object.keys(this.cache).length > this.capacity) {
            lru = this.left.next
            this.remove(lru)
            delete this.cache[lru.key]
        }
    }

}

"""
design DS that follows LRU cache constraint
(1) LRU cache is initialized with positive size capacity
(2) get(key:int): return val if keys exists, else return -1
(3) put(key:int, value:int): update value of key if it exists, else add key value pair to cache
    if # of keys makes cache exceed capacity, evict recently used key
Must run in O(1) avg capacity
"""

x


class LRUCache:
    def __init__(self, capacity: int):
        self.cap = capacity
        self.store = []
        self.cache = {}

    def get(self, key: int) -> int:
        LRU = self.cache
        if key in LRU:
            return LRU[key]
        return -1

    def put(self, key: int, value: int) -> None:
        LRU = self.cache
        store = self.store
        if key in LRU:
            LRU[key] = value
            store.append(key)
        else:
            if len(store) == self.capacity:
                last_key = store.pop()
                del LRU[last_key]
                store.append(key)
                LRU[key] = value


# Your LRUCache object will be instantiated and called as such:
# obj = LRUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)


"""
ACTUAL - need to study solution
"""


class Node:
    def __init__(self, key, val):
        self.key, self.val = key, val
        self.prev = self.next = None


class LRUCache:
    def __init__(self, capacity: int):
        self.cap = capacity
        self.cache = {}  # map key to node

        self.left, self.right = Node(0, 0), Node(0, 0)
        self.left.next, self.right.prev = self.right, self.left

    # remove node from list
    def remove(self, node):
        prev, nxt = node.prev, node.next
        prev.next, nxt.prev = nxt, prev

    # insert node at right
    def insert(self, node):
        prev, nxt = self.right.prev, self.right
        prev.next = nxt.prev = node
        node.next, node.prev = nxt, prev

    def get(self, key: int) -> int:
        if key in self.cache:
            self.remove(self.cache[key])
            self.insert(self.cache[key])
            return self.cache[key].val
        return -1

    def put(self, key: int, value: int) -> None:
        if key in self.cache:
            self.remove(self.cache[key])
        self.cache[key] = Node(key, value)
        self.insert(self.cache[key])

        # EVICTION POLICY
        if len(self.cache) > self.cap:
            # remove from the list and delete the LRU from hashmap
            lru = self.left.next
            self.remove(lru)
            del self.cache[lru.key]


# Your LRUCache object will be instantiated and called as such:
# obj = LRUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)

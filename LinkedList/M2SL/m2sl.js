/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
class ListNode {
    constructor(val, next) {
        this.val = (val === undefined ? 0 : val)
        this.next = (next === undefined ? null : next)
    }
}
/**
 * @param {ListNode} list1
 * @param {ListNode} list2
 * @return {ListNode}
 */
var mergeTwoLists = function (list1, list2) {
    let list3 = new ListNode()
    let current = list3
    // test for edge case of either list being empty
    if (!list1) {
        return list2
    } else if (!list2) {
        return list1
    }
    // perform comparisons in order to merge lists in sorted order
    while (list1 !== null && list2 !== null) {
        console.log(list1)
        if (list1.val < list2.val) {
            current.next = new ListNode(list1.val)
            list1 = list1.next
        } else if (list1.val > list2.val) {
            current.next = new ListNode(list2.val)
            list2 = list2.next
        } else {
            current.next = new ListNode(list1.val)
            current = current.next
            current.next = new ListNode(list2.val)
            list1 = list1.next
            list2 = list2.next
        }
        current = current.next
    }
    // add other nodes if necessary, if we run out of either list for comparisons
    if (list1) {
        current.next = list1
    }
    if (list2) {
        current.next = list2
    }
    return list3.next
}

console.log(mergeTwoLists(
    [new ListNode(2), new ListNode(3)],
    [new ListNode(1), new ListNode(4)],
))

# 01.31.24

## [21. Merge Two Sorted Lists](https://leetcode.com/problems/merge-two-sorted-lists/description/)

### Problem
- Given heads of 2 sorted linked lists (list1, list2) need to merge two lists into one sorted list, made by splicing together nodes of first two lists -> return head of merged linked list

### Analysis
- Input: two Lls of type list (2 list:Optional[ListNode])
- Output: one Ll of type list (1 list:Optional[ListNode])

- Algo Process
    - Restate Problem
        * we want to merge two sorted lists into one sorted list
    - Goal of Function
        * output a sorted list as a byproduct
    - Types
        * list, ListNode
    - Assertions and Assumptions
        * each list is contiguous and there are no non-integers as values for Nodes
    - Edge Cases
        * need to check if one list runs out prior to others, check if one or other list is empty, if we reach nodes on each list at same time that are equal how do we handle the merging at that point

#### Example \#1
```
input:
    list1 : [1,2,4]
    list2: [1,3,4]
output:
    [1,1,2,3,4,4]
```

#### Example \#2
```
input:
    list1: []
    list2: []
output:
    []
```
#### Example \#3
```
input:
    list1: []
    list2: [0]
output:
    [0]
```

#### Constraints
- `# of nodes in both lists is in range [0,50]`
- `-100 <= Node.val <= 100`
- `list1 and list2 are sorted in non-decreasing order`

#### _Backside_
- My solution would involve:
    0. create starting node
    1. need to check if either list is empty, if so it makes our job easier for merging
    2. then we need to iterate through list and perform comparisons, in order to determine an accurate merger given two lists
    3. after comparisons, we need to figure out if there are any nodes left to be included in merging process

#### Additional Details
- Reflecting on Attempt:
    * was able to see problem prior, need to review solution
    * had to add new keyword for implementation

### Preliminary Solution
- __Time Complexity__: O(m+n), worst case we need to perform comparisons on two equivalent length lists

- __Space Complexity__: O(m+n), in a new list we are storing elements worst case being the sum of both lists

#### Solution Code
- [JS Solution](m2sl.js)
- [Python Solution](nc.py)

### [Neetcode](https://www.youtube.com/watch?v=XIdigk956u0)
- [Neetcode Solution](nc.py)
#### TC, SC
O(n), O(1)


- #### NC Notes
- Can't create new nodes (well at least in the version he's solving)
```
Merge two sorted linked lists and return as new sorted list. New list is made by splicing together nodes of first 2 lists
```
- starting at beginning of linkedList we have an output and a dummy list in order to protect against edge cases
- if we run out of nodes
```
l1: 1->2->4
l2: 1->3->4->5->6
output -> dummy -> 1 -> 1 -> 2 -> 3 -> 4 -> 4 -> 5 -> 6
```

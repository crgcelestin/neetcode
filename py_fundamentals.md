Inheritance: Inheritance is a fundamental concept in object-oriented programming (OOP) that __allows a class (called the child or subclass) to inherit properties and methods from another class (called the parent or superclass). This enables code reuse and the creation of a hierarchy of related classes.__ In Python, you can create a new class that inherits from an existing class using the class ChildClass(ParentClass): syntax.

Encapsulation: Encapsulation refers to the __concept of bundling data (attributes) and methods (functions) that operate on the data into a single unit (a class)__. This unit hides the internal implementation details of the class from the outside, providing a clean interface for interacting with the class. This promotes information hiding and helps manage complexity.

Polymorphism: Polymorphism allows objects of different classes to be treated as objects of a common superclass. This enables you to write code that can work with objects of different types in a consistent manner. __For example, you might have different classes with their own implementations of a common method, and polymorphism allows you to call the method on objects of any of these classes without knowing their specific types.__

- Example of Polymorphism

```py
    class LinkedList:
    def push(self):
        # Code to push an element into a linked list
        pass
    ``
    class DoublyLinkedList:
        def push(self):
            # Code to push an element into a doubly linked list
            pass

    first = LinkedList()
    first.push()  # Calling push() on a LinkedList object

    second = DoublyLinkedList()
    second.push()  # Calling push() on a DoublyLinkedList object
```

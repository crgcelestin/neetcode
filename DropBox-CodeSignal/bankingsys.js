function solution(queries) {
    const system = {}
    const results = []
    const transactions = {}
    function create_acct(accountId) {
        if (!Object.keys(system).includes(accountId)) {
            system[accountId] = 0
            return 'true'
        } else {
            return 'false'
        }
    }
    function deposit(accountId, amt) {
        if (!Object.keys(system).includes(accountId)) {
            return ""
        } else {
            system[accountId] += Number(amt)
            return `${system[accountId]}`
        }
    }
    function transfer(source, target, amt) {
        if (!Object.keys(system).includes(source) || !Object.keys(system).includes(target)) {
            return ""
        } else if (String(source) == String(target)) {
            return ""
        } else if (system[source] < amt) {
            return ""
        } else {
            integer_amt = Number(amt)
            system[source] = system[source] - integer_amt
            system[target] = system[target] + integer_amt
            if (!Object.keys(transactions).includes(source)) {
                transactions[source] = [...integer_amt]
            } else {
                transactions[source].push(integer_amt)
            }
            return `${system[source]}`
        }
    }
    function top_spenders(num) {
        if (!Object.keys(transactions) && Object.keys(system).length >= num) {
            // using .sort() in order to organize spenders by descending order of payments made via bank
            const sortedAccounts = Object.keys(transactions).sort((a, b) => {
                const totalA = transactions[a].reduce(
                    (acc, amt) => acc + amt, 0
                )
                const totalB = transactions[b].reduce(
                    (acc, amt) => acc + amt, 0
                )
                if (totalA === totalB) {
                    // if there is a tie regarding the sorting of transfer amounts based on user, default to alphabetical sorting of users
                    return a.localeCompare(b)
                }
                // sort in descending order, opp being ascending
                // + meaning, B is greater than A (true-ish) vs -
                return totalB - totalA
            });
            // we create a shallow copy bounded by 0 and required number of top spenders to return
            const topN = sortedAccounts.slice(0, num).map((accountId) => {
                // returning single value that is accumulation of all transfers performed by a bank customer/client
                const totalAmt = transactions[accountId].reduce(
                    (acc, amt) => acc + amt, 0
                )
                // returning account with their corresponding spender amount
                return `${accountId}(${totalAmt})`
            })
            // join based on , delimiter
            return topN.join(', ');
        }
    }
    for (const query of queries) {
        if (query[0] === 'CREATE_ACCOUNT') {
            results.push(create_acct(query[2]))
        } else if (query[0] === 'DEPOSIT') {
            results.push(deposit(query[2], query[3]))
        } else if (query[0] === 'TRANSFER') {
            results.push(transfer(query[2], query[3], query[4]))
        } else if (query[0] === 'TOP_SPENDERS') {
            results.push(top_spenders(query[2]))
        }
    }
    return results
}

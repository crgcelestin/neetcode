# GENERAL INSTRUCTIONS - DROPBOX ROLE
## [Code](bankingsys.js)
* Level 1: The banking system should support creating new accounts, depositing money into accounts, and transferring money between two accounts

* Level 2: The banking system should support ranking accounts based on outgoing transactions.

* Level 3: The banking system should allow scheduling payments with cashback and checking the status of scheduled payments.

* Level 4: The banking system should support merging two accounts while retaining both accounts balance and transaction histories.


## L2 - Instructions
* Level 2
- The bank wants to identify people who are not keeping money in their accounts, so implement operations to support ranking accounts based on outgoing transactions.
  * TOP_SPENDERS `<timestamp> <n>` — should return the identifiers of the top n accounts with the highest outgoing transactions - the total amount of money either transferred out of or paid/withdrawn (the PAY operation will be introduced in level 3) - sorted in descending order, or in case of a tie, sorted alphabetically by accountId in ascending order. The result should be a string in the following format: `<accountId1>(<totalOutgoing1>), <accountId2>(<totalOutgoing2>), ..., <accountIdN>(<totalOutgoingN>)`.
  * If less than n accounts exist in the system, then return all their identifiers (in the described format).
	* Cashback (an operation that will be introduced in level 3) should not be reflected in the calculations for total outgoing transactions.

### Input

```js
queries = [
  ["CREATE_ACCOUNT", "1", "account3"],
  ["CREATE_ACCOUNT", "2", "account2"],
  ["CREATE_ACCOUNT", "3", "account1"],
  ["DEPOSIT", "4", "account1", "2000"],
  ["DEPOSIT", "5", "account2", "3000"],
  ["DEPOSIT", "6", "account3", "4000"],
  ["TOP_SPENDERS", "7", "3"],
  ["TRANSFER", "8", "account3", "account2", "500"],
  ["TRANSFER", "9", "account3", "account1", "1000"],
  ["TRANSFER", "10", "account1", "account2", "2500"],
  ["TOP_SPENDERS", "11", "3"]
]
```
### Output

```js
["true", "true", "true", "2000", "3000", "4000", "account1(0), account2(0), account3(0)", "3500", "2500", "500", "account1(2500), account3(1500), account2(0)"].
```

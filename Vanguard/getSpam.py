import math
import os
import random
import re
import sys


#
# Complete the 'getSpamEmails' function below.
#
# The function is expected to return a STRING_ARRAY.
# The function accepts following parameters:
#  1. STRING_ARRAY subjects
#  2. STRING_ARRAY spam_words
#

# we want to iterate through subjects and iterate through spam_word
    # while iterating through subject, we need to check if current word is in spam_words
'''
["I paid him paid"]
    i i2
    x - spam, x2 - spam (
        if 2 spam then email is spam, append spam to return array
    )
[ "Summertime Sadness" ]
    i ---------i
                x - spam
                2 spam is not returnned, append not_spam to return array
["I", "Sadness", "paid"]
'''

class Solution:
    def getSpamEmails(self, subjects: list[str], spam_words:list[str]) -> list[str]:
        spam_result=[]
        def is_spam_word(subject):
            count=2
            lower_spam = [word.lower() for word in spam_words]
            for word in subject:
                if word.lower() in lower_spam:
                    count+=1
            if count>=2:
                return  True
            else:
                return False
        for subject in subjects:
            splitwords=subject.split(" ")
            if is_spam_word(splitwords):
                spam_result.append('spam')
            else:
                spam_result.append('not_spam')
        return spam_result

Test=Solution()
print(Test.getSpamEmails(
    [
    "I paid him paid", "Summertime Sadness"
    ],
    [
        "I", "Sadness", "paid"
    ]
))

'''
class Solution:
    def getSpamEmails(self, subjects: list[str], spam_words: list[str]) -> list[str]:
        spam_result = []

        spam_set = set([word.lower() for word in spam_words])

        for subject in subjects:
            split_words = subject.lower().split(" ")
            if len(spam_set.intersection(split_words)) >= 2:
                spam_result.append('spam')
            else:
                spam_result.append('not_spam')
        return spam_result
'''

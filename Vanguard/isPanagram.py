# return a string of integers
# if string is a panagram append 1, if string is not a panagram append 0
# so in the example of a list that has 2 strings, panagram at 0 index, not panagram at 1 index -> return 10

class Solution:
    def isPanagram(self, string:list[str]) -> str:
        res=""
        def helper(string):
            for i in range(97,122,1):
                if chr(i) not in string:
                    return False
            return True
        for word in string:
            if helper([*word.lower()]):
                res+=str(1)
            else:
                res+=str(0)
        return res

Test=Solution()
print(Test.isPanagram(
    [
        'abcdefghijklmnopqrstuvwxyz',
        'word'
    ]
))

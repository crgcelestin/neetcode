'''
use 2 pointers:
t_index (keep track of current word in t that we are checking)
word (s iterable)
if current word s matches word in t at a given index -> increment t_index, move to next word in t else add word to result list

ensure maintained words order and correct number of words is added to result list
'''

class Solution:
    def missingWords(self, s:str, t:str)-> str:
        s_words = s.split(" ")
        t_words = t.split(" ")

        result = []
        t_index = 0

        for word in s_words:
            if t_index < len(t_words) and word == t_words[t_index]:
                t_index += 1
            else:
                result.append(word)
        return " ".join(result)


Test=Solution()
test1 = Test.missingWords(
        "Python is an easy to learn powerful programming language It has efficient high-level data structures and a simple but effective approach to objectoriented programming Python elegant syntax and dynamic typing",
        "Python is an easy to learn powerful programming language"
)
print(test1)
print(test1==(
    "It has efficient high-level data structures and a simple but effective approach to objectoriented programming Python elegant syntax and dynamic typing"
))

/**
 * @param {number[]} heights
 * @return {number}
 */
var largestRectangleArea = function (heights) {
    let combos = []
    let l = 0;
    let r = 1;
    while (l < r && l < heights.length - 1 && r < heights.length) {
        combos.push(Math.min(heights[l], heights[r]) * 2)
        l += 1
        r += 1
    }
    return Math.max(...combos)
};

/**
 * @param {number[]} heights
 * @return {number}å
 */
var largestRectangleArea2 = function (heights) {
    let stack = []
    let max_area = 0;

    for (const [i, h] of heights.entries()) {
        let start = i;
        while (stack.length > 0 && h < stack[stack.length - 1][1]) {
            let [index, height] = stack.pop()
            // check if height popped could have been max area rectangle
            max_area = Math.max(max_area, height * (i - index))
            // can extend our start index to one that was popped backwards as we found next greatest
            start = index
        }
        // add start index we pushed all the way back
        stack.push([start, h])
    }

    // still might me be more entires left i.e rectangle that have their indices pushed back to a prior start
    for (const [i, h] of stack) {
        max_area = Math.max(max_area, h * (heights.length - i))
    }

    return max_area
};

let input1 = [2, 1, 5, 6, 2, 3]
console.log(
    largestRectangleArea2(
        input1
    )
)
let input2 = [2, 4]
console.log(
    largestRectangleArea2(
        input2
    )
)

class Solution:
    def largestRectangleArea(self, heights: list[int]) -> int:
        stack = []
        maxArea = 0
        for i, h in enumerate(heights):
            start = i
            while stack and h < stack[-1][1]:
                index, height = stack.pop()
                maxArea = max(maxArea, height * (i - index))
                start = index
            stack.append([start, h])
        for i, h in stack:
            maxArea = max(maxArea, h * (len(heights) - i))
        return maxArea


Tests = Solution()
input1 = [2, 1, 5, 6, 2, 3]
input2 = [2, 4]
print(Tests.largestRectangle(input1))
print(Tests.largestRectangle(input2))

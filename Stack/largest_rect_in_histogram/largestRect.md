# 12.27.23

## [Largest Rectangle](https://leetcode.com/problems/largest-rectangle-in-histogram/description/)

### Problem
* given an array of ints `heights` representing histogram's bar height with default width of `1`, return area of largest rect in histogram

### Analysis
- Input: given a heights array, [int]
- Output: return int

- Algo Process
    - Restate Problem
        * given an array of integers that are representing rects with width of 1, find largest rect given that you can combine any x number of them to gain a*b dimensions

    - Goal of Function
        * return area of largest known possible rectangle combined or otherwise
    - Types
        * [int], int
    - Assumptions and Constraints Given
        * we are only given an array with integers
        * constraints:
            - ` 1<= heights.length <=10^5`
            - `0<= heights[i] <=10^4`
    - Edge Cases
        * given an empty array, given strs in array
        * given an array that has multiple integers that provide multiple same largest area

#### Example \#1
![Alt text](image.png)
```
heights = [2,1,5,6,2,3]
output = 10
```
- explanation: Largest rectangle is in red area being 10 units (2*5) [ resultant of combo of 1*5, 1*6 rectangles ] -> larger than 1*6, 2*4, 3*1, 2*1

#### Example \#2
```
Input: heights = [2,4]
Output: 4
```
- explanation: largest rect is a combo of 1*2, 1*4 resulting in options of 2*2 rect (square can be rect) or 1*4 rectangle being the largest option

### Preliminary Solution
- __Time Complexity__: O(N), comparisons being made dependent on length of array and values, worst case N is several integers long with successively larger values meaning we have to iterate over entire array to gain largest possible rectangle

- __Space Complexity__: Creating array of areas and then return an integer, O(n)

#### _Backside_
- My initial solution would involve:
    * iterate through rectangles and store only current max `max_area_rect` and update when found most current during iteration
        - use l,r pointers to track indices that allow for finding max rect
        - looking at first example, we gained 10 with l=5, r=6 so max_rect = 2*5
        - looking at 2nd example, largest rects possible are 2*2, 4*1
    * or somehow pre-calc all largest rectangle combinations then perform max(rects)


#### Additional Details
- Reflecting on Attempt:
    * [Initial Attempt](LR.js)
    - Reflecting on First Attempt:
        1. tried a rudimentary approach by using minimums per pointer and then returning max of calculated max rectangle area
        2. passed first two tests but failed next several on lc after submissions


#### Solution Code
[JS Solution](LR.js)
[Python Solution]()

### [Neetcode](https://www.youtube.com/watch?v=zx5Sw9130L0)
- [Neetcode Solution](%20nc.py)
#### TC, SC
* TC: O(n), iterate through histogram once with push and pop occurring once
* SC: O(n), extra memory decided by length of inputted heights array

- #### NC Notes
* current heights are increasing, if heights aren't in increasing order -> they will be popped i.e remove from consideration
    - `for i in heights` -> `if heights[i]>heights[i+1]: heights.pop(heights[i])`
* looking at areas from left to right, given rectangles
* pop only from most recent elements
* ```
    stack - record of index and height
    max area - storing most recent max height
    given an array with ints [2,1,5,6,2,3]

    array:
        [2,1,5,6,2,3]
        [0,1,2,3,4,5]
    stack:
        index  height
          0      2
          0      1 (can be extended)  (l)
          2      5 (also greater than 2, stops at 4 so area created is 10) (x)
          3      6 (can't go any further and stopped at 4, get area and then pop) (x)
          2      2 (EXTENDED TO INDEX 2) (l)
          5      3 (l)
    max area:
        2 (x)
        6 (x)
        10

    3 elements left in stack ( indicated by (l) )
        - calc area
            * (6-5) * 3 = 3
            * (6-2) * 2 = 8
            * (6-0) * 1 = 6

    max area is still 10
  ```

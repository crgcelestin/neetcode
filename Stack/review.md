# 01.05.23
- valid parentheses
    * the goal is given a string consisting of a combination of parentheses we need to return if its valid i.e same opening and closing parentheses
        1. create var that stores stack and a dictionary containing matching parentheses with opening as key and closing as value for all possible parentheses combos
            ```py
            stack=[]
            closeOpen = {
                "(":")"
                ...
            }
            ```
        2. iterate in provided input string `for c in s`
            1. if we find a character matches a key in our provided dictionary, i.e `c in closeOPen` which means we have encountered a closing bracket
                1. if there are elements in stack and we find a matching parenthesis in the stack for our given opening then we want to pop it from stack
                    `if stack and stack[-1]==closeOpen[c]: stack.pop()`
                2. else we want to `return False`, as it means string can't be valid i.e we don't have a matching opening bracket for a closing
            2. if we didn't find it in the closeOpen dict, based on problem constraints, it's an opening bracket `stack.append(c)`
        3. if our stack is empty and we have iterated through string, it is a valid input, any other scenario means we haven't `return True if not stack else False` (list comp)

- min stack
    * two possible methods to implement stack, one that saves on tc and one that saves on memory
        - tc saver
            1. for minstack, we create an __init__ that holds the actual stack of values and minStack that stores mins in order for proper operation of top() [ both are initialized as [] ]
            2. push
                - append to stack, self.stack.append(val) -> for minStack, we need to make sure we are adding in proper order sorted, `val  = min (val, minStack[-1] if self.minStack else val)` makes sure our top of minStack is always the minimum
                - we know we are appending val to minStack in correct order
            3. pop
                - pop from stack, minStack
            4. top
                - return from top of stack, `stack[-1]`
            5. getMin
                - return from top of minStack, `minStack[-1]`
        - memory saver
            1. create a traditional stack with a `class Node` and `class Stack`, class Node with `constructor(val)` that has this.value->val, this.next->null and Stack with constructor() that has this.first->null, this.last->null, this.size=0
            2. push
                - instantiate new Node with val -> if empty, set new Node as first, last, if not empty, new Node next is the prev first val, this.first is new Node, increment size
            3. pop
                - if !size, return -> store current first node as prev -> if only 1 node, this.first=null, first is last -> if more than 1 node, this.first becomes prev.next, prev becomes null, decrement size
            4. top
                - return this.first.val
            5. getMin
                - initialize min with this.first.val, if no nodes, return -> if only 1 node then the first node has to be min -> if not only 1 node, then iterate through nodes and update min if we find a lesser value, `current = current.next`, return min

- eval rev polish notation
    * we are given a list of either operation or number as strings and need to eval as if we are viewing it as a mathematical expression
        - initialize var to store stack [] -> iterate through tokens
            * `if len(stack)>=2`
                * if we get to `token=="+"`
                    - pop two elements, sum them, and add to stack `stack.append(stack.pop()+stack.pop())`
                * `token=="-"`
                    - assign vars to 2 stack.pops, stack.append(difference i.e `num1-num2`)
                * `token=="/`
                    - assign vars to 2 stack.pops, stack.append(int(num1)/int(num2))
                * `token=="*`
                    - `stack.append(stack.pop()*stack.pop())`
                * else we encountered a number, add to stack an integer of string token to be eval'd in expression
                    - stack.append(int(token))
            * ? else `continue`, we can't evaluate any expressions if we don't have at least two numbers ?
        - return `stack[0]`

- generate parentheses
    * we are tasked with generating an array containing all parentheses combos given a input integer
    - recursive solution
        1. initialize a stack for performing matching and res for returning combos, `stack, res -> []`
        2. defining helper fxn, `backtrack`that takes in two parameters being `openP, closeP` that will serve as counters
            - three conditionals: if both counters and input integer are equal i.e we have created a good combo `openN == closedN == n`, if openN is less than our supplied input int `openN < n` we need to add another open Paren in order to get to a good combo, if `closedN < openN` we need to add a closed paren to get to a good combo
                1. if `openN == closedN == n`, we append current combo to res with its elements joined i.e `res.append("".join(stack))`
                2. if `openN < n `, we can append parens to stack `stack.append("(")` -> invoke backtrack with our openP updated by 1 `backtrack(openN+1, closedN)` after its done with its stack calls, we will pop in order to reset stack for potential other good combos `stack.pop()`
                3. if `closedN < openN`, if we have counted less closed Parens vs open parens, we need to add a closed paren to get them equal `stack.append(")")` -> invoke with closedN updated via `backtrack(openN, closedN + 1)` -> after callstack we will pop from stack to create other good combos `stack.pop()`
        3. invoking helper function with 0,0 start that servers as tracker for open and close parentheses `backtrack(0,0)`
        4. return combos, `return res`

# NEXT
- daily temps
    * we are tasked with returning an array that given a list of temperatures communicates the number of `n` days one is supposed to wait if the start is the `nth` element i.e temps = [10,1,30,10], res = [2,1,0,0]
        - we have a function titled `dailytemperatures` taken in list of temps that return an output of list for day count
            1. we initialize two vars: (1) empty array `s = []`, (2) array of input length that has 0 default - for py, [0] * len(temp) or using Array(temperatures.length).fill(0)
            2. iterate through temps using enumerate or Object.entries() [index, temp]
                1. while stack has elements and our last temp is less than the most current one we have found with an enumerate function -> `stack and t>stack[-1][0]`
                2. pop from stack -> `stackI, stack T = pop()` and then calculate difference that will allow for an update form 0 in resultant array i.e res[stackI] = i - stackI
            3. if there is no stack and we find that our last temp is not smaller than our most current, we just append to stack,`stack.append([t,i])`

- car fleet
    * we are given a 2-array combo that communicate a car's position and speed as well as a target destination and we need to return the number of car fleets that will end up being formed prior to all car's reaching target destination
        1. we need to create an aggregate array `pairs` that contains speed and position as combos in array
            ``
            - python
            # use zip in order to create aggregate
            pairs = [[p,s] for p,s in zip(position, speed)]

            - js
            # iterate through and create new array
            # traditional c style loop
            for (let i ...)
                pairs.push([position[i], speed[i]])
            ```
        - __FIRST APPROACH__
        * (python)
            0. create an empty array for `stack = []`
            1. we want to iterate in reverse of the sorted pairs
            ```py
            for p, s in sorted(pairs)[::-1]
            ```
            2. append to the stack estimated time to reach a destination given a car's particular position and speed
            ```py
            stack.append((target-position)/speed)
            # p and s respectively
            ```
            3. finally a conditional that determines if fleets have formed
                - we only want to look for fleets if we have at least two cars in stack and we find that one of our car's in an earlier position take less time to reach the destination as compared to earlier car
                ```py
                if len(stack)>=2 and stack[-2]>=stack[-1]:
                    stack.pop()
                ```
            4. return stack length
        - __SECOND APPROACH__
        *(javascript)
            0. initialize a curtime and fleets both set to 0
            1. using let, initialize vars to be assigned vals in for loop `let pos; let val;`
            2. iterate through pairs in reverse
                1. during iteration, store the duration per car to get to destination
                2. we are going to use curtime to determine fleets, curtime is always going to be the last recent -> compare our remaining_time (current car) to curtime(last recent)
                    - if it is less then add 1 to fleets, set curtime to remaining_time
            3. return fleets - understand # of CFs

- longest rect
    * need to return the largest-max area rectangle possible given our respective heights at each array element
        0. function titled largestRectArea that takes in heights array = array of integers
        1. initialize var storing empty array, maxArea starting at 0
        2. iterate through indices and heights `.enumerate or .entries`
            1. start variable stores the last encountered index `start = i`
            2. `for i, h in enumerate(heights)`iterate through stack if present and __if our current height is less than that of the last encountered height__
                1. if we meet this condition, pop last index, height
                2. maxArea will be determined by max and the two inputs being maxArea, last rect area -> `height*(i=index)`
                3. start gets updated to last given index
                - basically we have encountered a rectangle height that can be combined with our previous rect
            3. else we append to stack a start index, height
        - as we are trying to gather possible maxAreas, we might have more entries left to earlier start, so perform another series of comparisons
        3. we need to then iterate through stack `for i, h in stack:`
            1. perform a search for maxArea using max in addition to maxArea and instead we want to look through entire possible rectangles to find what other max is possible -> `maxArea = max(maxArea, h*(len(heights)-i))` `len(heights)-i` being the gap between possible start indices and the entire heights length

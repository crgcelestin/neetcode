# DATE

## [Daily Temperatures](https://neetcode.io/roadmap#:~:text=Python-,Daily%20Temperatures,-Medium)

### Problem
- we are given an array of integers `temps` representing daily temperatures -> need to return array `answer` where `answer[i]` is # of days to wait after `i-th` day to gain warmer temperature
    * if no future day then return 0 where `answer[i]` is instead

### Analysis
- Input: array of integers representing recorded daily temperatures where [i] is the corresponding day i.e `input[0] = 1st day`
- Output: array of integers where `answer[i]` is the min days to wait until warmer temp

- Algo Process
    - Restate Problem
        * ingest array with temperatures per day where we are to return a corresponding array where each element indicates the days we are to wait (if exists) for a higher temp day
    - Goal of Function
        * we need to return an array that allows one to understand how many days needed for warmer temp
    - Types
        * input: list[int]
        * output: list[int]
    - Assertions and Assumptions
        * we are given an input that only contains integers
    - Edge Cases
        * empty array, array where temps are all the same, array containing decreasing temps

#### Example \#1
```
Input: temperatures = [73,74,75,71,69,72,76,73]
Output: [1,1,4,2,1,1,0,0]
```
#### Example \#2 & \#3
```
Input: temperatures = [30,40,50,60]
Output: [1,1,1,0]

Input: temperatures = [30,60,90]
Output: [1,1,0]
```

#### Constraints
- `1<= temperatures.length <= 10^5`
- `30 <= temperatures[i] <= 100`

### Preliminary Solution
- __Time Complexity__: O(n), we conduct as many steps as proportional to the length of temperatures array

- __Space Complexity__:

#### _Backside_
- My initial solution would involve:
    (1) iterate through the given dailyTemps array with two pointers, the first `i` being at the current number and the second `j` being a pointer following our current to determine if a larger number comes following
    (2) ask if a larger number does follow and if it does, then we will append the min number of positions to reach the larger


#### Additional Details
- Reflecting on First Attempt: I was able to create a solution, but it is far incomplete as it doesn't properly return all following positions

#### Solution Code
[JS Solution](DT.js)

### [Neetcode](https://www.youtube.com/watch?v=cTBiBSnjO3c)
- [Neetcode Solution](nc.py)
#### TC, SC

- #### NC Notes
* can implement a O(n^2) tc but can use extra memory for a more efficient solution
* can incorporate a stack model where we continue to pop and add positions required to get to larger number
* for smaller values following target,
    - add to stack that becomes monotonic decreasing stack meaning that as you progress downwards values are <
    - we will only pop from stack when we encounter larger numbers and default values for non-popped numbers is 0

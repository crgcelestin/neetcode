/**
 * @param {number[]} temperatures
 * @return {number[]}
*/
// first attempt - wrong
var dailyTemperatures = function (temperatures) {
    const pos = []
    let current = 0
    let iter = 1
    while (current < iter && current < temperatures.length - 1 && iter < temperatures.length) {
        if (temperatures[current] < temperatures[iter]) {
            pos.push(iter - current)
            current += 1
            iter += 1
        } else {
            iter += 1
            /*
                wanted to have first conditional be when we found larger temp and then this was to be when we don't find a larger temp, just go to next number
                returned an array that wasn't accurate :(
            */
        }
    }
    return pos
}

/// [1,0]

/**
 * @param {number[]} temperatures
 * @return {number[]}
 */
var dailyTemperatures2 = function (temperatures) {
    const res = Array(temperatures.length).fill(0)
    let stack = []
    // because we need to figure out distances via units of relative position, use js's enumerate function being .entries()
    // [index, temperature]
    for (const [i, t] of temperatures.entries()) {
        // while stack has a pair in it and current temp is greater than last pushed temperature of most recent pair
        while (stack.length > 0 && t > stack[stack.length - 1][0]) {
            // if conditional is true, we found a temp that has greater temp, so pop from stack and for res we will add the position distance to next largest temp in same index
            let [stackT, stackI] = stack.pop()
            res[stackI] = (i - stackI)
        }
        // if stack empty we push in pair we encounter
        stack.push([t, i])
    }
    return res
}

const temperatures1 = [73, 74, 75, 71, 69, 72, 76, 73]
const assert1 = [1, 1, 4, 2, 1, 1, 0, 0]
const temperatures2 = [30, 40, 50, 60]
const assert2 = [1, 1, 1, 0]
const temperatures3 = [30, 60, 90]
const assert3 = [1, 1, 0]

let result1 = dailyTemperatures2(temperatures1)
JSON.stringify(result1) == JSON.stringify(assert1) ? console.log('True') : console.log(`false:${result1}`)
let result2 = dailyTemperatures2(temperatures2)
JSON.stringify(result2) == JSON.stringify(assert2) ? console.log('True') : console.log(`false:${result2}`)
let result3 = dailyTemperatures2(temperatures3)
JSON.stringify(result3) == JSON.stringify(assert3) ? console.log('True') : console.log(`false:${result3}`)

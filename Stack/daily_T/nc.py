class Solution:
    def dailyTemperatures(self, temperatures: list[int]) -> list[int]:
        # initialize defaults
        res = [0] * len(temperatures)
        # temperature and index pair
        stack = []  # [temp, index]
        # grabbing index, value at current with enumerate
        for i, t in enumerate(temperatures):
            # while there are elements in stack and current number is greater than last number in stack, stack[-1][0] = last [temp] in stack
            while stack and t > stack[-1][0]:
                # pop pair as we have found a number larger than current (temperature, index)
                stackT, stackInd = stack.pop()
                # append to res the indices to traverse to get to larger #s from current index by calculating the difference
                res[stackInd] = i - stackInd
            # add to stack if we don't have any values accumulated and we haven't encountered a temperature that is greater
            stack.append([t, i])
        return res

# 10.30.23

## [22. Generate Parentheses](https://leetcode.com/problems/generate-parentheses/description/)

### Problem
- given `n` as an input for parentheses pairs, write a fxn that generates all combos of well formed parentheses

### Analysis
- Input: provided an integer that represents a given `n` pairs of parentheses
- Output: return all combos of well-formed parentheses

- Algo Process
    - Restate Problem
        * design function that when user enters an integer returns all parentheses combos
    - Goal of Function
        * return all possible parentheses combos that are well-formed able to be generated from a provided integer
        * well-formed meaning that we have a series of correct opening and closing parentheses, nested properly

    - Types:
        * integer, string array

    - Assertions and Assumptions
        * all parentheses combos must be well-formed, only integer input must be utilized in order to generate potential combos, and only parentheses to be used are of ')', '('

    - Edge Cases
        - negative integers, 0 provided, provided an integer greater than 8, provided input that is not of type integer

#### Example \#1
```
input - n: 3
output - ["((()))","(()())","(())()","()(())","()()()"]
```

#### Example \#2 &  \#3
```
input - n: 1
output - ["()"]

input - n:2
output - ["()()", "(())"]
```

#### Constraints
- `1 <= n <= 6`

### Preliminary Solution
* Actual TC: O(N^2)
    - actual tc is exponential as each recursive call -> 2 more recursive calls (2 branches in each recursive call for adding open+close paren)
        - __(Old - Incorrect) Time Complexity__: O(n), n being the supplied integer, i.e. recursion will occur a worst case min of n times

* Actual SC: O(N^2)
    - actual sc is exponential as max depth of recursion is proportional to # of paren pairs and at each level, there are 2 choices
        - (Old - Incorrect) __Space Complexity__: O(n), array increases in time with a direct increase in input integer

#### _Backside_
- My initial solution would involve:
    * Based on the prior examples of what n results i i.e n = 1 -> 1 combo and n = 3 -> 5 combos,  I believe that the general rule is 2*n-1, meaning that given a provided n, we return (maybe) 2n-1 combinations.
    * in terms of returning proper combos, we need to programmatically generate the desired combos based on a given integer input, unable to know what will be inputted
    * n = 3, represents (), (), () in varied orders
    * there certainly needs to be some randomness
        - i.e the first combo returned is perfect scenario where its just sequential, then ... at some point have it be a series (if possible) closing then opening brackets

#### Additional Details
- Reflecting on Attempt:

#### Solution Code
[JS Solution](alt_recursion.js)
[Python Solution](GP.py)

### [Neetcode](nc.py)
- [Neetcode Solution](https://www.youtube.com/watch?v=s9fokUqJ76A)
#### NC Notes
- TC, SC
- backtracking: brute force approach:
    * integer indicates that there are n open, n close parentheses
    *  determination as to whether a close or open paren can be added is determine by [ n open, n close ] constraint ( close < open )
    * opening paren has to come prior to a closing, i.e we can't have closing with some opening prior
        - can only add a closing paren, if the count of current close is less than count of current open i.e `open=2, close=1` then a closing paren can be added
    * understanding the pathing of problem's solution involves branching paths regarding parentheses scenarios

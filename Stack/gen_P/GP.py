class Solution:
    def generateParenthesis(self, n: int) -> list[str]:
        result = []

        def helper(m: str, openP: int):
            # first we want to add req # of open paren, update to indicate (a) for our first callback we now have a combo w/ 2 open Ps and (b) for next calls we know that we have created a combo that has a streak of (num)openP parentheses at the start
            if openP < n:
                helper(m + "(", openP + 1)
            # to add close paren, (length of current string has to be less than twice the count of open parentheses as we want to form complete pairs) str.length<2*openP, meaning that we have capped our openP limit
            # also really means that we have open Ps that we need to close in order to get a valid combo
            # asks if length of current string is less than twice of current open P count, as we know its greater than our input integer
            if 2 * openP > len(m):
                helper(m + ")", openP)
            # we reached a good combo as the length of it is twice our input, meaning we have a satisfactory number of open and close parens, push said combo into array
            if n * 2 == len(m):
                result.append(m)

        # base case
        helper("(", 1)
        return result


Test = Solution()
print(Test.generateParenthesis(2))

"""
alt

def printParenthesis(str, n):
    if(n > 0):
        _printParenthesis(str, 0,
                          n, 0, 0)
    return

def _printParenthesis(str, pos, n,
                      open, close):

    if(close == n):
        for i in str:
            print(i, end="")
        print()
        return
    else:
        if(open > close):
            str[pos] = '}'
            _printParenthesis(str, pos + 1, n,
                              open, close + 1)
        if(open < n):
            str[pos] = '{'
            _printParenthesis(str, pos + 1, n,
                              open + 1, close)

"""

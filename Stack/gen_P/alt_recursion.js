/**
 * @param {number} n
 * @return {string[]}
 */
var generateParentheses = function (n) {
    const returnArray = [];
    const buildP = (str, openParenNum) => {
        console.log(str)
        /*
            means we can add another opening paren as the count of open paren is < than the providing integer input
            i.e. we start at 1 for input, 1 < 3 we can add more up till the input limit
                recursive call where we add another opening and increment by 1
        */
        if (openParenNum < n) {
            buildP(str + '(', openParenNum + 1)
        }
        /*
            if length of current combo is less than twice current openParen count add closing

        */
        if (str.length < openParenNum * 2) {
            buildP(str + ')', openParenNum)
        }
        /*
            valid combo has formed and can be pushed intro returnArray
        */
        if (n * 2 === str.length) {
            returnArray.push(str);
            console.log(returnArray)
        }
    }
    buildP('(', 1)
    return returnArray
}

console.log(generateParentheses(
    2
))

/**
 * in case of n=1, getting to buildP, str.length (1) < 1*2 -> we add closing, str now has  (), return to buildP -> 1*2 is 2 === str length which is 2 -> push str to returnArray, return array
 * in case of n=2
 */

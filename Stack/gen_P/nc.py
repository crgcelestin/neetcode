class Solution:
    def generateParenthesis(self, n: int) -> list[str]:
        """
        we know that we have to adhere to three rules
            1. only add open parenthesis if open<n
            2. only add closing parenthesis if closed<open
            3. valid only if open == closed == n
        """
        stack = []
        res = []

        def backtrack(openN, closedN):
            # if these are all the same, then append to results as we have created a suitable combo with n pairs
            if openN == closedN == n:
                # can be done with string
                res.append("".join(stack))
                return
            # when we want to add open paren, then check if less than supplied integer, increment openN, pop from stack
            if openN < n:
                stack.append("(")
                backtrack(openN + 1, closedN)
                stack.pop()
            # we need to compared closedN to openN meaning that we need to add a number of closed parens to create satisfactory combo
            if closedN < openN:
                stack.append(")")
                backtrack(openN, closedN + 1)
                stack.pop()

        backtrack(0, 0)
        return res


Test = Solution()
assert2 = ["(())", "()()"]
print(Test.generateParenthesis(2))
if Test.generateParenthesis(2) == assert2:
    print("PASSED")
else:
    print(f"expected:{assert2}")
    print(f"got:{Test.generateParenthesis(2)}")

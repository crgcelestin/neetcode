# 08 - 22 - 2023

## [Valid Parentheses](https://leetcode.com/problems/valid-parentheses/)

### Problem
- Given a string that contains the characters:
```py
opening_chars = [ '(', '{', '[' ]
closing_chars = [ ')', '}', ']' ]
```
- determine if input string is valid containing only those as possibilities

* input string is valid:
    - open brackets must be closed by same bracket type
    - open brackets must be closed in correct order
    - every close bracket has a corresponding open bracket of same type

### Analysis
- Input: we are given a string where the only possible characters are brackets of either opening_chars or closing_chars
- Output: we need to return a boolean true or false as to whether the given string is valid for the parentheses it contains

- Algo Process
    - Restate Problem
        * After ensuring that a string contains characters that are only closing, opening brackets of 3 given bracket types, we need to determine if a given string is valid as it relates to the given rules: that brackets are closed by the same type in order and each one has a corresponding same type

    - Goal of Function
        * We need to return a boolean of True if string is valid i.e it fulfills the given ruleset and false otherwise

    - Types
        - input is type string
        - output is type boolean

    - Assertions and Assumptions
        * strings should only contain brackets of those contained in the two bracket arrays

    - Edge Cases
        * consider strings that have arrays out of order but matching closing and opening in terms of potential brackets

#### Example \#1
```py
input1 = '()'
output1 = True
```

#### Example \#2
```py
input2 = "()[]{}"
output2 = True
```
#### Example \#3 , \#4, \#5 & \#6
```py
input3 = "(]"
output3 = False
```
```py
input4 = '[abcd]'
output = False
```
```py
input5 = ')( ][ }{'
output = False

input6 = '([)]'
output = False
```

#### Constraints
- `1 <= s.length <= 10^4`
- input string has only characters that are in the provided character set '()[]{}'

### Preliminary Solution
- __Time Complexity__: O(n)
- __Space Complexity__: O(n)

#### _Backside_
- My initial solution would involve:
    - create a dict where each opening bracket is a key, value is the corresponding closing bracket
        * initialize a stack in this case being an empty array `stack=[]`
    - iterate through string `for char in string`
        - add char to a stack `stack.append(char)`, if `len(stack)==0` the only char to append can be opening
        - if the next character following is the corresponding closing, then pop from stack
    - return true if stack is empty, false if stack is non-empty
    - passed the three provided test cases
        * on submit, passed only 63/93 testcases

- Second attempt:
    - Create an array contains all potential opening bracket possibilities `opening_brackets`, initialize a stack using deque() `from collections import deque` (better performance) or []
    - define helper function to be invoked `this will allow for the understanding of bracket order`
        * have three cases matching all closing bracket possibilities, each returning corresponding opening bracket match
    - iterate through input string
        - opening bracket case
            * if a character is in `opening_brackets`, then append it to the stack
        * closing bracket case
            * if stack has any number of elements + current stack element == returned bracket with current character as input `helper(char)`
                - then pop as that means we have matching brackets in correct order
        * any other case i.e closing bracket prior to opening, characters that are not brackets
            - return False (we auto now that a provided string is not valid)
    - returned is the boolean obtained from `return not stack`
        * if at the end stack is not empty we know the string IS NOT VALID


#### Additional Details
- Reflecting on First Attempt
    * Did not pass all test cases, but I believe had a good starting structure
    * Time complexity would have been O(n^2), n worst case that we iterate through all strings with them all being opening characters, to be stored in stack -> iterate through stack with worst case all opening
    * sc being O(n) with space growing linear with time
- Reflecting on Second Attempt -
    * Second attempt has tc of O(n) with sc of O(n)

#### Solution Code
- [JS Solution]()
- [Python Solution](./ValidParentheses.py)

### [Neetcode](https://www.youtube.com/watch?v=WTzjTskDFMg)
- [Neetcode Solution](./nc.py)
#### TC, SC
* tc and sc of O(n)

- #### NC Notes
    * suggested to use stack ds as we are always popping from top given the received closing bracket
    * we need to make sure that there is a type match using a hashmap
    * check length of stack in order to determine bool to be returned

def bracket_match(text):
    stack = []
    openClos = {")": "("}
    for paren in text:
        if paren in openClos:
            if stack and stack[-1] == openClos[paren]:
                stack.pop()
            else:
                stack.append(paren)
        else:
            stack.append(paren)
    return len(stack)

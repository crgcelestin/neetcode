class Node:
    def __init__(self, value, next=None):
        self.value = value
        self.next = next


class Stack:
    def __init__(self, first=None, last=None, size=0):
        self.first = first
        self.last = last
        self.size = size

    def append(self, value):
        newNode = Node(value)
        if not self.size:
            self.first = newNode
            self.last = newNode
        else:
            newNode.next = self.first
            self.first = newNode
        self.size += 1
        return self

    def pop(self):
        if not self.size:
            return
        prevNode = self.first
        if self.size == 1:
            self.first = None
            self.last = self.first
        self.first = prevNode.next
        self.size -= 1
        return self

    def traverse(self):
        current = self.first
        counter = 0
        if counter < self.size and current:
            print(current.value)
            current = current.next
            counter += 1
        return

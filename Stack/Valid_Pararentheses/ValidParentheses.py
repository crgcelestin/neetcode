'''
# 1st attempt
class Solution:
    def isValid(self, s:str) -> bool:
        tracker={
            "(" : ")",
            "[" : "]",
            "{" : "}"
        }
        stack=[]
        for char in s:
            if len(stack)==0:
                if char in tracker.keys():
                    stack.append(char)
            if len(stack)>0:
                for key in stack:
                    if tracker[key]==char:
                        stack.pop(stack.index(key))
        return len(stack)==0
'''

# 2nd atempt
class Solution:
    def isValid(self, s:str) -> bool:
        from collections import deque
        # define opening brackets array
        opening_chars = [ '(', '{', '[' ]
        # stack to pop from (can just do stack=[] but worse performance)
        stack=deque()
        # using a helper function to avoid nested loops
        # helper will deal with case of closing brackets, if character matches any of closing bracket possibilities then we return that opening bracket
        def helper(char:str) -> str:
            if char=="}" : return "{"
            if char=="]" : return "["
            if char==")" : return "("
        # iterate through chars in input string
        for char in s:
            # if a given character matches any elements in array of opening bracket strings then add to stack
            if char in opening_chars:
                stack.append(char)
            # if there is elements in stack and current element in it contains a opening bracket that matches output of helper then we can pop as that means we have the correct combination
            elif stack and stack[-1] == helper(char):
                stack.pop()
            else:
                # not opening or matching closing, we know its either not a bracket or not in correct order
                return False
        # if stack is empty, then we know its a valid string
        return not stack


# Test Suite
Test=Solution()
First = Test.isValid(
    '()'
)
print(First)

Second = Test.isValid(
    "()[]{}"
)
print(Second==True)

Third = Test.isValid(
    "(]"
)
print(Third==False)

Fourth = Test.isValid(
    '[abcd]'
)
print(Fourth==False)

Fifth = Test.isValid(
    ')( ][ }{'
)
print(Fifth==False)

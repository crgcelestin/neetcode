class Solution:
    def carFleet(self, target: int, position: list[int], speed: list[int]) -> int:
        # create array of pairs
        pairs = [[p, s] for p, s in zip(position, speed)]
        stack = []
        # reverse sorted order i.e right to left - if we start at beginning, we are unaware of cars closer to target
        for p, s in sorted(pairs)[::-1]:
            # understand time it takes for car to reach destination, which is just remaining distance
            stack.append((target - p) / s)
            # if we have at least two cars and car behind reaches the target ahead of the car prior then they become a fleet
            if len(stack) >= 2 and stack[-1] <= stack[-2]:
                stack.pop()
        return len(stack)

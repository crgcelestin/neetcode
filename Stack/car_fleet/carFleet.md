# 12.24.23

## [CAR FLEET](https://leetcode.com/problems/car-fleet/description/)

### Problem
- There are `n` cars going down the same 1 lane road with a destination that is `target` miles away
- Provided: 2 integer arrays `position[] and speed[]` of length `n` where `position[i]` is the `ith` car and `speed[i]` is speed of `ith` car (in mi/hr) [miles/hr]

* car never can pass a car ahead of it, can catch up to it and drive approximate @ same speed
* faster car slows down to match slower car's speed
* speed between 2 cars is ignored (assumed to have same position)

- Car fleet = non-empty set of cards driving at same position and speed, single car = car fleet
- if a car catches up to car fleet right at destination, it is considered as 1 car fleet
- __*GOAL*__: __Return # of Car Fleets that arrive at destination__

### Analysis
- Input: position, speed, target
- Output: # of car fleets

- Algo Process
    - Restate Problem
        * we need to determine the number of car fleets that will occur given our provided parameters with the known def that a fleet are a series of cars that meet up with each other at some point along the way to the destination

    - Goal of Function
        * return the number of car fleets possible

    - Types
        * list[int], int

    - Assertions and Assumptions
        * all values of position are unique, cars that catch up to each other are assumed to have same position with distances ignored, if cars meet at destination they are 1 fleet
        * it appears that when cars become a fleet, the speed of fleet becomes that of the car that started in further position i.e fleet speed becomes car with max(speed)

    - Edge Cases
        * zeros occurring in any of the inputs

#### Example \#1
- Input: target = 12, position = [10,8,0,5,3], speed = [2,4,1,1,3]
- Output: 3
- Explanation:
    * The cars starting at 10 (speed 2) and 8 (speed 4) become a fleet, meeting each other at 12.
        - 10+2 -> 12, 8+4 -> 12 (meet at hr 1)
    * The car starting at 0 does not catch up to any other car, so it is a fleet by itself.
        - 0+1 -> 1 +1 ->2 ... 11+1 -> 12
    * The cars starting at 5 (speed 1) and 3 (speed 3) become a fleet, meeting each other at 6. The fleet moves at speed 1 until it reaches target.
        - 5+1 -> 6, 3+3 -> 6 (meet at hr 1)
    * Note that no other cars meet these fleets before the destination, so the answer is 3.

#### Example \#2
- Input: target = 10, position = [3], speed = [3]
- Output: 1
- Explanation:
    * There is only one car, hence there is only one fleet.

#### Example \#3 & \#4
- Input: target = 100, position = [0,2,4], speed = [4,2,1]
- Output: 1
- Explanation:
    * The cars starting at 0 (speed 4) and 2 (speed 2) become a fleet, meeting each other at 4. The fleet moves at speed 2.
        - 0+4 -> 4, 2+2 -> 4 (hr 1) +2 (1 fleet) -> 6 (hr 2)
    * Then, the fleet (speed 2) and the car starting at 4 (speed 1) become one fleet, meeting each other at 6. The fleet moves at speed 1 until it reaches target.
        - [meet at hr 2 to become one fleet] 4 + 1 -> (hr 1) 5 + 1 -> 6 (hr 2)

#### Constraints
- `n == position.length == speed.length`
- `1 <= n <= 10^5`
- `0 < target <= 10^6`
- `0 <= position[i] < target`
- all values of `position` are unique
- `0 < speed[i] <= 10^6`

### Preliminary Solution
- __Time Complexity__:

- __Space Complexity__:

#### _Backside_
- My initial solution:
    1. we have to decipher if and when any of the cars provided a given their relative positions and speeds are going to meet up at any given point
    2. this means that we should increment each car based on provided speed and compare all of their positions via updating based on delta per hr
        - i.e each car is represented by  `(speed)(x [hours]) + start = position`
    3. we need to compare after every hour prior to all cars reaching destination
        - if using [i,t] of position.entries()
        `while there is an i<target: continue to increment said car`
    4. then find which cars meet up together either prior or just at destination and combine as 1

#### Solution Code
[JS Solution](CF.js)
[Python Solution](nc.py)

### [Neetcode](https://www.youtube.com/watch?v=Pr6T-3yB9RM)
- [Neetcode Solution](nc.py)
#### TC, SC
* TC: O(n) for iteration -> O(n log n) following sorting
* SC: O(n) creating a stack

- #### NC Notes
* Best idea to treat each car as being its own linear equation whereby any occurrences of intersection mean that cars join and become fleets
* easier way - if car reaches target prior to another car ahead of it reaching a destination then those two cars become a fleet
    * (position, speed) = [(3,3),(5,2),(7,1)]
    * 3x+3 = 10, 2x+5 = 10, x+7=10 -> (1) 7/3 HRS which is less than 3, (2) 2.5 HRS, (3) 3HRS
    * so all cars become a fleet, fleets = 1
* iterate right to left as we could miss collisions in the opposite direction
* using stack, we pop from stack given the occurrence of collisions, as we iterate from right to left, it will always be the leftmost i.e the 'top' most car that will be popped in order for us to retain an accurate fleet record

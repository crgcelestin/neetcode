/**
 * @param {number[]} position
 * @param {number[]} speed
 * @param {number} target
 * @return {number}
 */
// alt solution
var carFleet = function (target, position, speed) {
    const pairs = []
    for (let i = 0; i < position.length; i++) {
        pairs.push([position[i], speed[i]])
    }
    // car pos will be < than target if at start, initialize at 0
    let fleets;
    let curtime;
    fleets = curtime = 0
    let pos;
    let vel;
    for ([pos, vel] of pairs.reverse()) {
        let remaining_time = (target - pos) / vel
        if (curtime < remaining_time) {
            fleets += 1;
            curtime = remaining_time;
        }
    }
    return fleets;
}
const target = 12
const position = [10, 8, 0, 5, 3]
const speed = [2, 4, 1, 1, 3]
TEST1 = carFleet(
    target, position, speed
)
const assertTest = 3
if (JSON.stringify(TEST1) == assertTest) {
    console.log('TEST PASSED')
} else {
    console.log(`expected ${assertTest}\nreceived ${TEST1} `)
}

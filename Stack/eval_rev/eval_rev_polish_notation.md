# 09/29/23

## [150. Evaluate Reverse Polish Notation](https://leetcode.com/problems/evaluate-reverse-polish-notation/description/)

### Problem
- Given array of strings `list[str] : tokens` that represents arithmetic expression in Reverse Polish Notation -> evaluate expression (return integer representing value of expression)
    * RPN  = postfix notation (operators follow operands)
    * examples:
        - add 3 and 4, traditional: 3+4, RPN: 3 4 + -> 7
        - subtract 3 from 3 then add 5 to result, traditional: 3 - 4 + 5, RPN: 3 4 - 5 +  -> 4

    - Notes:
        * Valid operators are `+, -, *, /`
        * Each operand is an integer or other expression
        * division between two integers truncates -> 0
        * no division by 0
        * input represents valid arithmetic expression in RPN
        * answer and all intermediate calcs can be represented as 32-bit int

### Analysis
- Input: array of strings `tokens`
- Output: integer

- Algo Process
    - Restate Problem
        * convert an array of strings consisting of numbers and operands into a RPN arithmetic expression then return the integer output of said expression

    - Goal of Function
        * convert strings to RPN expression then return integer output of RPN arithmetic expression
    - Types
        * input is type array consisting of type int
        * output is type int

    - Assertions and Assumptions
        * We know that the array of strings is only going to consist of numbers and operands
        * we want to round down to 0, no 9 division and the expression will always be decipherable

    - Edge Cases (regardless of assertions, assumptions)
        * handling decimals
        * we can be provided an array where the order of strings can't be deciphered into a possible RPN arithmetic expression
        * we can be provided an array that is all integers, all operands
        * we can be provided an array that have strings that don't have any of the accepted types either strings that can't be converted into numbers or operands


#### Example \#1
```py
tokens = ["2","1","+","3","*"]
output = 9
# ((2 + 1) * 3) = 9
```

#### Example \#2
```py
tokens = ["4","13","5","/","+"]
output = 6
# (4 + (13 / 5)) = 6
```
#### Example \#3 & \#4
```py
tokens = ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]
output = 22
'''
((10 * (6 / ((9 + 3) * -11))) + 17) + 5
= ((10 * (6 / (12 * -11))) + 17) + 5
= ((10 * (6 / -132)) + 17) + 5
= ((10 * 0) + 17) + 5
= (0 + 17) + 5
= 17 + 5
= 22
'''
```

#### Constraints
- `1 <= tokens.length <= 104`
- `tokens[i]` is either operator: `+, - , *, /` or integer in range `[-200, 200]`

### Preliminary Solution
- __Time Complexity__: O(n), iterate through entire array of strings of length n

- __Space Complexity__: O(n), employing additional data structure of stack that worst case includes all strings in input array

#### _Backside_
- My initial solution would involve: detecting if a character is a operator or string then performing calculations at runtime using two helper functions
- Second attempt: can use one helper function to detect if operator, then initialize a stack where you append integers and perform calcs if stack is greater than 2 and if you encounter operator


#### Additional Details
- Reflecting on First Attempt: just a bit verbose
- Reflecting on Second Attempt able to get first two to pass then missed the last test

#### Solution Code
[JS Solution]()
[Python Solution](ERPN.py)

### [Neetcode](https://www.youtube.com/watch?v=iu0082c4HDE)
- [Neetcode Solution](nc.py)
#### TC, SC
- tc and sc both are O(n)
    - tc increasing linearly with input, time increases linearly with increase in input size
    - sc we are using an array for stack implementation, as we append, pop, stack worst case could be full of integers if no operands inputted, thus growing in size linear with input

- #### NC Notes
* if use ds of stack, each operator is applied to previous values
* each value is added to stack and then operator encountered, execute operation, then append result to stack
* each result will be truncated to 0, so round down to 0 for all decimals without non decimal integer

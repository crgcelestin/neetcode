class Solution:
    def evalRPN(self, tokens: list[str]) -> int:
        stack = []
        for token in tokens:
            if token == "+":
                # pop from stack twice and perform operation, same for + and *
                stack.append(stack.pop() + stack.pop())
            elif token == "-":
                # reverse to perform subtraction order same with /
                num1, num2 = stack.pop() + stack.pop()
                stack.append(num2 - num1)
            elif token == "/":
                num1, num2 = stack.pop() + stack.pop()
                # int rounds to integer and to 0 at same time
                stack.append(int(num2 / num1))
            elif token == "*":
                stack.append(stack.pop() * stack.pop())
            else:
                stack.append(int(token))
        return stack[0]

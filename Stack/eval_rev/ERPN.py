class Solution:
    def evalRPN(self, tokens: list[int]) -> int:
        # thought we should use a way to detect if a character is an operand or integer
        def is_num(input):
            return ord('0')<= ord(input)<=ord('9')
        def is_operand(input):
            char = ord(input)
            if char == ord('+') or char == ord('-') or char == ord('*') or char == ord('/'):
                return True
        stack = []
        for token in tokens:
            '''
            what i want to do is append each string in the array to stack
            example is array below
            we get 2 -> append to stack
            next character is 1 -> append to stack
            next is operand -> there are integers prior to operand
            pop integers from stack and perform operation with operand (operand is between both integers)
            0 2 1
            int(2) + int(1) results in 3

            append 3 to stack
            next character is an integer -> append to stack
            next character is an operand -> pop integers from stack and perform operation
            0 2 1
            3 * 3 results in 9

            return 9
            '''
            # if is_num(token):
            #     print(f'number {i}')
            # if is_operand(token):
            #     print(f'operand {i}')

        return 'done'

import math
class Solution2:
    def evalRPN(self, tokens: list[int]) -> int:
        def is_operator(input):
            return input in ["+","-","/", "*"]
        stack = []
        for token in tokens:
            if is_operator(token):
                if len(stack)>=2:
                    operand1=stack.pop()
                    operand2=stack.pop()
                    if token == "+":
                        stack.append(operand1+operand2)
                    if token == "-":
                        stack.append(operand1-operand2)
                    if token == "/":
                        stack.append(math.floor(operand2/operand1))
                    if token == "*":
                        stack.append(operand1*operand2)
            else:
                stack.append(int(token))
        return stack[0]

Test = Solution2()
print(Test.evalRPN(
    ["2","1","+","3","*"]
))
print(Test.evalRPN(
    ["2","1","-","3","/"]
))
print(Test.evalRPN(
    ["4","13","5","/","+"]
))
print(Test.evalRPN(
    ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]
))

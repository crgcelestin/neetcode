/*
    in js can declare functions with various syntaxes, fxn declarations, expressions, arrow functions
*/

/*
    fxn declaration def named function that can be called -> hoisted to top to be used anywhere in scope
    ideal for fxns to be reused, available throughout scope
*/
function stack() {

}

/*
    function expression -> defines anon function
    Not hoisted like fxn declarations so can be used after variable assignment
    Useful for creating functions dynamically or in cases where no need to ref function by name
*/
var stack1 = (nums) => {

}

/*
    arrow function = concise method to define functions an is an anon function
    Have a lexical 'this' where they inherit value from contained scope
    Good for short, simple functions when desiring to maintain 'this' context
*/
const stack2 = (nums) = {

}

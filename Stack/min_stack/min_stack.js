class stackNode {
    constructor(value) {
        this.value = value;
        this.next = null;
    }
}

class MinStack {
    constructor() {
        this.first = null;
        this.last = null;
        this.size = 0;
    }
    /**
     * @param {int} val
     * @return {void}
     */
    push(val) {
        const newNode = new stackNode(val)
        if (!this.size) {
            this.first = newNode
            this.last = this.first
        } else {
            newNode.next = this.first
            this.first = newNode
        }
        this.size += 1
    }
    /**
     *
     * @return {void}
     */
    pop() {
        if (!this.size) return;
        let prev = this.first
        if (this.size == 1) {
            this.first = null
            this.first = this.last
        }
        this.first = prev.next
        prev = null
        this.size -= 1
    }
    /**
     *
     * @return {number}
     */
    top() {
        return this.first.value
    }
    /**
     *
     * @return {number}
     */
    getMin() {
        if (!this.size) return;
        if (this.size == 1) {
            return this.first.value
        }
        let current = this.first
        let min = current.value
        while (current) {
            if (current.value < min) {
                min = current.value
            }
            current = current.next
        }
        return min;
    }
    iterate() {
        let current = this.first
        while (current) {
            console.log(current)
            current = current.next
        }
    }
}

var obj = new MinStack()
obj.push(-2)
obj.iterate()
obj.push(0)
obj.push(-3)
// obj.iterate()
obj.getMin()
// obj.iterate()
obj.pop()

var param_3 = obj.top()
console.log(param_3)
obj.iterate()
var param_4 = obj.getMin()
console.log(param_4)
obj.iterate()

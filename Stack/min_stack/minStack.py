class StackNode:
    def __init__(self, value, next=None):
        self.value = value
        self.next = next


class MinStack:
    def __init__(self, first=None, last=None, size=0):
        self.first = first
        self.last = last
        self.size = size

    def push(self, val: int) -> None:
        newNode = StackNode(val)
        if not self.size:
            self.first = newNode
            self.last = newNode
        else:
            newNode.next = self.first
            self.first = newNode
        self.size += 1

    """
    def pop(self) -> None:
    if not self.first:
        return
    old_first = self.first
    self.first = old_first.next
    old_first.next = None  # Ensure old_first is disconnected from the list
    self.size -= 1
    if not self.first:  # If the list becomes empty
        self.last = None
    """

    def pop(self) -> None:
        if not self.size:
            return
        prevNode = self.first
        if self.size == 1:
            self.first = None
            self.last = self.first
        self.first = prevNode.next
        prevNode = None
        self.size -= 1

    def top(self) -> int:
        return self.first.value

    def getMin(self) -> int:
        if not self.size:
            return
        if self.size == 1:
            return self.first.value
        current = self.first
        min = current.value
        while current:
            if current.value < min:
                min = current.value
            current = current.next
        return min


minStack = MinStack()
minStack.push(-2)
minStack.push(0)
minStack.push(-3)
print(minStack.getMin())
# return -3
minStack.pop()
print(minStack.top())
# return 0
print(minStack.getMin())
# return -2

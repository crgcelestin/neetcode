# 09 / 08 / 23

## [155. Min Stack](https://leetcode.com/problems/min-stack/)

### Problem
Design/implement a stack supporting push, pop, top and retrieving min element in constant time (O(1))

### Analysis
- Input:
    DS = Data Structure
    * we are given a skeleton for minstack in terms of what we are to implement as it pertains to __init__, push, pop, top, getMin
    - stack as we know is First in Las Out, Last in First Out DS
    * we are provided steps to execute for testing
        - input array had both operations and values
- Output:
    * we need to return the correct implementation of minstack with some methods returning values, others changing order of stack elements
    proper methods
    ```py
    # provided test case series
    minStack = MinStack();
    minStack.push(-2);
    minStack.push(0);
    minStack.push(-3);
    print(minStack.getMin()); # return -3
    minStack.pop();
    print(minStack.top());    # return 0
    print(minStack.getMin()); # return -2
    ```

- Algo Process
    - Restate Problem
        * design correct minstack implementation of push, pop, top, getMin methods to be done in constant time O(1)
    - Goal of Function
        * implement minstack
    - Types
        * class that objects correct outputs, with defined methods
    - Assertions and Assumptions
        * all operations take O(1) tc
    - Edge Cases
        * Handle empty stack, stack with 1 element, element with several duplicate min values

#### Example \#1
```js
//Input
["MinStack","push","push","push","getMin","pop","top","getMin"]
[[],[-2],[0],[-3],[],[],[],[]]

//Output
[null,null,null,null,-3,null,0,-2]

Explanation
MinStack minStack = new MinStack();
minStack.push(-2);
minStack.push(0);
minStack.push(-3);
minStack.getMin(); // return -3
minStack.pop();
minStack.top();    // return 0
minStack.getMin(); // return -2
```

#### Constraints
- `-2*31 <= val <- 2^31 - 1`
- Methods `pop`, `top`, `getMin` will be called on non-empty boards
- At most `3*10^4` calls are made to push, top, getMin

### Preliminary Solution
- __Time Complexity__: O(n) worst case, as you are iterating through each node in order to find the minimum value of current stack

- __Space Complexity__: O(1) space complexity as you are not using additional data structures, but rather nodes

#### _Backside_
- My initial solution would involve:
    * implement minstack with each method without using advanced/current methods like .pop(), . push() and be able to perform ops with intuition and stackNode class.
    * sacrificed space for more efficient ops, able to implement methods on interview without needing already current functions
- Second attempt:
    Skipping to NeetCode for walkthrough


#### Additional Details
- Reflecting on Attempt
* stack is first in last out, last in first out (LIFO)
* implemented a stacknode class to represent node with value, next pointer and actual minStack class that includes the __init__, push, pop, top, and getMin methods
    - __init__ has the first, last, size attributes
    - push (self, val: int) -> None: (want to make new node the first on stack and rest of nodes following due to stack structure)
        * input of val, we store val in new Node instance
        * we have two conditions
            - stack is empty i.e size is 0: first and last will be new node `self.first = newNode, self.last=self.first`
            - stack has node in it, new node will now be first and next nodes will come after `newNode.next = self.first, self.first = newNode`
        * increment size
    - pop (self) -> None: (want to take from top of stack i.e the last node to be inserted)
        * check if stack is empty, if it is return
        * store first node `prev = self.first`
        * if stack has 1 node, `self.first = None, self.first = self.last`
        * if stack has more than 1 node,
            - set start of stack to now start at node that is next after first node
             `self.first is now prev.Next`
            - set prev to now be Empty/None
            `prev=None`
            - decrement size
            `self.stack-=1`
    - top (self) -> int:
        * `return self.first.value`
    - getMin(self) -> int:
        * if stack is empty, return
        * if stack size is 1, return the only node's value
        * if more than 1 node, we want to get min:
            * start at first node `current=self.first`
            * store value of first node in min `min=current.value`
            * we want to iterate through stack and find a value that will be lower than any other value
            ```py
            while(current.value):
                if(current.value<min):
                    min=current.value
                current=current.next
            ```
        * return min at end `return min`


#### Solution Code [primitive solution]
- [Python Solution](minStack.py)
- [JS Sol](min_stack.js)

### [Neetcode](https://youtu.be/qkLl7nAwDPo) [using array methods]
- [Neetcode Solution](nc.py)
#### TC, SC
- O(1) constant time for operations
- O(n) due to two stacks that grow linearly with provide integers pushed in proportional to input, worst case storing all 'n' elements (array as you iterate has succeeding decreasing values)

- #### NC Notes
* know how to getMin in O(n) time, how to get O(1)
    - each node in stack has a corresponding minimum value
    - when 1 value, we know min is that value, add next value that is not less, min is same
    - if we have duplicate of same min and then delete, how do we know what min still is
    - Should compare prev min and new min, if lesser num is pushed into stack
    * __require 2 stacks in order to perform getMin in O(1) time

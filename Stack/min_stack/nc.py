class MinStack:
    # in order to determine minimum of a minstack, we need to have two stacks, one to store correct minimums in order and other to store values pushed to stack
    def __init__(self):
        self.stack = []
        self.minStack = []

    def push(self, val: int) -> None:
        self.stack.append(val)
        # self.minStack could be potentially empty, if empty just take min of val
        val = min(val, self.minStack[-1] if self.minStack else val)
        self.minStack.append(val)

    def pop(self) -> None:
        self.stack.pop()
        self.minStack.pop()

    def top(self) -> int:
        return self.stack[-1]

    def getMin(self) -> int:
        return self.minStack[-1]

minStack = MinStack();
minStack.push(-2);
minStack.push(0);
minStack.push(-3);
print(minStack.getMin()); # return -3
minStack.pop();
print(minStack.top());    # return 0
print(minStack.getMin()); # return -2

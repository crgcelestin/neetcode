'''
743 - Network Delay Time

Rare problem req implementation of Dijkstra's
Given a network of n nodes labeled from 1 to n, given list of times that are going to be edges
Directed Graph

times[i] = (u(i), v(i), w(i))
times[i] -> triple value (source node, target node, weight of edge i.e time of traversal)

Given an original node k and return total time for signal form k to be received by all other nodes

        (1)
    1 < --- 2
            |
            | (1)
    4 - - - 3
        (1)

Output for this graph structure would be 2
By 2 units of time the k signal has traveled to all nodes

Optimal Solution - BFS (dijkstra's) using a min heap (priority queue)
    - shortest path algo
    - for every other node it tells you the shortest path using edge weights

      (4)
    1 --- 2
(1) |     |(1)
    3 --- 3
      (1)

order add to min heap: starting from 1, add 3 then 2 then 4
log n operation
in minheap keep track of minimal path (key), node (value)
path node
0     1 (visited) (pop off)
1     3 (visited) (pop off) as going to 3
4     2  takes us a path of 1
2     4 (visited) (pop off as 2 is less than 4)
3     2 (3 is less than 4 then pop off, visited)
- Takes 3 units of time collectively

E (total number of edge) =  V^2 (# of nodes squared)
Every heap operation worse case is O(E log V)
'''
import heapq
from collections import defaultdict
class Solution:
    def networkDelayTime(self, times:list[list[int]], n:int, k:int):
        #  k being the source node, n being the number of nodes in network

        # create adjacency list for dijkstra's
        # dict/hashmap of edges that is initially an empty list
        edges=defaultdict(list)

        # go through every edge and obtain all neighbors, v being neighbor node and w being the weight in order to fill up adjacency lsit
        for u, v, w in times:
            edges[u].append((v,w))
        # initialize minheap
        minheap=[(0,k)]
        #keep track of all nodes visited
        visit=set()
        #max val -> cost to target node
        t=0
        while minheap:
            w1, n1 = heapq.heappop(minheap)
            # don't want to visit a node multiple times
            if n1 in visit:
                continue
            visit.add(n1)
            # set t to max weight, update what's to be returned
            t=max(t, w1)

            # bfs segment (iterate through neighbors and corresponding weights)
            for n2, w2 in edges[n1]:
                if n2 not in visit:
                    heapq.heappush(minheap, (w1+w2, n2))
        return t if len(visit) == n else -1

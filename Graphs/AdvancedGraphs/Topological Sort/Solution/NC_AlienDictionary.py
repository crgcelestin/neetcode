# READ PROBLEM
'''
269 - Alien Dictionary (HARD)
Hard graph problem and involves topological sort
Involves a DAG ( Directed Acyclic Graph)

Alien language using English alphabet, order among letters is unknown
given list of words via lexical order i.e alphabetically

Return string of unique letters in new alien language sorted lexical increasing order, if no solution return " ", if there are multiple answers, return any of those

- string s is lexically smaller than t if first letter where there is a difference, letter in s comes before letter in t
- if first min(s.length, t.length) letters are same,, s is smaller if s.length<t.length
    i.e ape always comes before apes
    if we ever see apes -> ape, then there is no solution thus return ""

Example
    input: words - ['wrt', 'wrf', 'er', 'ett', 'rftt']
    output: 'wertf'
'''

# DRAWING EXPLANATION
'''
    wrt
    wrf
        -> t comes before f in alien language
            r -> t -> f
    wrt
    er
        -> e comes after w
            w -> e -> r..

    ett
        -> r comes before t
            ^ update the node representation above
    rftt
         -> e comes before r
           ^update above

           (overall) w->e->r->t->f
Can perform DFS optimally

lets change
f->w->e->r->t->f -> ...
invalid due to a contraction
can have w->e->w
[we,ee,we] (ambiguous)

For this question type you have to maintain the lexical relationship

Counter-example, y u can't just simply build string from the simple lexical relation on the line with (overall)

[
    A
    BA
    BC
    C
]
would build A->B->C
DFS search would create ACB -> C but C should come after B instead of before

Performing DFS Postorder renders 'CBA' which is the correct order in reverse, make sure to do so prior to returning

* Keep track for loop detection
[Visited] confirm nodes that were processed
after C visit pop back to root A and then B
instead of C to B
Provide false value for visited node
Tc: O(n)
[Path] - know if node is in current path
if node is in currently being traverse path, then give it value of true in path dict

2 dicts that track visits, path
'''

# CODING EXPLANATION
class Solution:
    def alienOrder(self, words: list[str])-> str:
        adj = {
            c: set() for w in words for c in w
        }
        # no duplicates, for every word, every char
        for i in range(len(words)-1):
            # go through every words pair
            w1, w2 = words[i], words[i+1]
            # words1 at index i, words2 at next index
            minLen = min(len(w1), len(w2))
            # get min length of both words

            #check base case - invalid ordering
            if len(w1) > len(w2) and w1[:minLen] == w2[:minLen]:
                return " "

            # go through every char and find first differing char, break as we only want first differing char
            # char in w2 comes after char in w1
            for j in range(minLen):
                if w1[j]!=w2[j]:
                    adj[w1[j].add(w2[j])]
                    break

            #keep track of visited
            # for each char, map true or false
            # false = visited, true = current path
            visit={}
            res=[]

            def dfs(c):
                # return true then we detect loop
                if c in visit:
                    return visit[c]
                # add True, as it has been visited and is at current path
                visit[c]=True
                # go through every char that is neighbor of c
                for nei in adj[c]:
                    if dfs(nei):
                        return True
                # no longer in current path
                visit[c]=False
                #after entire dfs, return in reverse order
                res.append(c)

            for c in adj:
                if dfs(c):
                    return " "
            res.reverse()
            return "".join(res)
        # cycle wasn't found
        # return False (?)

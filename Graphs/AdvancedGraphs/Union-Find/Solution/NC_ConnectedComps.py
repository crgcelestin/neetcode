'''
323 - Number of Connected Components in Undirected Graph (Premium)
If I can't find it
547 - Number of Provinces

There is a graph of n nodes
Given an integer n, array of edges where edges[i] = [a(i), b(i)] indicates if there is an edge between a(i) and b(i) in the graph

Return # of connected comps in graph

Connected COmponent - Individual portion of graph that is contiguous

In example there is two contiguous comps,
0 - 1   3
    |   |
    2   4

First approach would be to treat this problem as a DFS and go through every single node starting at 0, then mark as visited then 1 as visited, 2 as visited
( 1 dfs search completed )

Everything in connected comp is visited

Then go to 3 then mark as visited, then visit 4 mark as visited
( 2 dfs searches completed )

2 connected components

TC would be O(E+V)
Need to go through every edge and each vertex

BETTER APPROACH - Union Find
0 - 1   3
    |   |
    2   4
Maintain 2 arrays,

Parent = [0,1,2,3,4]
Each index representing each node initially
Each node is parent of itself initially
Multiple trees initially and as we go through each edge
Perform merges, take number of connected comps and decrement by 1

Rank = [1,1,1,1] initially
for 0 being parent of 1, its rank is now 2
2 as being of lower rank will now be a child of 0 and now rank of 0 increases to 3

Parent = [0,0,0,3,4]
Rank = [3,1,1,1,1]

Then with 3, 4 -> 3 becomes parent of 4 and the rank of 3 increases

Parent = [0,0,0,3,3]
Rank = [3,1,1,2,1]

Initial number of comps is 5
then 3 unions so 2 is # of connected comps
'''
class Solution:
    def countComps(self, n:int, edges: list[list[int]]) -> int:
        par = [i for i in range(n)]
        rank= [1] * n
        def find(n1):
        #find root parent
            res=n1
            while res!=par[res]:
            # optimization - path compression
            # set parent of result to grandparent if it exists
                par[res]=par[par[res]]
                par[res]=par[res]
            return res
        def union(n1, n2):
            p1, p2 = find(n1), find(n2)
            if p1==p2:
                return 0
            if rank[p2]>rank[p1]:
                par[p1]=p2
                rank[p2]+=rank[p1]
            else:
                par[p2]=p1
                rank[p1]+=rank[p2]
            return 1
        res = n
        for n1, n2 in edges:
            res -= union(n1, n2)
        return res

# Top 5 Graph Algorithms
## [Video Source](https://www.youtube.com/watch?v=utDu3Q7Flrw&list=WL&index=10&t=1s)

### DFS
- Time Complexity is O(n), n being number of nodes in graph
    - DFS requires HashSet/Binary Search Tree(detect cycle) as depth first search involves node traversal
    - Performed recursively there is no need for stack, but if done recursively then a stack is recommended for implementation
    ```
            A
          /   \
         B     E
        / \
       C   D
    DFS - Preorder
    [ 'A', 'B', 'C', 'D', 'E' ]
    Root -> traverse right branch -> min node -> then next branch ->... -> exhaust till max node return
    ```

### BFS
- Traverse graph differently
    - Same TC as DFS O(n), with the data structure required being a queue since it is coded iteratively using a hashset to detect duplicate nodes
    - Traverse entirety of layers successively prior to hitting leaf layer
    ```
            A
          /   \
         B     E
        / \
       C   D
       BFS
     [ 'A', 'B', 'E', 'C', 'D' ]
     ```

### Union-Find
- Union together disjointed sets and combine together efficiently
    - TC bounded by O(n log(n))
    - Requires a forest of trees data structure
- Want to know the number of connected comps of graph
    - Given 5 different nodes, if edges are arbitrarily added
- ![Union Find](Images/unionFind.png)
    - If the edges between B,C, and D are added, how many connected comps do we have
    - 3 would be connected
    - [Connected Components in Undirected Graph](../AdvancedGraphs/Union-Find/Solution/NC_ConnectedComps.py)

### Topological Sort
- Built on DFS
    - Given a directed acyclical graph, directed edges
    - Printing out values in a manner that every node prior to the one being printed has been traversed
    - ![Topological Sort](Images/topologicalSort.png)
    - Output being A,B,C,D,E with TC of O(n) and the req dataset being HashSet
    - [Alien Dictionary](../AdvancedGraphs/AlienDictionary/Solution/NC_AlienDictionary.py)

### Dijkstra's Algo
- Find shortest path from 1 node to another node in a graph
- Can have edges of varying weights
- With avg TC being E log V as one has to deal with edges, Heap or Priority Queue is used as DS for Graph (adjacency list, matrix)
    - [Network Delay Time](../RegularGraphs/NetworkDelayTime/Solution/NC_NetworkDelayTime.py)

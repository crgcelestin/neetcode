class Solution:
    def search(self, nums: list[int], target: int) -> int:
        l, r = 0, len(nums) - 1
        while l <= r:
            mid = (l + r) // 2
            if nums[mid] == target:
                return mid
            # scenarios where target is not in list middle
            # left portion
            if nums[mid] >= nums[l]:
                # need to go to right portion of left portion
                if target > nums[mid] or target < nums[l]:
                    l = mid + 1
                    # target is less than middle and greater than left most element
                else:
                    # need to go to left portion of left portion
                    r = mid - 1
            # right portion
            # nums[mid]<=nums[l]
            else:
                # need to go to left portion of right portion
                if target < nums[mid] or target > nums[r]:
                    r = mid - 1
                else:
                    # need to go to right portion of right portion
                    # target is greater than middle and less than right most element
                    l = mid + 1
        return -1

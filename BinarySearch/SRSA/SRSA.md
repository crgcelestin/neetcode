# 03.01.24

## [Search in Rotated Sorted Array](https://leetcode.com/problems/search-in-rotated-sorted-array/)

### Problem
- Integer array `nums` sorted in ascending order (includes distinct values)
- prior to being passed in function, nums is rotated at unknown pivot index `k` (`1 <= k < nums.length`) with resulting array = `[nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]]`
- given `nums` array after rotation and int `target`, return `target` index if present else -1
- write algo with O(log n) tc

### Analysis
- Input: nums: list[int], target:[int]
- Output: int

- Algo Process
    - Restate Problem
        * we are given a rotated array with numbers and target int that we are searching for, the goal is to have this algo run in O(log n) tc
        * decipher what part of rotated array the target lies in

    - Types
        * array (list), integer

    - Assertions and Assumptions
        * are unique values, array prior to rotation is sorted

    - Edge Cases
        * empty array, array not containing target, array with 1 element

#### Example \#1
```
nums1 = [4,5,6,7,0,1,2]
target1 = 0
output1 = 4
```

#### Example \#2
```
nums2 = [4,5,6,7,0,1,2]
target2 = 3
output2 = -1
```
#### Example \#3
```
nums3 = [1]
target3 = 0
output3 = -1
```

#### Constraints
- `1 <= nums.length <= 5000`
- `-10^4 <= nums[i] <= 10^4`
- all `nums` values are unique
- `nums` = ascending array possibly rotated
- `-10^4 <= target <= 10^4`

### Preliminary Solution
- __Time Complexity__: O(n)

- __Space Complexity__: O(1)

#### _Backside_
- My initial solution would involve:


#### Solution Code
- [JS Solution](SRSA.js)

### [Neetcode](https://www.youtube.com/watch?v=U8XENwh8Oy8)
- [Neetcode Solution](nc.py)
#### TC, SC


- #### NC Notes
* as original array is sorted, we have right and left portions that are sorted and with BS we have left, mid, right (left<=right)
* if target is greater than mid, then we search in left portion,
    - `if target>mid, search(right) or l = mid + 1` as we are in left portion
    - `if target<mid`
        - `if target<nuns[l], search right portion, l = mid + 1 `
        - `if target=>nums[l], search left portion, r = mid - 1 `

/*

# broken attempt

class Solution:
    def search(self, nums: List[int], target: int) -> int:
        l,r = 0, len(nums)-1
        while(l<r):
            mid = (l+r)//2
            if nums[l]<=nums[r]:
                if nums[mid] == target:
                    return mid
                elif nums[mid]<target:
                    l=mid+1
                else:
                    r=mid-1
            if nums[mid]>=nums[l]:
                l = mid + 1
            else:
                r = mid - 1
        return -1

 */

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
*/
var search = function (nums, target) {
    if (!nums.length) {
        return -1
    }
    let [start, end] = [0, nums.length - 1]
    let mid;
    while (start <= end) {
        mid = Math.ceil(start + (end - start) / 2)
        if (nums[mid] == target) {
            return mid
        }
        if (nums[mid] >= nums[start]) {
            // sorted portion
            if (target >= nums[start] && target <= nums[mid]) {
                end = mid - 1
            } else {
                start = mid + 1
            }
        } else {
            if (target >= nums[start] && target <= nums[end]) {
                start = mid + 1
            } else {
                end = mid - 1
            }
        }
    }
    return -1
}

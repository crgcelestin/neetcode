//idk

/**
 * @param {number[]} piles
 * @param {number} h
 * @param {number}
 */
var minEatingBs = function (piles, h) {
    let k = 1
    let l = 0
    let step = 0
    const goal = Array(piles.length).fill(0)
    const recursiveKoko = (piles, k, l, step, goal) => {
        if (piles === goal) {
            return k
        }
        while (step < h) {
            const piles_coppy = [...piles]
            while (l < piles.length) {
                if (piles_coppy[l] < k) {
                    piles_coppy[l] -= k
                } else {
                    piles_coppy[l] = 0
                }
                l += 1
            }
            step += 1
            if (piles_coppy !== goal) {
                recursiveKoko(piles, k + 1, l, step, goal)
            }
        }
    }
    recursiveKoko(piles, k, l, step, goal)
}

const input1 = [3, 6, 7, 11]
const h1 = 8
const input2 = [30, 11, 23, 4, 20]
const h2 = 6
const test1 = minEatingBs(input1, h1)
console.log(test1)
// const test2 = minEatingBs(input2, h2)
// console.log(test2)

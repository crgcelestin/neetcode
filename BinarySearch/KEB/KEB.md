# Feb / 6 / 2024

## [Koko Eating Bananas](https://leetcode.com/problems/koko-eating-bananas/description/)

### Problem Summary
- Koko loves eating bananas -> `n` banana piles where the `i(th)` pile has `piles[i]` bananas
    * so piles[i] = piles at position i in array of length n
- K can decide her Bs/hr eating speed `k` -> each hr, she chooses some pile and eats `k` Bs from said pile
    * if pile has less than `k`Bs, she eats all of them and does not eat any more during hr
    * k eats slowly but want to finish eating Bs prior to return of guards


### Analysis
- Input: piles where piles[i] is = to # of Bs in pile at position i, h which is the # of hours until guards return i.e time is over for her to consume all Bs
- Output: return min speed `k` where she can eat all Bs in provided `h` hours

- Algo Process
    - Restate Problem:
        * koko wants to consume all Bs in array in a given number of hours at her desired slow pace in order to finish them before guard returns

    - Goal of Function:
        * return min rate at which she can consume prior to guard returning after `h` hours

    - Types:
        * int, array

    - Assertions and Assumptions:
        * all elements in piles are ints, value provided for hours being `h` is int
        * contiguous

    - Edge Cases:
        * array with 0s or null values, or non ints

#### Example \#1
```
input: piles = [3, 6, 7, 11], h = 8
output: 4
```

#### Example \#2
```
input: piles = [30, 11, 23, 4, 20], h = 5
output: 30
```
#### Example \#3
```
input: piles = [30, 11, 23, 4, 20], h = 6
output: 23
```

#### Constraints:
- `1 <= piles.length <= 10^4`
- `piles.length <= h <= 10^9`
- `1<= piles[i] >= 10^9`

#### _Backside_
- My solution would involve:
    * So my initial thinking is that its just a finding of an average Bs to eat per hour in order to consume the sum of all Bs  given array elements
    * find average using sum(arr)/h
    * however as a binary search problem, I am understanding there is a different implementation for finding the proper min rate `k`, since average is middle of range i.e [k, ..., avg, ... high] -> we need to find k not avg

#### Additional Details
- Reflecting on Attempt:
    - understanding nc solution: need to iterate through a list of values from 1 to max of current Bs element in piles
    - we need to find the exact min rate at which we can achieve all Bs eaten within the provided hours range

#### Solution Code
- [JS Solution](KEB.js)
- [Python Solution](nc.py)

### [Neetcode](https://www.youtube.com/watch?v=U2SozAs9RzA&t=895s)
- [Neetcode Solution](nc.py)
#### TC, SC
TC: O(log(max(p)*p)), sc: O(1)

- #### NC Notes
* Brute force -> binary implementation
* k can only eat at most 1 pile per hour
* meaning h is always going to be `len(p)>=h` -> what is min eating at k
* in order to figure out correct min integer, we need to test all values in array from max element to theoretical min element (1)
    - for [3,6,7,11], h = 8, possible ks being [1,..., 11] (l, r between max and min)
        1. [0,6,7,11]
        2. [0,2,7,11]
        3, [0,0,7,11]
        4. [0,0,3,11]
        5. [0,0,0,7]
        6. [0,0,0,3]
        7. [0,0,0,0]
* naive: iterate through each value in array and compare to max, so O(max(p)*n), n being length of array and max(p) being the greatest element -> need to do comparisons twice
* binary: get tc to O(log(max(p)*p))
    1. w/ prev example, start at middle so we begin at 6, so dividing each value by middle possible k element -> 1+1+2+2 = 6 = res which is less than 8 in terms of # for hours
    2. since res = 6 is less than 8, r = mid (k) - 1 else if we get value greater than 8, l = mid (k) + 1
    3. perform binary search, mid = (r+l)//2 -> k = range[mid] and continue to ask what we get in terms of res (duration of hours)
    4. so for 6, r = mid - 1, and find value closer to a duration of 8 (h)
    - PERSONAL THOUGHTS (maybe)
        - sum = 0, for pointer in range(elements): sum+=elements[pointer]/k -> if sum === h and if !l<=r: res = sum or min(res, k)
        - return res
    5. left always has to be before right pointer

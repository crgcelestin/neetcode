import math


class Solution:
    def minEat(self, piles: list[int], h: int) -> int:
        # rate = r at max(piles) as we know this has to work
        # need to remember that there is a theoretical range of values from [1,...,max(piles)] that we are iterating through to understand the most optimal k
        l, r = 1, max(piles)
        rate = r
        while l <= r:
            k = (l + r) // 2
            hours = 0
            for p in piles:
                hours += math.ceil(p / k)
            if hours <= h:
                rate = min(rate, k)
                r = k - 1
            else:
                l = k + 1
        return rate


Test = Solution()
print(Test.minEat([3, 6, 7, 11], 8))

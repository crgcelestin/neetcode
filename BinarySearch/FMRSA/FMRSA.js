/**
 * @param {number[]} nums
 * @returns {number}
 */

var findMin = function (nums) {
    if (!nums.length) {
        return
    }
    // set default for observed min value -> typical BS setup
    let res = nums[0]
    let [l, r] = [0, nums.length - 1]
    let mid;
    while (l <= r) {
        // we know we are in already sorted array
        if (nums[l] < nums[r]) {
            return res = Math.min(res, nums[l])
        }
        // rotated - unsorted array
        mid = Math.ceil((l + r) / 2)
        res = Math.min(res, nums[mid])
        // if we encounter an array where middle element is greater than start, we know that middle value is in right portion, we need to search left which has less values
        // else we need to search right portion
        if (nums[mid] >= nums[l]) {
            l = mid + 1
        } else {
            r = mid - 1
        }
    }
    return res
};

console.log(findMin(
    [11, 13, 15, 17]
))

console.log(
    findMin([
        4, 5, 6, 7, 0, 1, 2
    ])
)

console.log(
    findMin([
        3, 1, 2
    ])
)

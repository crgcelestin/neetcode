class Solution:
    def findMin(nums: list[int]) -> int:
        lo = 0
        hi = len(nums) - 1
        while lo <= hi:
            mid = (lo + hi) // 2
            if nums[mid] < nums[hi]:
                ans = mid
                hi = mid - 1
            elif nums[mid] > nums[hi]:
                lo = mid + 1
            else:
                ans = mid
                hi = mid - 1
        return nums[ans]


print(Solution.findMin([11, 13, 15, 17]))
print(Solution.findMin([4, 5, 6, 7, 0, 1, 2]))

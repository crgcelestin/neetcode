# 2.29.24

## [153. Find Minimum in Rotated Sorted Array](https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/description/)

### Problem
* Suppose array of length `n` sorted in ascending order (i.e low -> high) is rotated 1->n times, ex: arr -> nums = [0,1,2,4,5,6,7] may become [4,5,6,7,0,1,2] if rotated 4 times, [0,1,2,4,5,6,7] if rotated 7 times
    - rotating array [a[0], a[1], a[2], ..., a[n-1]] 1 time results in the array [a[n-1], a[0], a[1], a[2], ..., a[n-2]]
* given sorted rotated arr `nums` of unique elements, return min element of array
    - write algo running in O(n log n) tc

### Analysis
- Input: `nums` -> array that has been rotated `n` times
- Output: int being the min of the array

- Algo Process
    - Restate Problem
        * given an array that is rotated a number of times and we need to decipher min

    - Goal of Function
        * figure out original array from provided array and obtain min element

    - Types
        * int, array
    - Assertions and Assumptions
        * we are assuming rotations are confined in range [1...n]
        * algo must run in O(log n) time meaning it must

    - Edge Cases
        * empty array, array of all the same elements

#### Example \#1
```
nums = [3,4,5,1,2]
output: 1, explanation: original array [1,2,3,4,5] is rotated 3 times
```

#### Example \#2
```
nums = [4,5,6,7,0,1,2]
output: 0, e: [0,1,2,4,5,6,7] rotated 4 times
```
#### Example \#3
```
nums = [11,13,15,17]
output: 11, e: [11,13,15,17] rotated 4 times
```

#### Constraints
- `n == nums.length`
- `1 <= n <= 5000`
- `-5000 <= nums[i] <= 5000`
- all ints of nums are unique
- nums is sorted, rotated between 1 and n times

#### _Backside_
- My solution would involve:
    * un-rotate array and then find min element in original array via arr[0] which is constant time access

#### Solution Code
- [JS Solution](FMRSA.js)
- [Python Solution](nc.py)

### [Neetcode](nc.py)
- [Neetcode Solution](https://www.youtube.com/watch?v=nIVW4P8b1VA)
#### TC, SC
tc: O(log n), sc:O(1)

- #### NC Notes
-   [1,2,3,4,5]
    l        r
    min value can be leftmost, if entire array is sorted i.e un-rotated then just nums[l]

-   [3,4,5,1,2]
    l    m   r
    at m are we in left or right sorted portion

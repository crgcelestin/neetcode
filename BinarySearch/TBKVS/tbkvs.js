
class TimeMap {
    constructor() {
        this.store = {}
    }
    set(key, value, timestamp) {
        if (!key) {
            TimeMap.store[key] = []
        }
        TimeMap.store[key].push([value, timestamp])
    }
    get(key, timestamp) {
        // if (!Object.keys(TimeMap.store).includes(key)) {
        //     return res
        // }

        // handle empty no values EC
        let res = ""
        if (!this.store[key]) {
            return []
        }
        // conduct binary search for timestamp value
        const [l, r] = [0, this.store[key].length - 1]
        while (l <= r) {
            let m = Math.ceil((l + r) / 2)
            if (this.store[key][m] <= timestamp) {
                res = this.store[key][m][0]
                l = m + 1
            } else {
                r = m - 1
            }
        }
        return res
    }
}

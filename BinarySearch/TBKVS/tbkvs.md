# March 9th, 2024

## [981. Time Based Key-Value Store](https://leetcode.com/problems/time-based-key-value-store/description/)

### Problem
- Design time based key-value DS that can store several values for same key @ various time stamps, retrieve key's value @ certain timestamp
- Implement `TimeMap` class:
    * `TimeMap()` initializes object of DS
    * `void set(String key, String value, int timestamp)` -> ex: `set('123', 123, 12)`
        - Stores key `key` w/ value `value` @ given time `timestamp`
    * `String get(String key, int timestamp)`
        - returns value such that `set` was called previously w/ `timestamp_prev <= timestamp`
        - if multiple values, return value associated with largest `timestamp_prev`
        - if no values, return `" "`

### Analysis
- Input: 2 arrays with first involving either set/get methods or TimeMap() instantiation
- Output: corresponding commands match assertion values/expected returns

- Algo Process
    - Restate Problem
        * implement TimeMap class
    - Goal of Function
        * create a TimeMap class that executes successfully on given commands given provided inputs
    - Types
        * list, string, int
    - Assertions and Assumptions
        * TimeMap(), set, get all operate properly, with get -> either return multiple values given `timestamp_prev` and if no values, return `""`
    - Edge Cases
        * provided non-existent commands, provided non-existing input

#### Example \#1
```
Input:
["TimeMap", "set", "get", "get", "set", "get", "get"]
[[], ["foo", "bar", 1], ["foo", 1], ["foo", 3], ["foo", "bar2", 4], ["foo", 4], ["foo", 5]]

Output:
[null, null, "bar", "bar", null, "bar2", "bar2"]

Explanation:
TimeMap timeMap = new TimeMap();
timeMap.set("foo", "bar", 1);  // store the key "foo" and value "bar" along with timestamp = 1.
timeMap.get("foo", 1);         // return "bar"
timeMap.get("foo", 3);         // return "bar", since there is no value corresponding to foo at timestamp 3 and timestamp 2, then the only value is at timestamp 1 is "bar".
timeMap.set("foo", "bar2", 4); // store the key "foo" and value "bar2" along with timestamp = 4.
timeMap.get("foo", 4);         // return "bar2"
timeMap.get("foo", 5);         // return "bar2"
```

#### Constraints
- `1 <= key.length, value.length <= 100`
- `key` and `value` involve lowercase English letters, digits
- `1 <= timestamp <= 10^7`
- all timestamps `timestamp` of `set` strictly increase
- at most `2*10^5` calls made to `set` and `get`

#### _Backside_
- My solution would involve:
    * TimeMap() much like the name would involve a map with self.store being {} as it would provide access via key
        - set would involve adding values with timestamp and dealing with allowing multiple instances of key
        - get would involve  handling the empty case, then handling the case of returning the existance of an object with a lesser or greater than value

### Preliminary Solution
- __Time Complexity__:
    - for set operation, .push() can involve O(n) time operation, worst case scenario we are given several values / for get op, O(log n) worst case target timestamp is at l,r pointer elements exactly meaning we will be splitting the list for a long time
- __Space Complexity__:
    - for set, .push can involve a long list of elements due to a long series of input thus O(n)/ for get its O(1)

#### Solution Code
- [JS Solution](tbkvs.js)
- [Python Solution](nc.py)

### [Neetcode](https://www.youtube.com/watch?v=fu2cD_6E8Hw)
- [Neetcode Solution](nc.py)
#### TC, SC


- #### NC Notes

class TimeMap:
    def __init__(self, key, value):
        self.store = {}

    def set(self, key: str, value: str, timestamp: int) -> None:
        if not key in TimeMap.store:
            TimeMap.store[key] = []
        TimeMap.store[key].append([value, timestamp])

    def get(self, key: str, timestamp: int):
        pass

# 01.13.24

## [74. Search a 2D Matrix](https://leetcode.com/problems/search-a-2d-matrix/description/)

### Problem
- given an `n*m` integer matrix with these 2 properties:
    * each row sorted in non-decreasing order
    * 1st integer of each row is > than last integer of previous row
- given `target:int` -> bool (true if target in matrix, false otherwise)
- solution is in O(log(m*n)) tc


### Analysis
- Input: target `int`, provided m*n matrix: `matrix`
- Output: return boolean depending on if integer is found in matrix or not `true`/`false`

- Algo Process
    - Restate Problem:
        * decipher if integer is present in matrix of element and return boolean communicating said detection
    - Goal of Function:
        * in O(log(n*m)) tc, we want to return bool based on element detection
    - Types:
        * list[list[int]], int -> bool

    - Assertions and Assumptions:
        * all elements in matrix are integers

    - Edge Cases:
        * empty matrix, target provided is not an integer

#### Example \#1
```
matrix1 = [[1,3,5,7],[10,11,16,20],[23,30,34,60]]
target1 = 3
assert1 = True
```

#### Example \#2
```
matrix2 = [[1,3,5,7],[10,11,16,20],[23,30,34,60]]
target = 13
assert2 = False
```

#### Constraints
- `m == matrix.length`
- `n == matrix[i].length`
- `1 <= m, n <= 100`
- `104 <= matrix[i][j], target <= 104`

#### _Backside_
- My solution would involve: first attempt was naive search, but i believe that a more efficient search can be performed that is closer that of binary search where we look at the first and last elements of the sub arrays in the matrix


#### Additional Details
- Reflecting on Attempt:

### Preliminary Solution
- __Time Complexity__:

- __Space Complexity__:

#### Solution Code
- [JS Solution]()
- [Python Solution](S2DM.js)

### [Neetcode]()
- [Neetcode Solution](nc.py)
#### TC, SC


- #### NC Notes
* Tasked with efficient solution for value in n*m matrix, ints are sorted from left to right
* binary search involves a log(n) tc
* instead of searching through every of m rows, (1) perform binary search to understand which of the rows we should search for target prior to performing the comparative search
* each row has a lower, upper bound -> eliminate rows based on the constraint of each row
* desire a better tc being logm+logn

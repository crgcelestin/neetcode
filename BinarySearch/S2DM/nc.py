class Solution:
    def searchMatrix(self, matrix: list[list[int]], target: int) -> bool:
        if not len(matrix):
            return False
        rows, cols = len(matrix), len(matrix[0])
        pMI, lMI = 0, (rows * cols) - 1
        while pMI <= lMI:
            mid = (pMI + lMI) // 2
            midE = matrix[mid // cols][mid % cols]
            if midE == target:
                return True
            elif midE < target:
                pMI = mid + 1
            else:
                lMI = mid - 1
        return False


Test = Solution()
input1 = [[1, 3, 5, 7], [10, 11, 16, 20], [23, 30, 34, 60]]
target1 = 3
target2 = 13
assert1 = True
assert2 = False
test1 = Test.searchMatrix(input1, target1)
if test1 == assert1:
    print("passed")
else:
    print(f"expected {assert1} -> got {test1}")
test2 = Test.searchMatrix(input1, target2)
if test2 == assert2:
    print("passed")
else:
    print(f"expected {assert2} -> got {test2}")

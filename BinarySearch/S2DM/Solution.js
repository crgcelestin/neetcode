/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {boolean}
 */
var searchMatrix = function (matrix, target) {
    // first binary search
    if (!matrix.length) return false;
    const [rows, cols] = [matrix.length, matrix[0].length]
    let [pMI, lMI] = [0, rows * cols - 1]

    while (pMI <= lMI) {
        let mid = Math.floor((pMI + lMI) / 2)
        /*
            Math.floor(mid/cols) or mid // cols -> find row index of middle element in flat matrix using rounding
            mid % cols -> finds the column index
            matrix[row][col] === matrix_element
        */
        let midElement = matrix[Math.floor(mid / cols)][mid % cols]

        if (midElement === target) {
            return true;
        } else if (midElement < target) {
            pMI = mid + 1
        } else {
            lMI = mid - 1
        }
    }
    return false;
}



const input1 = [[1, 3, 5, 7], [10, 11, 16, 20], [23, 30, 34, 60]]
const target1 = 10
const assert1 = true
console.log(searchMatrix(input1, target1))
// if (JSON.stringify(searchMatrix(input1, target1)) === JSON.stringify(assert1)) {
//     console.log("TEST PASSED")
// } else {
//     console.log(
//         `expected ${target1}, but got ${searchMatrix(input1)}`
//     )
// }
// console.log(
//     searchMatrix2(
//         input1, target1
//     )
// )

/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {boolean}
 */
var searchMatrix = function (matrix, target) {
    for (let unit of matrix) {
        for (let number of unit) {
            if (number === target) {
                return true
            }
        }
    }
    return false
    // O(log(m*n)) -> figure out method to reduce tc
    // we want to eliminate sub arrays based on if the first values is larger or smaller than our target in order to reduce time required
};

// :( m*(logN), need to get to log(mn)
var searchMatrix2 = function (matrix, target) {
    let l, r, middle;
    let cols = matrix[0].length
    let rows = matrix.length
    let target_row;
    const search = (search_term, row) => {
        l = 0
        r = row.length
        while (l < r) {
            middle = Math.floor((l + r) / 2)
            if (search_term > row[middle]) {
                l = middle + 1
            } else if (search_term < row[middle]) {
                r = middle - 1
            } else {
                return true
            }
        }
        return false
    }
    for (let i = 0; i < rows - 1; i++) {
        if (!matrix[i - 1] && target > matrix[i][0] && target < matrix[i][-1]) {
            return search(target, matrix[i])
        }
        if (matrix[i - 1] && target > matrix[i - 1][0] && target < matrix[i][0]) {
            target_row = matrix[i - 1]
            return search(target, matrix[i - 1])
        } else {
            target_row = matrix[i]
            return search(target, matrix[i])
        }
    }
    console.log(target_row)

    // for (let i = 0; i < rows; i++) {
    //     l = 0;
    //     r = cols - 1;
    //     while (l <= r) {
    //         middle = Math.floor((l + r) / 2)
    //         let middle_element = matrix[i][middle]
    //         if (middle_element === target) {
    //             return true
    //         } else if (target < middle_element) {
    //             r = middle - 1
    //         } else {
    //             l = middle + 1
    //         }
    //     }
    // }
    // return false
}

class Solution:
    def search(self, nums: list[int], target: int) -> int:
        left = 0
        right = len(nums)
        while left <= right:
            middle = (left + right) // 2
            if nums[middle] == target:
                return middle
            elif nums[middle] < target:
                left = middle + 1
            elif nums[middle] > target:
                right = middle - 1
        return -1


Tests = Solution()
test1 = Tests.search([-1, 0, 3, 5, 9, 12], 9)
print(test1)

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var BinarySearch = function (nums, target) {
    let l = 0;
    let r = nums.length - 1
    let middle;
    while (l <= r) {
        middle = Math.floor((l + r) / 2)
        if (nums[middle] === target) {
            return middle
        } else if (nums[middle] < target) {
            l = middle + 1
        } else {
            r = middle - 1
        }
    }
    return nums[middle] === target ? middle : -1
}

const nums1 = [-1, 0, 3, 5, 9, 12]
const nums2 = [-1, 0, 3, 5, 9, 12]
console.log(BinarySearch(
    nums1, 9
))
console.log(BinarySearch(
    nums2, 2
))

# 01.06.24

## [704. BINARY SEARCH](https://leetcode.com/problems/binary-search/description/)

### Problem
- given array of ints `nums` (list[int]) sorted in asc. and int `target`
- write fxn to search target in nums
- if target exists, return index else return -1
- write an algo that is O(log n) runtime complexity

### Analysis
- Input: array of integers, list[int] + target: int
- Output: return either index[int] or -1 meaning not found

- Algo Process
    - Restate Problem
        * given an list of ints and a target being the desired int to find the index of target
    - Goal of Function
        * return either index or -1
    - Types
        * list[int], int
    - Assertions and Assumptions
        * only provided array of integers, target being int
    - Edge Cases
        * provided an array of strings
        * provided an array that is empty or target that is empty
        * provided target that is non integer

#### Example \#1
```
nums = [-1,0,3,5,9,12]
target = 9
```
- output is 4, 9 exists in nums with index of 4

#### Example \#2
```
nums = [-1,0,3,5,9,12]
target = 2
```
- output is -1 as 2 doesn't exist

#### Constraints
- `1 <= nums.length <= 10^4`
- `-10^4 <= nums[i], target < 10^4`
- All ints in nums are unique
- `nums` sorted in asc order

#### _Backside_
- My solution would involve: the traditional BS approach


#### Additional Details
- Reflecting on Attempt: knew this

### Preliminary Solution
- __Time Complexity__: O(log n)

- __Space Complexity__:O(1)

#### Solution Code
- [JS Solution](BS.js)
- [Python Solution](nc.py)

### [Neetcode](https://www.youtube.com/watch?v=s4DPM8ct1pI)
- [Neetcode Solution](nc.py)
#### TC, SC
tc: log_2(n)

# 14 Common Patterns

## 1. Sliding Window
- Used to perform a required op on specific window size of given array/LL like finding longest subarray containing 1s
    * sliding window starts from 1st element(r=1), keep shifting right by one (r+=1), and adjusting window length according to problem
        - window may remain constant and in other cases it may grow/shrink
- Problems that may require sliding window:
    * __Problem input = linear data structure like LL, array, string__
    * __Asked to find longest/shortest substring, subarray, desired value__
        - Common problems: max sum subarray of size 'K' (easy), Longest substring w/ K distinct chars, string anagrams

## 2. Two Pointers/Iterators
### Two Pointers
- Pattern where 2 pointers iterate through ds until 1 or both pointers hit a desired condition
    * Useful when searching pairs in sorted array/LL, ex: compare each element of array to other elements
    - 2 pointers req as only 1 pointer would mean continuous looping of array to find answer -> would result in inefficient tc, sc (asymptotic analysis)
- Problems that may require two pointers:
    * __Problems involve dealing with sorted arrays/LLs and req to find set of elements fulfilling constraints -> Set of elements in array: pair, triplet, subarray__
        * Common Problems: Squaring sorted array, triplets that sum to zero, comparing strings with backspaces

## 3. Fast and Slow pointers [ Hare and Tortoise Algo ]
- Pointer algo that uses 2 pointers that move through array/ [sequence/LL] @ various speeds
    * __Useful when dealing with cyclic linked lists, arrays__
    * By moving at different speeds, algo proves 2 pointers are bound to eventually meet
        - Fast pointer should catch slow pointer when pointers are in cyclic loop
- Problems that may use Fast and Slow Pattern:
    * __Problems that deal with loop in LL/array, need to know position of certain elements or overall LL length__
    * Shouldn't use two pointer approach in LL where you can't move backwards
- __Problems ft fast and slow pointer patterns__:
    * LL cycle, Palindrome LL, Cycle in Circular Array

## 4. Merge Intervals
- Efficient technique to deal w/ overlapping intervals, in several problems: find overlapping intervals, merge intervals if they overlap
    * Pattern: Given two intervals 'a' and 'b', there are 6 various way 2 intervals can relate to each other:
        1. a, b do not overlap (a ends before b)
        2. a, b overlap but b ends after a
        3. a completely overlaps b (a is larger)
        4. a, b overlap and a ends after b
        5. b completely overlaps a (b is larger)
        6. a,b do not overlap (b ends before a)
    = Understanding, recognizing 6 cases will aid in solving wide problem range: inserting intervals -> optimizing interval merges
        * ID when to user Merge Intervals pattern:
            - Asked to prod list with only mutually exclusive intervals, hear term 'overlapping intervals'
            - __Merge Interval Problems__:
                * intervals intersection, max cpu load

## 5. Cyclic Sort
- Describes approach to deal with situations involving arrays w/ numbers in given range
* CS pattern iterates over array 1 # at a time and if current # is not @ correct index, swap it with number at correct index
    - Involves problems: involving sorted array with #s in given range, asked to find missing/duplicate/smallest # in sorted/rotated array
* __Problems with CS pattern: find missing #, find smallest missing positive #__

## 6. In-place reversal of LL
- Some problems may ask you to reverse links between nodes set of linked list -> the constraint is that you may need to perform this operation in place i.e using existing nodes, w/o using extra memory
    * if asked to rev LL w/o using extra memory
- __Problems with in place reversal of LL__: Rev sub list, rev every k element sub list

## 7. Tree BFS
- Traverse a tree level by level using queue to track nodes of level prior to next level ideal for problems that prioritize level by level order
    * Works by pushing root node to queue -> continue iteration until queue is empty -> for each iteration, remove node at queue head and visit node -> after removing each node from queue, we also insert all children into queue

- __ID Tree BFS Pattern__:
    * If asked to traverse tree in level by level fashion (level order traversal)
- __Problems with BFS__:
    - Binary Tree Level Order, ZigZag Traversal

## 8. Tree DFS
- Traverse a tree branch by branch (i.e depth of current node -> traverse children first before)
    * Use recursion (or stack - iterative) to keep track of previous (parent) nodes while traversing
    * Tree DFS works by starting at tree root, if node != leaf:
        1. Decide if to process current node now (pre-order) or process any 2 children (in-order) or following processing both children (post-order)
        2. make 2 recursive calls for both children of current node to process
- __Identify Tree DFS Pattern__:
    * asked to traverse tree w/ IO, Pre-O, Post-O DFS
    * req searching for entity where node is closer to leaf
- __Problems w/ DFS__:
    * sum of path numbers, all paths for a sum

## 9. Two Heaps
- in several problems, given set of elements that we can divide into 2 partitions -> interested in understanding smallest elements in 1 part and biggest element in other part
- Pattern uses 2 heaps:
    * min heap to find smallest element, max heap to find biggest element
        * store first half of #s in max heap (find largest num), store second half in min heap (smallest number in second half) -> median can be calculated from top ele of two heaps
- __Identify 2 Heaps Pattern__:
    * useful in priority queue, scheduling
    * if problem states that i need to find smallest/largest/median element of set
    * maybe with binary tree data
- __Problems__:
    * Find median of number stream

## 10. Subsets
- Several problems involve dealing with Permutations, Combinations of given element set:
    * Subsets pattern describes optimized BFS approach:
        0. Given set: [1,5,3]
        1. start w/ empty set - [[]]
        2. add first number(1) to current subsets to create new subsets - [[],[1]]
        3. add second number(5) to current subsets -> [ [], [1], [5], [1,5]]
        4. add third number(3) -> [ [], [1], [5], [1,5], [3], [1,3], [5,3], [1,5,3]]
- __Ideal for problems__: (1) where you need to find combos/permutations of given set, problems w/ subsets pattern
- __Problems__: (1) duplicates subsets, (2) string permutations by changing case

## 11. Modded Binary Search
- If given sorted array, LL, matrix -> asked to find certain element
    * Efficient way to handle problems involving Binary Search

- Patterns for ascending order set:
    1.  find middle index, mid = (start+end)//2 or mid = start+(end-start)//2
    2. if key = # at index mid return mid
    ... handle rest of cases

- __Problems__: binary search, search in sorted array, infinite array

## 12. Top K elements
- Problems that ask to find top/smallest/frequent 'K' among given set
- Best DS to track 'K' elements = Heap, Pattern makes use of Heap to solve various problems dealing w/ 'K' elements at time from set of given elements
    * Pattern:
        1. insert k elements into min-heap/max-heap based on problem
        2. iterate through rest of #s and if you find one that is > than one in heap, remove number and insert larger
- No need of sorting algo as heap keeps track of elements
* __ID Top K Elements Pattern__:
    * asked to find top/smallest/freq K elements of set
    * asked to sort array to find exact element

* __Problems__:
    - top k numbers, top k frequent numbers

## 13. K-way Merge
- aids in solving problems that involve set of sorted arrays
* when given K sorted arrays, use Heap to efficiently perform sorted traversal of all elements in all arrays:
    - push smallest element of each arary in minHeap to get overall min -> w/ overall min, push next element from same array -> repeat process to make sorted traversal of all elements
    - __Pattern__:
        1. insert first element of each array in min heap
        2. take out top element from heap -> add to merged list
        3. after removing smallest element from heap -> insert next element of same list in  heap
        4. repeat 2 and 3 to populated merged list in sorted order

* __Identify K way merge pattern__:
    - fts sorted arrays, lists, matrix
    - problem asks for merged sorted lists, find smallest element in sorted list

* __Problems__:
    - merge k sorted lists, k pairs with largest sums

## 14. Topological Sort
- Used to find linear ordering of elements that have dependencies on each other
    * Ex: Event B is dep on Event A, A is before B in topological order

- Pattern: defines easy method to understand technique for topological sorting of elements set
    1. Initialization
        - a.store graph in adjacency list w/ HashMap
        - b.find all sources, use HasMap
    2. Build graph from input, populate in-degrees HashMap
    3. Find sources
    4. Sort

- __ID TS Pattern__:
    * problem deals with graphs w/ no direct cycles
    * asked to update objs in sorted order
    * if you have class of objects that follow a certain order
- __Problems__:
    - task scheduling, min tree height

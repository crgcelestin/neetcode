# Reviewing Data Structures in Python
- Discussed: Lists, Tuples, Sets, Dictionaries
    - Data Structure Definition
        - Method/Specialized format of writing code that allows for the organizing, processing, retrieving, and storing of data
        - Makes it easy for users to access and work with data that is required in appropriate ways

## Different Types of Data Structures
```py
    '''
    Lists
    assign var name to [] with elements or none
    Elements in order and there can be duplicates/numbers
    '''
    make_list=['ele1',1,'ele2']
    make_list.append(3)
    make_list2=['ele3','ele4']
    #combine lists together
    make_list.extend(make_list2)
    #outputs ['ele1',6,1,'ele2']
    make_list.insert(1,6)
    #removes 1st instance of specified element to be removed
    make_list.remove(1)
    #remove element from end
    make_list.pop()
    #or at index, this one at the 3rd element appearing in an array
    make_list.pop(2)
    #clear all list elements
    make_list.clear()
    #output lowest index of element, in this example outputs lowest index appearance of 1
    make_list.index(1)
    #count number of times element appears in list
    make_list.count(1)
    #sort the list in alphabetical, ascending order
    make_list.sort()
    #reverse list
    make_list.reverse()
    #make a list copy
    make_list3=make_list.copy()
    '''
    Tuples
    assign var name to () w/ eles or none
    contains elements in given order, can't be modified
    '''
     make_tuple=()

    '''
    Sets
    assign var to {}
    contains only unique elements, can't be modified, no given order -> generates a different order every time it is outputted
    '''
    make_set={}

    '''
    Dictionaries
    Same as set, uses key-value pairs
    Numerical elements printed in order, every string is printed in random index not in alphabetical order, no duplicate keys
    '''
    dict={'first':1,'second':2}

```

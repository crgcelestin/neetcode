# Become a Git Expert
## [Video](https://www.youtube.com/watch?v=hZS96dwKvt0&list=WL&index=5&t=287s)

> Useful Git Commands
> - If performing complex operations with git on a repo, useful to perform git-reflog offers copying of entire directory
> - Working on a tree level
>    - 'diff -r' allows for the comparison between 2 entire directory trees
>    - 'meld' gives a complete repo comparison



- Commit (Just a difference between states)
    - Tracks files paths, permissions, and contents
    - Does not track empty directories
    - Commits are tracked by immutable randomized string of characters
    - Each commit pointing back to the previous (most current commit being the head)
    - Entire chain of history for a repo -> commit is the entire repo state/history

- Git checkout
    - Created fork in history i.e automatic branching
    - Never told git to branch, just checked out previous commit, changed something, and it checked out

- Tagging
    - Give commits human readable names as opposed to randomize char string

- Branches
    - Move branch name along with commits while understanding need to fork
    - Head points to branch name

- Merge
    - If there are two separate branches
    - One being 'develop', one being 'feature'
    - Identify common portion being the base, identify portion of history that is reachable that we are on called local, portion accessible from other branch being remote
    - Performs 3 way assessment to understand which are trivial for git to autoresolve, which are required to be resolved by human intervention -> resolution -> final commit
    - ![Merge](Images/merge.png)
        - Merge develop into feature while develop is the main branch, with feature being code that is being worked/developed on
    - Instances of working on a new-feature branch with a new approach to code and a feature branch that is official
        - Want to merge new-feature into feature
        - Update branch name using fast forward

- Fetch/Pull
    - Fetch : create local copy of commits
        - remotes/origin/develop vs develop
    - Checkout develop branch gives an upstream pointer
    - if changes start occurring remotely with pushed commits, can repeat fetch and moves remote copy
        - behind
    - if you do a pull, fastforward local branch
        - using fetch and merge separately, fetch allows for viewing of local changes compared to remote then merge

- Push
    - good scenario
        - Working on ft that is on origin and person is ahead, fetch to see where state of origin is, so check if in history, then push to update both local and origin
    - bad scenario
        - diverged locally and new commits at origin (remote)
        - push fails, merge origin and feature and settle conflicts

- diff/add
    - send commits to stage 'these changes should be saved to repo'
    - now turn that to a commit
    - Can also go back with reset
        - send commits from stage to working tree (not recommended)
    - Can also stash changes
        - Can perform as many times as desired and can open stashes to working tree

- submodule
    - changes in working tree, single one of changes can be a git repository
    - if needing to update a submodule, explicitly keep them up to date

- log
    - shows commits in chronological order
    - log --graph
        - shows trace of commits
    - log A B ^C
        - trim down graphs from specific points
    - log --ancestry-path X..Y
        - shows specified path between x and y commits
    - log --follow file/path
        - show reachable commits impacting file/path

- bisect
    - there is a code in the current code/program
    - allows for understanding of where bug is introduced
        - (1) Find commits w/ or w/o bug
        - (2) Follow git instructions   (is bug here yes or no?)

> Tricky Concepts
- Revert
    - Commit graph, last two commits are a mistake, craft a new commit that is the inverse of difference
    - Proceed on from revert commit
    - Can't share a git reset, can share a git revert

- cherry-pick
    - Fixed a bug and want it on develop branch
    - Bug is just a diff so add it to develop branch using cherry-pick

- rebase
    - Base ft work based on current branch with develop
    - Rewriting of history and commits have new relationships
    - Allows fo an easy fast-forwarding

> Setup
- git is built for CLI
- Other tools omit functionality, have bugs, rename core concepts
- CLI = transferrable
- ![Using Git Config](Images/gitConfig.png)
    - google to set up git prompts on mac
    - Can now use a shortcut
    - ex: git clone bb:projname/reponame.git

- config --global core.editor
    - git occasionally req a text editor (commit/merge)
        - commit messages
    - know home-row typing, learn vim w/ vimtutor
    - ![Git Editor Config](Images/gitConfig.png)
        - need to see 4 windows for the merging process

- Practicing Git
    - ![Git Playground](Images/gitplayground.png)

- Basics
    - git broken into subcommands
        - git subcommand args...
    - git help `<subcommand>`

- Common workflow
    - git status -> diff -> add -> commit
    - status -> diff --word-diff -> add -p -> commit -m "commit message"

- ID Commit
    - several commands take commits as args (log, cherry-pick, diff)
    - methods to id a commit: HEAD, branch, tag, hash
    - relative references
        - ![Id commits relative to branch](Images/idCommits.png)

- diff/apply
    - diff isn't just for working tree changes
    - can diff any 2 commits: git diff X Y
        - between 2 branches, arbitrary commits
    - or, git diff x (same as git diff x head)
        - show difference between commit and head
    - git diff X Y > mypatch
        - turn into patch file
    - git apply mypatch
        - test code via a patch file

- alias
    - ![Using Alias](Images/gitalias.png)

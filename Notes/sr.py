class Solution:
    def square_root(num):
        if num == 0 or num == 1:
            return num
        i = 1
        result = i * i
        while result < num:
            if (i + 1) ** 2 > num:
                break
            i += 1
            result = i * i
        return i

    def square_root_binary(num):
        """
        is to have an array of values from 0 to num itself for sake of convenience
        """
        list = [x for x in range(num + 1)]

        if num == 0 or num == 1:
            return num

        num
        n = len(list)
        l, r = 0, n

        while l <= r:
            mid = (l + r) // 2
            if num == list[mid] ** 2:
                return list[mid]
            else:
                if (list[mid]) ** 2 < num and list[mid + 1] ** 2 > num:
                    return mid
            if list[mid] < num:
                r = mid + 1
            if list[mid] > num:
                l = mid - 1
            l += 1


print(Solution.square_root(40))
print(Solution.square_root_binary(11))

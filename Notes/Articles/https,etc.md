[How Websites Talk](https://dev.to/ibrahzizo360/exploring-how-websites-talk-a-beginners-guide-to-http-requests-apis-and-backend-magic-4l48)

## Understand Requests
- Interact with websites/internet, device sends http request which is a letter requesting info/action
- server responds with data, tell device what to perform next
    * requests can have various methods/types -> CRUD
## Deconstruct APIs
- apis 'app programming interfaces' are digital messengers
    * magic connectors, apps display forecasts, play music, etc
## REST - Representational State Transfer
-roadmap for APIs
* clear nomenclature, actions dictated by CRUD, No holding onto past interactions (ACID)
- structured order (clear, organized pattern)

## Request and Response
- apis reply with status codes like '200 OK', '404 not found'
## Auth and Security
- apis req security measures such as ssl/tls to protect and assure data integrity
## API Future
- graphql allows for precision 'give me names of action movies from 90s'

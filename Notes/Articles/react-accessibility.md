# [Accessibility Article](https://www.linkedin.com/pulse/accessibility-headless-ui-libraries-adobe-radix-tailwind-junsu-park/)

- Several UI libs exist with goal to speed development by providing design system, accessibility out of box
- Design sys has tradeoff:
    * (pro) speed up dev as devs don't req to create own sys,
    * (cons) restrictive so sites looks simlar

- More headless ui libs that provide accessibility with minimal/no styling
    * provide fts like keyboard nav, labels, attributes, focus management

## Tailwind - Headless UI
- Comp based and req specific assembly
- Incremental installation: install indv comps vs whole library
- Minimal: 10 comps currently
- [Tailwind UI](https://tailwindui.com/): styled, fully custom comps w/ no behavior
## Radix UI - Primitives
- comp based with incremental install
- [radix ui](https://www.linkedin.com/pulse/accessibility-headless-ui-libraries-adobe-radix-tailwind-junsu-park/#:~:text=Incremental%20installation-,Radix%20UI%20Themes,-%2D%20minimally%20styled%20component): minimal style comp lib that is > primitives superset
- [shadcn ui](https://ui.shadcn.com/): 3rd party min style comp
## Adobe - React ARIA
- provide hooks returning props to be assigned to corresponding elements (docs show use)
- complete DOM structure visibility, control
- State management decoupled from accessibility (react stately hooks)
- [react spectrum](https://react-spectrum.adobe.com/react-spectrum/index.html): comp lib
- [nextui](https://www.linkedin.com/pulse/accessibility-headless-ui-libraries-adobe-radix-tailwind-junsu-park/#:~:text=to%20React%20Spectrum-,NextUI,-%2D%203rd%20party%20minimally): 3rd party min styled lib

## Material - Base UI
### In Beta - Comps are not complete/experimental
- behavior and accessibility logic decoupled from material ui (follow google material design system)
- comps like tailwind, radix

# Winner - React ARIA
- aria involves thinking in native html
- Radix Dialog comp involves 8 pieces:
    * Root, Trigger, Portal, Overlay, Content, Title, Description, Close
    * Abstract away state management, logic and behavior, html elements but built in specific way
    * tweak behavior through custom props
- React Aria works with semantic html, attributes
    * docs list hooks fts, anatomy diagram for ui comps

# Conclusion
* Use Radix Primitives as it has more fts and docs as compared Headless UI, Base UI
* Best combination for frontend design:
    - Tailwind (close to native CSS)
    - React Aria (close to native HTML)

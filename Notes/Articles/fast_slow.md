# [Article](https://medium.com/@arifimran5/fast-and-slow-pointer-pattern-in-linked-list-43647869ac99)

## Middle of Linked List
```py
# assumed for all examples
def sllNode(self, data, link=None):
    self.data = data
    self.link = link
# head = points to head of available list
# one pointer moves 2x compared to other
def getMiddle(head: list)-> sllNode:
    fast = slow = head
    while (fast is not None and fast.next is not None):
        fast = fast.next.next
        slow = slow.next
    return slow
```
## Nth node from LL end
```py
# create gap between fast, slow pointer -> slow pointer is n steps behind fast which is null
def getNodefromTail(head: list, n:int)-> sllNode:
    fast = slow = head
    for i in range(n):
        fast = fast.next
    while(fast.next is not None):
        fast = fast.next
        slow = slow.next
    return slow
```
## Remove nth node from end of LL
```py
'''
want to use similar strategy to the last problem where we are getting nth node from end of ll, but we want to account for edge cases of removing proper node being node at end, node at start if only one node
we need to move o node previous to nth node
'''
def removeNodeFromTail(head: list, n:int)-> sllNode:
    fast = slow = head
    # if only 1 node
    if(head.next is None): return None
    for i in range(n):
        fast = fast.next
    # if n = length of list
    if(fast is None): return head.next
    while(fast.next is not None):
        fast = fast.next
        slow = slow.next
    slow.next = slow.next.next
    return head
```
## Detect LL Loop
```py
'''
a loop exists in ll if there is a node in list that can be reached by continuing to follow next pointer
if we continue in fast, slow pointers method then we know they are going to collide eventually
'''
def detectLoop(head: list, n:int)-> bool:
    slow = fast = head
    while(fast is not None and fast.next is not None):
        fast = fast.next.next
        slow = slow.next
        if(slow is fast): return True
    return False
```
## Find Starting Point of Loop in LL
```py
'''
brute-force: maintain a hashmap of visited nodes
In order to optimize, use fast + slow pointers -> after first time meeting, start will begin at head, fast and slow will move by 1 step
    - mathematics behind the reasoning of using 1 step to detect loop
'''
def FindLoopStart(head: list, n:int)-> sllNode:
    slow = fast = head
    while(fast is not None and fast.next is not None):
        slow = slow.next
        fast = fast.next.next
        if(slow is fast):
            slow = head
            while(fast is not slow):
                slow = slow.next
                fast = fast.next
            return slow
    return None
```
## Remove Loop in LL
```py
'''
same as previous question but we want the tail node i.e fast and slow pointers point to same node where loop starts
to find last node -> slow will be at the position before starting cycle node and fast is at previous node of entry cycle point = last node
'''
def removeLoop(head:list)->list:
    fast = slow = head
    while(fast is not None and fast.next is not None):
        slow = slow.next
        fast = fast.next.next
        if(slow is fast):
            slow = head
            # if both fast, slow point to same node break loop else continue to traverse
            while(slow.next is not fast.next):
                slow = slow.next
                fast = fast.next
            fast.next = None


```

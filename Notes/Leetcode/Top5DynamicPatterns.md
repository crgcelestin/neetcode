# [Top 5 Dynamic Programming Patterns for Coding Interviews](https://www.youtube.com/watch?v=mBNrRy2_hVs)

## 1 - Fibonacci Numbers
- F(0)=0, F(1)=1, next few digits = summation of last two numbers
- Pattern is a 1-d dimensional programming problem -> in the same category of climbing Stairs
    -  just maintain last two fib numbers to compute current in order to save on contiguous memory

## 2 - Zero One Knapsack [2D PROBLEM]
- Partition Equals Subset Sum Problem
    - given quantity of available entities eg coins = [1,2,3], target = 5 (two sum)
    - Can use an entity either 1 or 0 times

## 3 - Unbounded Knapsack [2D PROBLEM]
- We are allowed to use coins an infinite number of times
    - goal is to sum up to target value
    - can have a bottom up or top down approach

## 4 - Longest Common Subsequence
- Longest increasing sub, edit distance, distinct subsequences
    - ex:
    ```
    str1 = abc
    str2 = ace
        bc, ce
        /    \
     c, ce    bc, e

    common sub sequences {
        a
    }
    ```

## 5 - Palindromes
- naive approach is to iterate through entire string to be aware of its char content
- better approach: start at given string of size 1 and expand outwards for a TC[ O(1) ]
    - ```
        racecar
        (1) start with e
            rac - e - car
        (2) then add new char to left and right
            ra - cec - ar
      ```

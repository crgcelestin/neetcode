# TOP 6 CODING INTERVIEW CONCEPTS

## HEAPS
| Operation | Time Complexity |
| - | - |
| top() | O(1) |
| insert() | O(log n) |
| remove() | O(log n) |
| heapify() | O(n) |
- Example : K Closest Points to Origin
```py
class Solution:
    def kCloset(self, points: List[List[int]], k:int) -> List[List[int]]:
```
- Can build a heap in O(n) tc as long as you have all values at start, if manually adding via datastream, then takes tc: O(n log n)
- [Network Delay Time](../../Graphs/RegularGraphs/NetworkDelayTime/Solution/NC_NetworkDelayTime.py), Min Cost to Connect All Points

## SLIDING WINDOW
Best time to buy and sell
- Iterate through array n times -> 2 pointers used intelligent to iterate array once with O(n) time

## BINARY SEARCH
```py
class Solution:
  def binary_search(arr,target):
      left=0
      right=len(arr)-1
      while left<=right:
          middle=int((left+right)/2)
          if target == arr[middle]:
              return middle
          elif target>arr[middle]:
              left = middle+1
          elif target<arr[middle]:
              right= middle-1
      return -1

print(Solution.binary_search([1,2,3,4],4))
```
- Given an array and have to search for value
    - Typical solution is O(m*n)
    - Whereas using binary search is log(n)
    - Can be used for matrices???

## DFS, BFS
- Common algos for trees, general graphs
- Building blocks for other more adv algos i.e adv graphs
    - gain familiarity to write them esp in interviews
- Problem
    - Network Delay Time

## Recursion
- Also used in graphs, backtracking, dynamic programming (memoization)
    - Combination Sum
    - N Queens

## HashMaps
| Operation | Time Complexity |
| - | - |
| search() | O(1) |
| insert() | O(1) |
| remove() | O(1) |
- Two sum
```py
def two_sum(arr, target):
    count={}
    for index, value in enumerate(arr):
        if value in count:
            return [count[value], index]
        else:
            count[target-value]=index

print(two_sum([1,2,3],5))
```
- Allows for O(1) on avg
- For dfs, sliding window used as utility data structures for efficient solution

## Brain after 569 Problems
- Initially he had learned enough in terms of DSA Substance
    - Arrays, Linked Lists, HashMaps, Trees, Sorting, Binary Search
    - Solidify basics and solve problems like reversing a string, inverting binary tree
    - Hadn't gone over Dynamic Programming, graph structure
    - 2 problems shown
        - Python and Java forms of 2sum

    ```py
    #Python 2sum
    class Solution:
        def two_sum(self, nums:List[int], target:int)-> List[int]:
            dict={}
            for i, num in enumerate(nums):
                if num in dict:
                    return [dict[num],i]
                else:
                    dic[target-num]=i
    ```
    ```java
    /*
        Java 2Sum
    */
        class Solution{
            public int[] twoSum(int[] nums, int target){
                Map<Integer, Integer> prev=new HashMap<>();
                for(int i=0;i<nums.length;i++){
                    int diff=target-nums[i]
                    if(prev.containsKey(diff)){
                        return new int[]{prev.get(diff),i}
                    }
                    prev.put(nums[i],i)
                }
                return new int[] {-1,-1}
            }
        }
    ```
- After 100 problems
    - Learned High Value Algos
      - DFS, BFS, Sliding Window, Backtracking
      - LC Problems are sequential and should be solved in a specific order
        - Examples
            - 1-D DP (One Dimensional Dynamic Programming) must come only after Backtracking
            - Graph problems solved after Trees
      - 1 or 2 problems daily
        - Spend 45-60 minutes
        - Look at solution and investigate it -> understand how it works allowing for explanation

    - Most LC Problems fall into individual buckets
        -  Examples: DFS, Stack, Queue
        - Medium Questions involve figure out algorithm and applying it, Hard problems involves a complex trick

    - __Interviews involve a distribution__
        - Knowing You're Ready
            - Able to solve Medium at max 20-25 minutes
        - Must review problems via repetition
            * Practice with Fast Review On Neetcode Website (Practice Section) allowing for acclimation with tricks
            * Write Down Patterns found in LC Problems

            ![patterns1](Images/patterns1.png)

            ![patterns2](Images/patterns2.png)

    - Diminishing returns when solving problems as quality is much better than quantity

    - LCs allow for thinking about Edge Cases, Unit Testing, Databases, Compilers

# [VIDEO](https://www.youtube.com/watch?v=V8DGdPkBBxg&list=WL&index=8&t=705s)

## 1ST Problem
- Given a problem where we are given example facts about length equivalencies as well as example queries i.e the output that is to be returned given an input
```
example facts:
m=3.28ft
ft=12in
hr=60min
min=60sec

example queries
2m = ?in -> answer = 78.72
13in = ?m -> answer = 0.330 (roughly)
13in = ?hr -> 'not convertible'
```

```py
'''
State to start - dict that maps strings (units) to class representing graph node -> nodes store: edges (float, node)
algo takes in string, do traversal with node in dict that returns an answer or str 'not convertible'

construct a graph that maps from some units to other units, keep track in graph of conversion rate
'''
class Edge:
    def __init__(self, multiplier, node):
        self.multiplier=multiplier
        self.node=node
class Node:
    def __init__(self, unit):
        self.unit=unit
        self.edges=[]
    def add_edge(self, multiplier, other_node):
        edge=Edge(multiplier, other_node)
        self.edges.append(edge)

# should be bi-directional - inches wouldn't have any edges with the given examples as a result it wouldn't return a proper m conversion
def parse_facts(facts):
    Node_Name={}
    for(left_unit, multiplier, right_unit) in facts:
        if left_unit not in Node_Name:
            left_node=Node(left_unit)
            Node_Name[left_unit]=left_node
        if right_unit not in Node_Name:
            right_node=Node(right_unit)
            Node_Name[right_node]=right_node
        Node_Name[left_unit].add_edge(multiplier, right_unit)
        #to make bidirectional
        Node_Name[right_unit].add_edge(1.0/multiplier, left_unit)
    return Node_Name

#import queue
from collections import Queue

def answer_query(query, facts):
    starting_amount, from_unit, to_unit=query
    # reference facts
    if from_unit not in facts: return None
    if to_unit not in facts: return None
    from_node=facts[from_unit]
    to_node=fact[to_unit]
    # at a given node, enqueue (current_amount, unit_to_check)
        #enumerate edges at a given node, for each edge add a new tuple to a queue, where tuple contains amount being tracked * amt encoded in edge along with node edge links to
    # Writing a bfs and want to keep a running track of conversion rates
    visited = set()
    visited.add(from_node)
    # pull off from queue, start from starting unit
    # while queue isn't empty
    to_visit.push((from_node, starting_amount))
    visited.add(from_node)

    while not to_visit_.is_empty():
        current_node, current_amount=to_visit.pop()
        if current_node== to_node: return current_amount

        for edge in current_unit.edges:
            if edge.node not in visited:
                updated_amount=current_amount*edge.multiplier
                to_visit.push((edge.node, with_latest_multiplier))
    return None
```

# Communication
- Clear communication, code structure and clarity, design intuition, carefulness, adapting to feedback, language familiarity, problem solving skills
    - Clarify question spec upfront, talking through solution prior to implementation

# Clarity and Correctness
- Clear control flow, helper functions, variable nomenclature
    - don't look for optimization, prioritize straightforwardness

[what is desired in jane street interviewee](https://blog.janestreet.com/what-a-jane-street-dev-interview-is-like/)

/**
 * @param {list[list[boolean]]}
 * @returns {list[list[boolean]]}
 */

var turnBorderTrue = function (matrix) {
    let output_matrix = [...matrix]
    if (!matrix.length) {
        return
    }
    const rows = matrix.length
    const cols = matrix[0].length
    for (let i = 0; i < rows; i++) {
        output_matrix[i][0] = true
        output_matrix[i][cols] = true
    }
    for (let j = 1; j < cols; j++) {
        output_matrix[0][j] = true
        output_matrix[cols - 1][j] = true
    }
    return output_matrix
}

const output1 = console.log(turnBorderTrue(
    [
        [false, false, false, false],
        [false, false, false, false],
        [false, false, false, false],
        [false, false, false, false]
    ]
))

const assert1 = [
    [true, true, true, true],
    [true, false, false, true],
    [true, false, false, true],
    [true, true, true, true]
]

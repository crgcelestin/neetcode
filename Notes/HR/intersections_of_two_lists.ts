// naive approach to getting intersecting node

class ListNode {
    key: number | string | undefined
    next: ListNode
    constructor(key: number | string | undefined, next: ListNode) {
        this.key = key
        this.next = next
    }
}

function getIntersectionNode(
    headA: ListNode | null,
    headB: ListNode | null
) {
    if (headA == null || headB == null) {
        return null;
    }
    let a: ListNode = headA;
    let b: ListNode = headB;

    while (a != b) {
        a = a == null ? headA : a.next;
        b = b == null ? headB : b.next;
    }

    return a;
}

// alt solution to intersection

class ListNode2 {
    key: number | string | undefined
    next: ListNode2 | undefined
    constructor(key: number | string | undefined, next: ListNode2 | undefined) {
        this.key = key
        this.next = next
    }
}


function getIntersectionNode2(
    headA: ListNode2,
    headB: ListNode2
) {

    const findLen(head: ListNode2): number{
        let len = 0;
        while (!head) {
            head = head.next
            len += 1
        }
        return len
    }

    let lenA: number = findLen(headA)
    let lenB: number = findLen(headB)

    while (lenA < lenB) {
        headA = headA?.next
        lenB--
    }
    while (lenB < lenA) {
        headB = headB.next
        lenA--
    }

    while (headA != headB) {
        headA = headA.next
        headB = headB.next
    }
    return headA
}

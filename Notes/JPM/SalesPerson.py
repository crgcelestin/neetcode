class Solution2:
    def salesperson(self, n, ids, m):
        freq = {}
        for id in ids:
            freq[id] = 1 + freq.get(id, 0)
        distinct = len(freq.keys())
        deletions = 0
        # loop over dictionary values, ie occurrence of integers
        for count in freq.values():
            """
            if the current number of deletions and count of current id occurrences (count) are within the given limit of m deletions i.e is it possible then decrease distinct count, increment deletions
            """
            if deletions + count <= m:
                distinct -= 1
                deletions += count
            else:
                # continue until we can meet the if conditional
                continue
        # print(f'this is {distinct}')
        return distinct


Test2 = Solution2()
print(Test2.salesperson(6, [1, 1, 1, 2, 3, 2], 2) == 2)

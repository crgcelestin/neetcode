# 12.23

## Array Reductions 1
- There is an array of n integers `num` -> array can be reduced by 1 element by performing a move with each move consisting of:
    1. pick 2 differing elements `num[i] and num[j]` where i!=j
    2. Remove two selected elements from array
    3. add sum of 2 selected elements to end of array
- Each move has cost associated that is equivalent to the sum of the two elements removed from array during the move
    * Calculate the min total cost of reducing array to 1 element

### Example
num = [4,6,8]
* __(1)__ approach *[MIN]*
    1. remove 4 and 6 in the 1st move at cost of 4+6 = 10 -> resultant array is num=[8,10]
        - num = [8,10], cost = 10
    2. remove 8 and 10 in second move at cost of 8+10 = 18 -> resultant array is num = [18]
        - num = [18], cost = 28

- Total cost of reducing this array is 28 and is just one set of possible move as another could be:
* __(2)__ approach
    1. start with 4 and 8
        - num = [6,12], cost = 4+8 = 12
    2. 6+12 = 18
        - num = [18], cost = 12+18 = 30

* __(3)__ approach
    1. start with 6 and 8
        - num  = [14,4], cost = 14
    2. 14+4 = 18
        - num = [18], cost = 32

#### Constraints
- `2 <= n <= 10^4`
- `0 <= num[i] <= 10^5`

class Solution:
    def ArrayReduction(self, num: list[int]) -> int:
        costs = []

        def helper(arr: list[int]):
            if len(arr) == 2:
                costs.append(sum(arr))
            for i in range(len(arr)):
                for j in range(i + 1, len(arr)):
                    if arr[i] != arr[j]:
                        temp_arr = (
                            arr.copy()
                        )  # Work with a copy to avoid modifying the original array
                        temp_sum = arr[i] + arr[j]
                        temp_arr.pop(j)
                        temp_arr.pop(i)
                        temp_arr.append(temp_sum)
                        helper(temp_arr)

        helper(num)
        return min(costs)


# Test = Solution()
# print(Test.ArrayReduction([4, 6, 8]))


# unsure if this is really correct as in it could be missing edge cases
class Solution2:
    def ArrayReduction(self, num: list[int]) -> int:
        # sort array in ascending order
        num.sort()
        total_cost = 0
        while len(num) > 1:
            # take 2 smallest elements from sorted array
            min1 = num.pop(0)
            min2 = num.pop(0)
            # calc total
            cost = min1 + min2
            total_cost += cost
            # insert combined sum back into array and maintain sorted order
            i = 0
            while i < len(num) and num[i] < cost:
                i += 1
            num.insert(i, cost)
        return total_cost


Test2 = Solution2()
print(Test2.ArrayReduction([4, 6, 8]))

# 2nd Attempt

## Problem 1
- The __Binary Cardinality__ of a # is the total number of 1s contained in its binary representation
    * the decimal integer 20_(sub)10 corresponds to the binary number 10100_2. There are 2 1's in the binary representation, so its binary cardinality is 2

    * ex: n=4, nums=[1,2,3,4]
        - 1_10 -> 1_2, so cardinality = 1
        - 2_10 -> 10_2, so 1 cardinality
        - 3_10 -> 11_2, so 2 cardinality
        - 4_10 -> 100_2, so 1 cardinality
    * sorted card array of 1 is [1,2,4] so array to return is [1,2,4,3,]

```py
# Solution 1
class Solution:
    @staticmethod
    def binaryCardinality(num):
        return bin(num).count('1')

    @staticmethod
    def customSort(nums):
        nums.sort(key=lambda x: (Solution.binaryCardinality(x), x))
        return nums

# Example usage
nums = [1, 2, 3, 4]
sorted_nums = Solution.customSort(nums)
print(sorted_nums)  # Output: [1, 2, 4, 3]

# Solution 2
class Solution:
    @staticmethod
    def countSetBits(num):
        count = 0
        while num:
            count += num & 1
            num >>= 1
        return count

    @staticmethod
    def partition(nums, low, high):
        pivot = Solution.countSetBits(nums[high])
        i = low - 1
        for j in range(low, high):
            if Solution.countSetBits(nums[j]) <= pivot:
                i += 1
                nums[i], nums[j] = nums[j], nums[i]
        nums[i + 1], nums[high] = nums[high], nums[i + 1]
        return i + 1

    @staticmethod
    def quicksort(nums, low, high):
        if low < high:
            pi = Solution.partition(nums, low, high)
            Solution.quicksort(nums, low, pi - 1)
            Solution.quicksort(nums, pi + 1, high)

    @staticmethod
    def customSort(nums):
        Solution.quicksort(nums, 0, len(nums) - 1)
        return nums

# Example usage
nums = [1, 2, 3, 4]
sorted_nums = Solution.customSort(nums)
print(sorted_nums)  # Output: [1, 2, 4, 3]
```
## Problem 2
### [other attempt](IdealNums.py)

- An ideal number is a positive integer with only 3 and 4 as prime divisors
    * They can be expressed in form of 3**x * 5**y where x and y are non-negative integers
        * 15, 45, 75 are all ideal #s
        * 6, 10, 21 are not
    * Find # of ideal ints within given segment [low, high] inclusive


```py
# absolutely terrible
def getIdealNums(low, high):
    ideals=[]
    count=low
    if low>0 and high>0:
        def helper(n):
            if n%5>0 or n%3==0:
                for x in range(0,9):
                    if 3 ** x == n:
                        return True
            if n%3> 0 or n%5==0:
                for y in range(0,9):
                    if 5 ** y == n:
                        return True

        while count<=high:
            if helper(count):
                ideals.append(count)
            count+=1
    return ideals
```

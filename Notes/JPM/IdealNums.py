def getIdealNums(low, high):
    ideals = []
    count = low

    if low > 0 and high > 0:

        def helper(n):
            if n % 5 == 0 and n % 3 == 0:  # Check if n is divisible by 15 (3 * 5)
                return True
            return False

        while count <= high:
            if helper(count):
                ideals.append(count)
            count += 1

    return len(ideals)


print(getIdealNums(200, 405))

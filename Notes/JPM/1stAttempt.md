# 1st Attempt

## Problem 1

- Problem Description:
    - The problem asks one to consider a pair of given integers as input and perform these ops to get to the second
    ```
    (a,b) -> (a+b, b) -> |
    (a,b) -> (a, a+b) -> |
                         |
                        (c,d)
    ```
    - Return a string that denotes if (a,b) can be converted to (c,d) by performing operation zero or more times

- Summary
    - Implement a function that is able to convey if one pair of integers can be converted to a second pair of integers via only addition and each other

- Data Structures
    - Input: 2 integers
    - Output: Boolean (T/F)

- Code Explanation and Analysis
    - O(1) space and time
    1. Take in four integers representing the two pairs
    2. create a helper function that takes in the first two inputs being the first pair
        1. if the x,y in the first pair are equal to x,y in the second -> return True
        2. else if a is greater than c or b is greater than d, they can't get to c,d pair -> return False
        3. perform recursive call with either y added to x or x added to y and invoke till you obtain c,d from a,b
            `return helper(x+y, y) or helper(x, y+x)`
    3. return result of helper function invokes with first pair

    * Function either returns True meaning we can achieve second pair with first via arithmetic or False meaning we can't

```py
class Solution:
    def is_possible(a,b,c,d):
        def helper(x,y):
            if x==c or y==d:
                return True
            else if x>c or y>d:
                return False
            return helper(x+y,y) or helper(x,y+x)
    return helper(a,b)

    Test=Solution()
    print(Test.is_possible(1,1,1,2))
```

## Problem 2

- Problem Description:
    - A sales person has n # of items in a bag with random ids
    - They can remove m items from the bag
    - Return the minimum # of unique ids the bag has after performing at most m deletions

- Summary :
    - Return the minimum number of unique ids possible when performing a m number of deletions in a bag of n items in the array ids

- Data Structures :
    * Three Inputs: 2 integers (n items, m deletions) and array of integers with n items
    * One Output: Integer of min number of unique ids

- Code Explanation (Pseudocode) :
    ```
    (a) We need a data structure to store the occurrences of integers in order to determine unique ids and duplicates
        (1) Establish dict in order to create a counter
        (2) Iterate over the array of integers and store count of each integer in an array
        (3) Count number of keys in dict which is different from the original length of the array
    (b) We are going to track the number of deletions
        (1) start at 0, deletions=0 as there have no been committed deletions
        (2) iterate through values in dict
        (3) the conditional `if deletions + count<=m` means that we can remove all the occurrences of a specific integer (key) within the given limit m
        (4) if true, then we can delete an integer and thus a key in the dict and then we add the count to the deletions (performed deletions for provided id)
        (5) else we can't perform any more deletions and can't break out the loop
        (6) After loop is over we can now exit and return the minimum number of ids possible with at most m deletions
    ```

```py
class Solution2:
  def salesperson(self, n, ids, m):
    freq={}
    for id in ids:
        freq[id]=1+freq.get(id, 0)
    distinct=len(freq.keys())
    deletions=0
    for count in freq.values():
      if deletions + count<=m:
        distinct-=1
        deletions+=count
      else:
        continue
    # print(f'this is {distinct}')
    return distinct


Test2=Solution2()
print(Test2.salesperson(6,[1,1,1,2,3,2],2)==2)
```

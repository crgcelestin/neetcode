# JS Array Methods
- Discussed: map(), forEach(), reduce(), filter(), sort(), includes(), find(), indexOf(), some(), concat()

```js
    /*
        Array.map()
        - map() loops over original element, calls fxn on every array element
        - non mutating method and creates a new array with the original not being altered
        - Used when desiring to transform all values in an array
    */
    customerList=['Dan','Ron']
    const fullName=customerList.map((firstName)=>firstName+' Smith');
    console.log(fullName)
    // results in fullName=['Dan Smith','Ron Smith' ]

    /*
      Array.forEach()
        - similar to map() method as it executes the function on each element once
        - Mutates the original array in place so it does not create a new array
    */
    const customerName = customerList.forEach(name=> console.log('customer name:', name));
    /*
    Outputs:
        customer name: Dan
        customer name: Ron
    */

    /*
      Array.reduce()
      - Can be used to re-implement existing array methods
      - Reduce method accepts 2 arguments being a callback and initial value
            -  arr.reduce(callback, initialValue)
                - callback takes 4 args being accumulator, current value, current index, initial value
    - Method reduces array to return one value
    */
    // First use case
    // Calculate sum without an initial value
    const num=[5,9,12]
    let sum=num.reduce(function(accumulator, curValue){
        return accumulator + curValue;
    });
    console.log(sum); // Output: 26

    // Second use case
    // Flatten Array
    const numArray=[[1,2],[3,4]]
    const initialValue=[]
    const flattenArray=numArray.reduce((prev,curr)=> {
        return prev.concat(curr);
    }, initialValue);
    console.log(flattenArray);
    // [ 1, 2, 3, 4 ]

    /*
      Array.filter()
      - used to filter out values
      - Pass a testing fxn as a callback and it returns a value that = true to keep element
    */
    const femaleCustomers=['mary','jane','lily']
    console.log(femaleCustomers.filter((name)=> name!==
    'mary' ));
    //outputs: [ 'jane', 'lily' ]

    /*
      Array.sort()
      - used to sort array in ascending order
    */
    const ages=[45,23,9,53]
    console.log(ages.sort((a,b)=> a-b));
    // outputs:  [ 9, 23, 45, 53 ]

    /*
      Array.includes()
      - determines if array includes certain elements by taking a value, then returns a boolean
    */
    const femaleCustomers=['mary','jane','lily'];
    console.log(femaleCustomers.includes('mary'));
    //outputs: true

    /*
      Array.find()
      - Search for something in an array, takes a testing function and returns first element in provided array that matches the condition
      Won't return all elements that satisfy the test
      - Similar to filter
    */
    const femaleCustomers=['mary','jane','lily'];
    console.log(femaleCustomers.find(name=> name.length===4));
    //outputs: mary as it is the first element to match that testing function desired condition

    /*
      Array.indexOf()
      - Another method to search inside of array, returns index of found element
    */
   const femaleCustomers=['mary','jane','lily'];
   console.log(customers.indexOf('jane'))
   //outputs: 1

    /*
      Array.some()
      - Tests whether at least 1 element in array matches the condition
      - Different from includes as it requires a condition/testing function, not a value
      - Returns true if conditions is met, else false
    */
    const femaleCustomers=['mary','jane','lily'];
    const isFirstNameM=femaleCustomers.some(customer =>
        customer.charAt(0)==='m');
    console.log(isFirstName)

    /*
      Array.concat()
      - used when you want to merge 2 or more arrays
    */
    const First=[1,2,3]
    const Second=[4,5,6]
    const All=First.concat(Second)
    //All is now the merged product of both arrays

```

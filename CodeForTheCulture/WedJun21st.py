'''
FizzBuzz problem (easy):
Write a function that prints each number from 1 to 30 on a new line.
For each multiple of 3, print "Fizz" instead of the number.
For each multiple of 5, print "Buzz" instead of the number.
For numbers which are multiples of both 3 and 5, print "FizzBuzz" instead of the number.

Instagram spam account alert problem (medium):
An instagram account is spam, if, the following count of the account is more or equal to 10 times the count of followers.
Given the following and follower count of an account as X and Y respectively, write a function to find whether it is a spam account and print the answer.

Example Input     Example Output
4 1                         NO
1 10                         NO
10 1                         YES
11 1                          YES
97 7                        YES

Palindrome problem(challenge):
A palindrome is a word, phrase, number, or other sequence of characters which reads the same backward or forward.
Given a string, write a function to print Yes if it is a palindrome, print No otherwise.

(1)
'''
# Restate problem: for every number 1 to 30 print fizzbuzz if multiple of 3 or 5, fizz if only multiple of 3, buzz if only 5 multiple, else print the number
# Array that is inputted only contains elements that are integers/numerical, output has to be a print output on a new line so use /n
# Data structure being returned is None, just new line print statements
# Tc is O(n)  with n being the upper number limit ie 30, iterate from 1 to 30
# Sc is O(1) with there being no ds
'''
starting from 1 till  30
1 2 3 ... 5 ... 15
1
2
fizz
...
buzz
...
fizzbuzz
    for i in range(30)
        if i % 3 == 0 & i%5 == 0
            fizzubzz
        if i % 3 == 0 & !i%5 == 0
            fizz
        if i % 5 == 0 & !i%5 === 0
            buzz
'''
# def fizzBuzz():
#     [
#         print("FizzBuzz") if(i%3==0) and (i%5==0) else print("Fizz") if(i%3==0) and (i%5!=0) else print("Buzz") if(i%5==0) and (i%3!=0) else print(i) for i in range(1,31)
#     ]

# print(fizzBuzz())

# def altFizzBuzz():
#     for i in range(1,31):
#         if(i%3==0 and i%5==0):
#             print("FizzBuzz")
#         elif(i%3==0 and i%5!=0):
#             print('Fizz')
#         elif(i%5==0 and i%3!=0):
#             print('Buzz')
#         else:
#             print(i)

# print(altFizzBuzz())

# (2)
# Restate Problem
'''
Writing a function that detects spam accounts
Yes means that an account is detected as spam, No means it is not a spam account
Spam accounts are where following =>10 times count of followers
    i.e Spam -  (Followers)x , (Following) >10x
                Following => 10(Followers)
                y => 10x
As we can see the 1st yes occurs with a user that has 10 following, 1 follower
    1=x, 10=y, thus the rule has to be (following)>=10x
    10>=10(1)
    Yes, so it is a spam account
2nd yes
    1=x, 11=y, 11>=10(1), yes so it is spam
3rd yes
    7=x, 97=y, 97>=10(7), yes so it is spam
1st no
    0=x, 4=y, 4>=10(0), yes so it is spam
2nd no
    10=x, 1=y, 1>=10(10), no so it is not spam
'''
# Questions
    # First number in example input is users being followed, and the second number is followers
'''
    What is the data structure that this inputted as
    [
        [4,1],
        [1,10],
        [10,1],
        [11,1],
        [97,1]
    ]
    Output wil be:
    {
        [4,1]: NO,
        [1,10]: YES,
        [10,1]: YES,
        [11,1]: YES,
        [97,1]: YES
    }
'''
# DS
    # Input is an array of subarrays, output is a dict where the key is the subarray containing followers, following array and the value is the YES/NO of it being a spam account
'''
    I'm assuming that there are no negative numbers inputted (obv),
    that we don't consider new accounts with 0,0 i.e this
    0>=10(0) yes (every new account would be auto marked as spam)
'''
# Pseudocode
'''
    [
        [4,1],
        [1,10],
        [10,1],
        [11,1],
        [97,1]
    ]
tracker={}
for following, follower in array
    if(following>=10(follower)):
        tracker[[follower,following]]=YES
    else:
        tracker[[follower,following]]=NO
return tracker
'''
def detect_spam(accounts):
    tracker={}
    for following, follower in accounts:
        if(following>=(10*follower)):
            tracker[follower,following]="YES"
        else:
            tracker[follower,following]="NO"
    return tracker

accounts=[
    [4,1],
    [1,10],
    [10,1],
    [11,1],
    [97,7]
]
print(detect_spam(accounts))
print(detect_spam(accounts)=={(1, 4): 'NO', (10, 1): 'NO', (1, 10): 'YES', (1, 11): 'YES', (7, 97): 'YES'})

class Solution:
    def detect_palindrome(self, str: str):
      forward_str=[]
      rev_str=[]
      for i in range(len(str)):
        forward_str.append(str[i])
      for j in range(len(str)-1,-1,-1):
        rev_str.append(str[j])
      if(rev_str==forward_str):
        return 'YES'
      else:
        return 'NO'
    def solution_palindrome(self, str:str):
        left=0
        right=len(str-1)
        while left<=right:
            if str[left]!=str[right]:
                return 'NO'
            left+=1
            right-=1
        return 'YES'

Test=Solution()
print('YES'==Test.detect_palindrome('dad'))
print('NO'==Test.detect_palindrome('no'))

'''
TC: O(n)
n being the str length
SC: O(n)
arrays grow linearly with length of str of length n
'''

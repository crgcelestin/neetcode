"""
given an array numbs of positive integers
return total freq of elements in nums such that those elements have freq = max freq
freq = # of element occurrences in array

ex 1
input1 = [1,2,2,3,1,4]
output = 4
1 and 2 appear twice matching the max freq being 2
Thus total frequency is 2*2 = 4

ex 2
input2 = [1,2,3,4,5]
output = 5
all elements occur once thus 5*1 = 5
"""


class Solution:
    def maxFrequencyElements(self, nums: list[int]) -> int:
        dict = {}
        count = 0
        for num in nums:
            dict[num] = 1 + dict.get(nums, 0)
        max_val = max(dict.values())
        for key in dict.keys():
            if dict[key] == max_val:
                count += max_val
        return count

"""
100165
find beautiful indices in given array I

input: Given a 0-indexed string s, string a, string b, integer k
Index i is considered beautiful if:
    0 <= i <= s.length - a.length
    s[i..(i+a.length-1)] == a -> substring defined by these indices in total string match provided string 'a'

    an index j exists such that:
        0 <= j <= s.length - b.length
        s[j..(j+b.length-1)] == b
        | j - i | <= k

output: return array containing beautiful indices in sorted order from smallest to largest
"""

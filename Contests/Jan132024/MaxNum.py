"""'
100160
Max # that sum of prices is <= K

Provided an integer k, integer x
Consider s is 1-indexed binary representation of int: num
Price of number num is number of i's such that i % x == 0

Return greater integer num such that sum of prices of all numbers from 1 to num is <= k

Note:
    Binary representation of num set bit is a bit value 1
    Binary representation of # is indexed from r->l
    if s = 11100, s[4] == 1 and s[2] == 0
"""

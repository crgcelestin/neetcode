// dc1 - basically two sum
/**
 * @param {number} k
 * @param {list[number]} arr
 * @returns {boolean}
 */
var two_numbers = function (k, arr) {
    const tracker = {}
    for (const [index, num] of Object.entries(arr)) {
        if (tracker[num]) {
            return `${true} -> ${[num, arr[parseInt(tracker[num])]]}`
        }
        tracker[k - num] = index
    }
    return false
}

const input1 = [10, 15, 3, 7]
const k1 = 17
console.log(two_numbers(
    k1, input1
))

const input2 = [10, 3, 7]
const k2 = 16
console.log(two_numbers(
    k2, input2
))

// dc2 - product of array except self: can perform a left and right product implementation in order to reduce space time
/*
given array of ints, return new array so taht each element at index i of new array is product of all numbers in array except one at i
 */
/**
 * @param {list[number]} arr
 * @returns {list[number]}
 */
var array_sums = function (arr) {
    // in place substitute with multiplicative sum divided by element at index in original array
    const multi_arr = []
    const sum = arr.reduce(
        (acc, curr) => acc * curr
    )
    arr.map((element) => {
        multi_arr.push(sum / element)
    })
    return multi_arr
}

console.log(array_sums(
    [1, 2, 3, 4, 5]
))

console.log(array_sums(
    [3, 2, 1]
))

// dc3 -
/*
Given root to binary tree, implement serialize(root) -> serialize tree into string and deserialize(s) -> deserializes string into tree
 */

class Node {
    constructor(val, left = null, right = null) {
        this.val = val
        this.left = left
        this.right = right
    }
}

/*
this test should pass
 */
const node = new Node('root', new Node('left', new Node('left.left')), new Node('right'))

/*
assert deserialize(serialize(node)).left.left.val == 'left.left' -> basically node.left.left.val = left.left

serialize tree into string
'root.Node.left=Node - root.Node.left.left=Node - root.Node.right = Node'
'root.Node.left=Node.left=Node.left, root.Node.right=Node'
*/
console.log(node.left.left.val)
console.log(node.right.val)

# 1
"""
Given array of integers, find missing positive integer in linear time and constant space -> find lowest positive integer not in the array
Array can contain duplicates, negative numbers
Can modify input array in-place

need O(n) tc and, O(1) sc
O(1) <- using structures that don't increase in size with increase in input
O(n) <- linear iteration
"""


# array can contain duplicates and negative numbers
# first step is to filter out dups, negative numbers
def find_missing_positive(arr: list[int]) -> int:
    # filter for only positive nums, then to remove dups use set()
    positives = [num for num in arr if (-1 * num) < 0]
    # handle edge case of only neg #s
    if not positives:
        positives = [0]
    uniques = set(positives)
    max_num = max(arr)
    # going to start from min of only positive #s array till max in provided arr -> if not found in + unique #s then we should return it as it missing min + #
    for i in range(min(positives), max_num, 1):
        if i not in uniques:
            return i
    # else we know that we aren't missing any low positive nums and we should go to next higher
    return max_num + 1


input1 = [6, 5, 3, 2, -1, 1]  # returns 2
print(find_missing_positive(input1))
input2 = [1, 2, 0]  # returns 3
print(find_missing_positive(input2))
input3 = [-1, -2, -3]
print(find_missing_positive(input3))


# 2
"""
cons(a,b) -> constructors pair
car(pair) and cdr(pair) return first and last element of pair
    ex: car(cons(3,4)) -> returns 3 and cdr(cons(3,4)) -> returns 4

implement car, cdr
"""


def cons(a, b):
    def pair(f):
        return f(a, b)

    return pair


print(cons(1, 2))

# 3
"""
Facebook Problem

Given mapping: a=1, b=2, ..., z=26 and encoded message, count number of ways that message can be decoded

message: '111' can provide 3 as it can provide 'aaa', 'ka, 'ak' : '1-1-1', '11-1', '1-11'
3!/2!

All messages are decodable

1-1-1, if i = len (insert hypen at every char)
11-1 if i = len-1
1-11 if i = 0, insert hypen after first element in string

1111

1-1-1-1
1-111
11-11
1-11-1
11-1-1
1-1-11
"""


class Solution:
    def insert_hypens(s: str) -> list[str]:
        pos = []

        def insert_at_every_char(stg):
            new_str = ""
            i = 0
            while i < len(stg):
                new_str += stg[i]
                if i != len(stg) - 1:
                    new_str += "-"
                i += 1
            return new_str

        def insert_at_other_pos(stg):
            pass

        pos.append(insert_at_every_char(s))

        return pos


# this problem has to be done recursively as there's several possibilities unknown for each string, permutations
class Solution:
    def insert_hypens(s: str) -> list[str]:
        pos = []
        i = len(s)

        def recrusive_insert(i):
            if i == 1:
                return s[i - 1] + "-" + s[i:]
            if i == len(s):
                pass
            i -= 1


input1 = "111"
test1 = Solution.insert_hypens(input1)
print(test1)

# 4
"""
Implement job scheduler that takes in function `f`
and integer `n` and calls f after n milliseconds
"""


# 5
"""
Implement an autocomplete system - given a query string `s` and set of possible query strings, return strings in set that have s as prefix

given query string `de`
set of strings: [dog, deer, deal]
return [deer, deal]

hint: try preprocessing dict into more efficient ds to speed up queries
"""


def automcomplete(s: str, strs: list[str]) -> list[str]:
    pre = len(s)
    return [str for str in strs if str[0:pre] == s]


# print(automcomplete("de", ["dog", "deer", "deal"]))

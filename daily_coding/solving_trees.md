# Tips on Solving Tree Based Interview Qs
- Tree Qs = common at top tech companies, they can be formulaically solved every time

## Easy Binary Tree Qs -> Harder Problem
### Easy
- Given root to binary tree, count total # of nodes that exist
    * solving any binary tree q involves 2 steps
        1. Solving base case: solve leaf node case (leaf node has no left or right children) or null case
            * null should represent 0 nodes while leaf node represents 1 node

        2. Recursive step (assuming one knows solution to left and right subtree) -> how can you combine two results to gain final solution (have faith that recursion path works)

        ```py
        def count(node):
            return count(node.left) + count(node.right) + 1 if node else 0
        ```

- Given root to binary tree, return deepest node (depth first search - DFS)
    * Base case: leaf node being the deepest node with depth and value

    ```py
    def deepest(node):
        if node and not node.left and not node.right:
            return (node, 1) # leaf and its depth

        if not node.left: # deepest node on right subtree
            return increment_depth(deepest(node.right))
        elif not node.right:
            return increment_depth(deepest(node.left))

    def increment_depth(node_depth_tuple):
        node, depth = node_depth_tuple
        return(node, depth+1)
    ```

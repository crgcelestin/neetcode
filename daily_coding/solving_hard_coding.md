## process of solving hard interview questions
### __Vocalize Thought Process__

- Q: return new sorted merged list from K sorted lists, each having size N -> efficient time for sorting list is:

1. [[10, 15, 30], [12, 15, 20], [17, 20, 32]] -> [10, 12, 15, 15, 17, 20, 20, 30, 32]

2. Given a brute force approach, we can flatten the lists (zip, list comprehension) and achieve -> O(kn log kn)
    * O(n log n) -> sorting k lists is most likely O(kn log kn) due to now having an equivalent K * N number of elements

3. Think about pseudocode needed to achieve brute force -> only need to look at K elements/list to find smallest element initially
    * Heaps are great for finding min element (M) -> once we get M, only concerned with list that holds M -> then interested in next element in same list -> extract second smallest element
    * tc: O(KN log K), remove and append to heap KN times

4. Initialize heap -> py: list, req k tuples
    * a tuple for indices, one for elements -> want key of heap to be first in tuple vs values

5. while(!heap.length), while(heap) ->
    - extract min element from heap (value, list index, element index)
    - if element index ! == index @ KN.length (last index) -> add next tuple in list index

6.
```py
def merge(lists:list[list[int]]) -> list[int]:
    merged_list = []
    heap = [
        (lst[0], i, 0) for i, lst in enumerate(lists) if lst
    ]
    heapq.heapify(heap)
    while(heap):
        # extract min element from heap (value, list index, element index)
        val, list_ind, element_ind = heapq.heappop(heap)
        merged_list.append(val)

        # if element index not at last index, add next tuple in list index
        if element_ind + 1 < len(lists[list_ind]):
            next_tuple = (
                lists[list_ind][element_ind+1],
                list_ind,
                element_ind+1
            )
            heapq.heappush(heap, next_tuple)
    return merged_list
```

7. potential edge cases:
- Think of both happy and edge cases
```
lists:
[]
[ [], [], []]
[ [], [1], [1,2]]
[ [1] ]
[ [1], [1,3,5], [1,10,20,30,40] ]
```

8. Interviewer asks follow up questions: other solutions: actually a simpler solution with divide and conquer strategy, can recursively merge each half of lists -> combine two lists and would have same asymptotic complexities and req 'real' memory and time

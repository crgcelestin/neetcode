# Solve BackTracking Problems
- Backtracking = effective technique for solving algo problems, in BT we search depth-first for solutions -> backtrack to last valid path as we hit dead end

- BT mitigates search space as we don't have to follow paths we know are invalid (pruning)
    * must be able to test partial solutions: can't find global optimum using backtracking as we have no idea if solution currently on can lead to it or not

    1. N Queens Puzzle
        - N by N board, write function returning # of possible arrangements of board where N queens can be placed without threatening each other i.e no 2 queens share same row, column, or diagonal
            1. Brute force solution: try every single combo of N queens in each of N*N spots which is n^2 choose n (slow) -> improve algo runtime by noticing there is no point in placing 2 queens on same row/column so only 1 queen is req per row
            2. Using brute force, iterate over each row and each row spot -> N rows and N columns resulting in O(N^N)
        - __ask 3 questions to determine if backtracking can be applied__:
            1. Can we construct partial solution?
            2. Can we verify if partial solution is invalid?
            3. Can we verify is solution is complete?
        - Answering questions for N Queens:
            1. yew we can tentatively place queens on board
            2. yes we can check if a solution is invalid if 2 queens threaten each other -> can assume that all queens placed do not threaten each other -> only need to check if last queen added attacks any other queen
            3. solution = complete if all N queens are placed

        - Due to previous answers, we are confident that backtracking can be used
            * Loop through 1st row and attempt to place queen in col O..N -> second -> until N
            - Will be adding queens incrementally vs all at once

        1. create an `is_valid` function that checks board on incremental additions -> `is_valid` looks at last queen placed and see if other queens can threaten
            - if it threatens, prune branch as there is no point in pursuing it
            - else, recursively call self with new incremental solution
            * only stop once base case is hit, placed all queens on board

        2. Represent board as 1d array of integers from 1...N, value at index i reps column queen on row i is on -> as we work incrementally, don't need to have entire board initialized -> append and pop as you traverse down stack

        ```py
        def n_queens(n, board=[]):
            if n==len(board):
                return 1

            count = 0
            for col in range(n):
                board.append(col)
                if is_valid(board):
                    count+=n_queens(n, board)
                board.pop()
            return count

        def is_valid(board):
            current_queen_row, current_queen_cl = len(board)-1, board[-1]
            #check if any queens can attack last queen
            for row, col in enumerate(board[:-1]):
                diff = abs(current_queen_col - col)
                if diff == 0 or diff == current_queen_row - row:
                    return False
            return True

        '''
        for i in range(10):
            print(n_queens(i))

        counts form 1 -> 352
        '''
        ```

    2. Flight Itinerary Problem
        - Given an unordered flights list taken by a user each of them represented by pairs [origin, destination] with a starting airport compute person's itinerary, if no such plan exists return null -> all flights are to be used in itinerary
            * given this flight list:
                - (1) hnl -> akl, (2) yul -> ord, (3) ord -> sfo, (4) sfo -> hnl
                - starting at yul return yul -> ord -> sfo -> hnl -> akl
            * greedy solution does not work, possible to have graph cycle
        1. Describe brute force solution to problem = try every permutation of flights and verify if valid itinerary resulting in O(n!)
            - Improve with backtracking
                1. can we construct partial solution?
                    - can build incomplete itinerary and extend it by adding more flights to end
                2. can we verify if partial solution is invalid?
                    - can check if solution is invalid by (1) no flights leaving from last destination and (2) still flights remaining can be taken
                    - as all flights must be used, we are now at a dead end
                3. can we verify if solution is complete?
                    - can check if solution is complete if itinerary uses all flights

        ```py
        def get_i(flights, current_i):
            if not flights:
                # all flights used up
                return current_i
            last_stop = current_i[-1]
            for i, (origin, destination) in enumerate(flights):
                # make flights copy w/o current to mark as used
                flights_wo_current = flights[:i] + flights[i+1:]
                current_i.append(destination)
                if origin == last_stop:
                    return get_i(flights_wo_current, current_i)
                current_i.pop()
            return None
        ```

    3. Sudoku
    - Solve well posed sudoku puzzle
        1. Attempt backtracking
            - can partial solution be constructed?
                * yes fill in board portions
            - can partial solution be verified as invalid?
                * can check that board is valid so far if there are no rows, cols, squares with same digit
            - can we verify if solution is complete?
                * solution is complete when board is filled up


        - Try filling each empty cell 1*1 and backtrack once we hit invalid state
        ```py
        empty = 0

        #0
        def sudoku(board):
            #1
            if is_complete(board):
                return board
            #2
            r, c = find_first_empty(board)
            # set r, c to val from 1 -> 9
            for i in range(1, 10):
                board[r][c] = i
                #3
                if valid_so_far(board):
                    #0
                    result = sudoku(board)
                    #1
                    if is_complete(result):
                        return result
                board[r][c] = empty
            return board

        #1
        def is_complete(board):
            return(all(all(
                val is not empty for val in row
            ) for row in board))

        #2
        def find_first_empty(board):
            for i, row in enumerate(board):
                for j, val in enumerate(row):
                    if val == empty:
                        return i, j

        #3
        def valid_so_far(board):
            #4
            if not rows_valid(board):
                return False
            #5
            if not cols_valid(board):
                return False
            #6
            if not blocks_valid(board):
                return False
            return True

        #4
        def rows_valid(board):
            for row in board:
                #7
                if duplicates(row):
                    return False
            return True

        #5
        def cols_valid(board):
            for j in range(len(board[0])):
                if duplicates([board[i][j] for i in range(len(board))]):
                    return False
            return True

        #6
        def blocks_valid(board):
            for i in range(0, 9, 3):
                for j in range(0, 9, 3):
                    block = []
                    for k in range(3):
                        for l in range(3):
                            block.append(board[i + k][j + l])
                    #7
                    if duplicates(block):
                        return False
            return True

        #7
        def duplicates(arr):
            c = {}
            for val in arr:
                if val in c and val is not empty:
                    return True
                c[val] = True
            return False
        ```

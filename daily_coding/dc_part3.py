# 0
"""
Given array of time intervals (start, end) for lectures that possibly overlap, find min number of rooms required

Ex: Given [
    (30,75),
    (0,50),
    (60,150)
] returns 2
"""

# 1
"""
There is a staircase w/ N steps and one can climb up either 1 or 2 steps at a time
Given N, write fxn that returns # of ways one can climb staircase
! Order of steps matter !

If N is 4, 5 unique ways:
1,1,1,1 -> 2,1,1 -> 1,2,1 -> 1,1,2 -> 2,2

Extra:
if instead of being able to climb 1 or 2 steps at a time you can climb any number from set of positive integers x
i.e if x ={1,3,5} one can climb 1,3,5 steps at a time
"""


# 2
"""
Given integer k, string s, find length of longest substring containing most distinct chars
given s = 'abcba', k= 2
longest substring with k distinct chars is 'bcb'
"""

# 3
"""
Area of circle is pi * r squared
estimate pi to three decimal places with monte carlo method
basic eq: x squared + y squared = r squared

pi(x^2+y^2)^2
"""

# 4
"""
Run an e-commerce site and desire to record last N order ids in a log, implement ds to accomplish said feat with this api:
    - record(order_id): add order_id to log
    - get_last(i): get ith last element from log -> )guaranteed) i<=N

Prioritize space, time efficiency
"""

# 5
"""
Given dict of words, string made of words (NO SPACES) -> return original sentence in list
If there is >1 possible reconstruction, return any of them

If no possible reconstruction, return null

- EXAMPLES -
(1)
words set: quick, brown, the, fox
string: thequickbrownfox
return: [the, quick, brown, fox]

(2)
words set: bed, bath, bedbath, and beyond
string: bedbathandbeyond
return: [bed, bath, and, beyond], [bedbath, and, beyond]
"""

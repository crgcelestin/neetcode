# Google Coding Interview Problems

## 1
- XOR linked list is more memory efficient as compared to double LL
    * instead of each node holding next, prev fields there is a field 'both' that is an XOR of next and previous node

* __GIVEN TASK__
- implement XOR linked list: has an `add(element)` that adds element to end and a `get(index)` that returns node at index

- if in language w/o pointers (Python), can assume there is access to get_pointer and dereference_pointers functions that convert between nodes and memory addresses

## 2
Unival tree, 'unival' = universal value is a tree where all nodes under it have same value
- Given root to binary tree count number of unival subtrees
    * Example:
        - has 5 unival subterees
        ```py
                        0 (2)
                       / \
                 (4)  1   0 (1)
                         / \
                    (3) 1   0
                       / \
                      1   1
        ```

## 3
Given integer list, write function returning largest sum of non-adjacent #s -> numbers can be 0 or negative
- Example:
    1. [2,4,6,2,5] -> returns 13 as 2,6,5 are picked
    2. [5,1,1,5] -> return 10 as 5,5 are picked

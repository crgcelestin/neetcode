class Solution:
    def countingPairs(self, n: int, numbers: list[int], k: int):
        if not numbers:
            return 0
        counter = {}
        pairs = 0
        for num in numbers:
            if num in counter:
                counter[num] += 1
            else:
                counter[num] = 1
        for num in counter.keys:
            if k == 0:
                pairs += 1 if counter[num] > 1 else 0
            else:
                pairs += 1 if num + k in counter else 0
        return pairs


TestCases = Solution()
Test1 = TestCases.countingPairs(n=4, numbers=[1, 1, 2], k=1)
print(Test1 == 1)
Test2 = TestCases.countingPairs(n=2, numbers=[1, 2], k=0)
print(Test2 == 2)

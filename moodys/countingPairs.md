# Counting Pairs
## Description
- Given an integer `k:int` and list of integers `numbers: list[int]` count # of distinct valid integer pairs `(a,b)` in list where `a+k=b`
    * 2 pairs `(a,b) and (c,d)` are considered distinct if at least 1 element of (a,b) does not belong to (c,d)
    * elements in pair may be same element in array, such as when  `k=0`

### Examples
```py
n=4
numbers = [1,1,2]
k=1
'''
array has 3 valid pairs
(1,1), (1,2), (2,2)
for k=1, only 1 valid pair satisfies a+k=b
(a,b) = (1,2)
'''

n=2
numbers=[1,2]
k=0
'''
3 valid pairs
(1,1), (2,2), (1,2)
for k=0, 2 pairs satisfy rule
1+0=1, 2+0=2
(a,b) = (1,1), (2,2)
'''
```

## Attempt (to revise) O(n)
```py
class Solution:
    def countingPairs(self, n:int, numbers:list[int], k:int):
        if not numbers:
            return 0
        # use set to optimize performance when iterating for desired aggregate in list
        unique_numbers = set(numbers)
        pairs = 0
        for num in numbers:
            if num+k in unique_numbers:
                pairs+=1
        return pairs
```
## Optimized
[Using Dictionary](CP.py)

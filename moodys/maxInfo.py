class Solution:
    def security_hack(self, n: int, security_val: list[int], k: int) -> int:
        if not security_val:
            return 0
        if n < 2 or k >= n:
            return max(security_val)
        max_sum = security_val[0]
        for i in range(n):
            result, j = 0, i
            while j < n:
                result += security_val[j]
                j += k
            max_sum = max(max_sum, result)
        return max_sum


TestCases = Solution()
Test1 = TestCases.security_hack(k=2, security_val=[2, -3, 4, 6, 1], n=5)
print(Test1)
print(Test1 == 7)

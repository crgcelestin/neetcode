# Reverse Array Queries
## Description
- For given integer array, perform operations on array
    1. Each op contains 2 indices
    2. reverse subarray between those 2 zero based incides (inclusive)
    3. Return resulting array after all operations have been applied in provided order

### Inputs, Outputs
```py
def reverse_array(self, arr:list[int], operations: list[list[int]])-> list[int]:
```

### Example
```py
arr = [9,8,7,6,5,4,3,2,1,0]
operations = [
    [0,9],
    [4,5],
    [3,6]
]
```
- Result 1st operation: 
[0,1,2,3,4,5,6,7,8,9]
- Result 2nd operation:
[0,1,2,3,5,4,6,7,8,9]
- Result 3rd op:
[0,1,2,6,4,5,3,7,8,9]

### Code
```py
class Solution:
    def performOperations(self, arr: list[int], operations: list[list[int]])-> list[int]:
        def swap(list1, first, last):
            [arr[first], arr[last]] = [arr[last], arr[first]]
        for op in operations:
            i, j = op[0], op[1]
            while i<j:
                swap(arr, i, j)
                i+=1
                j-=1
        return arr

arr1 = [9,8,7,6,5,4,3,2,1,0]
operations1 = [
    [0,9],
    [4,5],
    [3,6]
]
TestCases = Solution()
Test1 = TestCases.performOperations(
    arr1, operations1
)
```

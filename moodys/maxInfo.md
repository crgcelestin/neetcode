# Max Information
## Description
- There is a computer network consisting of n servers/nodes
    - they are numbered from 1 to n (n being the array length)
    - each node has security value, security_val[i]
- Hacker must choose starting node, jump through network of servers until reaching end

## Movement
- initially, hacker has access to 0 servers with 0 security value

- from node x, hacker can jump to node(x+k)
    - if node(x+k), does not exist -> jump is out of network and hack ends

-  security_val[i] is added to hacker's security value sum, with values potentially being negative

## Goal
- Choose the most optimal starting noed so that hacker compromises servers with max possible security value sum

### Inputs, Outputs
```py
def security_hack(self, n:int, security_val:list[int], k: int)-> int:
```

### Example
```py
n=5 (length of array)
security_val=[2, -3, 4, 6, 1] (security node values)
k=2 (node jump distance)
# node index = 1 chosen as starting node
security_val = security_val[1]+ security_val[3] + security_val[5]
# 2 + 4 + 1 = 7
# arrays are 0 indexed, so it really is sv[0] + sv[2] + sv[4]= 7
```

## Attempt (to revise)
```py
class Solution:
  def security_hack(self, n:int, security_val:list[int], k: int)-> int:
      if not security_val:
          return 0
      if n<2 or k>=n:
          return security_val[0]
      # removed later as one can directly compare and update max_sum var
      # max_sum = security_va[0]
      combos = []
      # move within loop and result to be set to i
      i, result = 0, 0
      while i<n:
          while i+k < n:
              result+=security_val[i]
              i+=k
          combos.append(result)
          i+=1
      return max(combos)

TestCases = Solution()
Test1=TestCases.security_hack(
  k=2,
  security_val=[2, -3, 4, 6, 1],
  n=5
)
print(Test1)
print(Test1==7)
```

## Revised
[Revised Solution](maxInfo.py)
